/*
 * app常用工具对象
 */
 
var Utils = {
	
	// 判断网络是否可用,返回true或者false
	isNetworkAvailable: function(){
		var number = plus.networkinfo.getCurrentType();
		if (number === 1) {  // 1代表未连接网络
			return false;
		} else {   			// wifi,流量或者状态未知
			return true;
		}
	}
	
}
