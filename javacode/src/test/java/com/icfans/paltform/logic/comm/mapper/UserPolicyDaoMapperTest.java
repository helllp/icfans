package com.icfans.paltform.logic.comm.mapper;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.model.SusrRole;
import com.icfans.paltform.test.BaseSpringBootTest;

public class UserPolicyDaoMapperTest extends BaseSpringBootTest{
	@Autowired
	private UserPolicyDaoMapper daoMapper;
	
	@Test
	public void queryByUserIdTest(){
		Long userId = 97724834917781504L;
		
		List<SusrRole> list = daoMapper.queryByUserId(userId);
		
		for (SusrRole susrRole : list) {
			System.out.println(susrRole.getRoleName());
		}
	}
}
