package com.icfans.paltform.demo.web;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.icfans.paltform.logic.demo.service.RedisService;
import com.icfans.paltform.test.BaseSpringBootTest;


public class TestControllerTest extends BaseSpringBootTest{
	MockMvc mvc;  
	  
    @Autowired  
    WebApplicationContext webApplicationConnect;  
    
	@Resource
	private RedisService service;

    @Before  
    public void setUp() throws JsonProcessingException {  
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationConnect).build();  
    }
    
    //	模拟请求
	@Test
	public void testRedis() throws Exception{
		String uri = "/redis?no=lxm&name=name1";
		
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON))  
                .andReturn();  
        int status = mvcResult.getResponse().getStatus();  
        String content = mvcResult.getResponse().getContentAsString();
        
        System.out.println(status);
        System.out.println(content);
	}
	
	@Test
	public void testAssert() {

		// 一般匹配符
		int s = 2;
		// allOf：所有条件必须都成立，测试才通过
		assertThat(s, allOf(greaterThan(1), lessThan(3)));
		// anyOf：只要有一个条件成立，测试就通过
		assertThat(s, anyOf(greaterThan(1), lessThan(1)));
		// anything：无论什么条件，测试都通过
		assertThat(s, anything());
		// is：变量的值等于指定值时，测试通过
		assertThat(s, is(2));
		// not：和is相反，变量的值不等于指定值时，测试通过
		assertThat(s, not(1));

		// 数值匹配符
		double d = 3.33;
		// closeTo：浮点型变量的值在3.0±0.5范围内，测试通过
		assertThat(d, closeTo(3.0, 0.5));
		// greaterThan：变量的值大于指定值时，测试通过
		assertThat(d, greaterThan(3.0));
		// lessThan：变量的值小于指定值时，测试通过
		assertThat(d, lessThan(3.5));
		// greaterThanOrEuqalTo：变量的值大于等于指定值时，测试通过
		assertThat(d, greaterThanOrEqualTo(3.3));
		// lessThanOrEqualTo：变量的值小于等于指定值时，测试通过
		assertThat(d, lessThanOrEqualTo(3.4));

		// 字符串匹配符
		String n = "Magci";
		// containsString：字符串变量中包含指定字符串时，测试通过
		assertThat(n, containsString("ci"));
		// startsWith：字符串变量以指定字符串开头时，测试通过
		assertThat(n, startsWith("Ma"));
		// endsWith：字符串变量以指定字符串结尾时，测试通过
		assertThat(n, endsWith("i"));
		// euqalTo：字符串变量等于指定字符串时，测试通过
		assertThat(n, equalTo("Magci"));
		// equalToIgnoringCase：字符串变量在忽略大小写的情况下等于指定字符串时，测试通过
		assertThat(n, equalToIgnoringCase("magci"));
		// equalToIgnoringWhiteSpace：字符串变量在忽略头尾任意空格的情况下等于指定字符串时，测试通过
		assertThat(n, equalToIgnoringWhiteSpace(" Magci   "));

		// 集合匹配符
		List<String> l = new ArrayList<String>();
		l.add("Magci");

		// hasItem：Iterable变量中含有指定元素时，测试通过
		assertThat(l, hasItem("Magci"));

		Map<String, String> m = new HashMap<String, String>();
		m.put("mgc", "Magci");

		// hasEntry：Map变量中含有指定键值对时，测试通过
		assertThat(m, hasEntry("mgc", "Magci"));
		// hasKey：Map变量中含有指定键时，测试通过
		assertThat(m, hasKey("mgc"));
		// hasValue：Map变量中含有指定值时，测试通过
		assertThat(m, hasValue("Magci"));
	}
}
