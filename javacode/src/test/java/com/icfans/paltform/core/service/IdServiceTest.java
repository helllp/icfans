package com.icfans.paltform.core.service;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.test.BaseSpringBootTest;

public class IdServiceTest extends BaseSpringBootTest{
    @Autowired
    private IdService idService;
    
//	@Test
    public void testId() {
        class IdServiceThread implements Runnable {
            private Set<Long> set;
            
            private IdService idService;
            
            public IdServiceThread(Set<Long> set, IdService idService) {
                this.set = set;
                this.idService = idService;
            }

            @Override
            public void run() {
                while (true) {
                    long id = idService.nextId();
                    System.out.println("duplicate:" + id);
                    if (!set.add(id)) {
                        System.out.println("duplicate:" + id);
                    }
                }
            }
        }

        Set<Long> set = new HashSet<Long>();
        for(int i=0;i<100;i++){
            Thread t1 = new Thread(new IdServiceThread(set, idService));
            t1.setDaemon(true);
            t1.start();
            try {
                Thread.sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
	
}
