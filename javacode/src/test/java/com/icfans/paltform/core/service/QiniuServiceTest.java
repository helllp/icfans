package com.icfans.paltform.core.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.test.BaseSpringBootTest;
import com.qiniu.common.QiniuException;

public class QiniuServiceTest extends BaseSpringBootTest{
	@Autowired
	private QiniuService service;
	
	@Test
	public void testDomainList() throws QiniuException{
		String bucketName = "hexo";
		
		String[] arr = service.domainList(bucketName);
		
		for(int i=0; i<arr.length; i++){
			System.out.println(arr[i]);	
		}
		
	}
}
