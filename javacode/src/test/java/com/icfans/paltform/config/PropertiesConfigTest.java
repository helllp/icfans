package com.icfans.paltform.config;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.test.BaseSpringBootTest;
import com.icfans.paltform.utils.HttpClientUtil;

public class PropertiesConfigTest extends BaseSpringBootTest{
	@Autowired
	private PropertiesConfig config;
	
	@Test
	public void test(){
		System.out.println(config.getAccessKey());
		System.out.println(config.getBucketName());
		System.out.println(config.getPhoneUrl());
		System.out.println(config.getSecuretKey());
	}
	
	@Test
	public void getUserContract() throws Exception{
		String url = config.getUserContract();
		
		System.out.println(HttpClientUtil.GetMethod(url));
		
	}
}
