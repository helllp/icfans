package com.icfans.paltform;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.icfans.paltform.utils.DbTypeUtils;
import com.icfans.paltform.utils.HttpClientUtil;

public class NormalTest {
	@Test
	public void yyyyMMdd() throws ParseException{
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); // 日期格式
		Date date = dateFormat.parse("2017-01-01"); // 指定日期
		System.out.println(date);
		System.out.println(date.getTime());
		
	}
	
	@Test
	public void testDate(){
		System.out.println(DbTypeUtils.getDate());
		System.out.println(DbTypeUtils.getTime());
		System.out.println(DbTypeUtils.getDateTime());
	}
	
	@Test
	public void stringUtilTest(){
		String t = StringUtils.join(new String[]{},",");
		System.out.println(t);
	}
	
	@Test
	public void testGetHtml() throws Exception{
		System.out.println(HttpClientUtil.GetMethod("http://www.cnblogs.com/zhuawang/archive/2012/12/08/2809380.html"));
	}
}
