package com.icfans.paltform.data;

import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.mapper.SusrUserManagerMapper;
import com.icfans.paltform.mapper.SusrUserMapper;
import com.icfans.paltform.mapper.SusrUserinfoMapper;
import com.icfans.paltform.model.SusrUser;
import com.icfans.paltform.model.SusrUserManager;
import com.icfans.paltform.model.SusrUserinfo;
import com.icfans.paltform.test.BaseSpringBootTest;
import com.icfans.paltform.utils.DbTypeUtils;

/**
 * 创建测试用的用户数据
 * 
 * @author 林晓明
 *
 */
public class CreateUserData extends BaseSpringBootTest{
	@Autowired
	private SusrUserMapper userMapper;
	
	@Autowired
	private SusrUserinfoMapper userInfoMapper;
	
	@Autowired
	private SusrUserManagerMapper userManagMapper;
	
	@Autowired
	private IdService idService;
	
	private static String[] phoneArr = {
			"13000000000",
			"13000000001",
			"13000000002",
			"13000000003",
			"13000000004",
			"13000000005",
			"13000000006",
			"13000000007",
			"13000000008",
			"13000000009",
			"13000000010",
			"13000000011",
			"13000000012",
			"13000000013",
			"13000000014",
			"13000000015",
			"13000000016",
			"13000000017",
			"13000000018",
			"13000000019",
			"13000000020",
			"13000000021",
			"13000000022",
			"13000000023",
			"13000000024",
			"13000000025",
			"13000000026",
			"13000000027",
			"13000000028",
			"13000000029"
	};
	
	private static String[] nameArr = {
			"丁聪华",
			"夏潇琦",
			"曾帛员",
			"韩松",
			"孙蝶妃",
			"江浩华",
			"田宇旺",
			"孔良超",
			"许娇翔",
			"庞妍",
			"陈莲眉",
			"冉迪振",
			"崔子希",
			"曹娅娴",
			"张红",
			"陈寿渊",
			"樊瑶芳",
			"唐亚升",
			"马桂蓓",
			"徐经岚",
			"阮恭琴",
			"任希",
			"孙花",
			"赵美珍",
			"姚道益",
			"汪冰蕴",
			"何彦",
			"米泽升",
			"朱咏娴",
			"陆银兴"
	};
	
//	@Test
	public void createUserInfo(){
		for(int i=0; i<phoneArr.length; i++){
			SusrUser user = new SusrUser();
			
			user.setId(idService.nextId());
			user.setTelephone(phoneArr[i]);
			DbTypeUtils.setCommonProp(user);
			userMapper.insert(user);	
			
			SusrUserinfo userInfo = new SusrUserinfo();
			
			userInfo.setId(idService.nextId());
			userInfo.setUserId(user.getId());
			userInfo.setUserName(nameArr[i]);
			userInfo.setSex(i % 2);
			DbTypeUtils.setCommonProp(userInfo);
			userInfoMapper.insert(userInfo);
			
			SusrUserManager manager = new SusrUserManager();
			
			manager.setId(idService.nextId());
			manager.setUserId(user.getId());
			manager.setAnswerNum(i);
			manager.setArchiveNum(i);
			manager.setFollowedNum(i);
			DbTypeUtils.setCommonProp(userInfo);
			userManagMapper.insert(manager);
		}

	}
}
