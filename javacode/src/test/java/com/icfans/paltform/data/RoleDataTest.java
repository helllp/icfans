package com.icfans.paltform.data;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.mapper.SusrRoleMapper;
import com.icfans.paltform.mapper.SusrRolePermissMapper;
import com.icfans.paltform.model.SusrRole;
import com.icfans.paltform.model.SusrRolePermiss;
import com.icfans.paltform.test.BaseSpringBootTest;
import com.icfans.paltform.utils.DbTypeUtils;

public class RoleDataTest extends BaseSpringBootTest{
    @Autowired
    private SusrRoleMapper roleMapper;

    @Autowired
    private SusrRolePermissMapper rolePremissMapper;
    
    @Autowired
    private IdService idService;
    
    @Test
    public void createRoleData(){
    	SusrRole role = new SusrRole();
    	
    	role.setCreateTime(DbTypeUtils.getDateTime());
    	role.setUpdateTime(role.getCreateTime());
    	role.setCreateUserId(0L);
    	role.setUpdateUserId(0L);
    	
    	role.setId(1L);
    	role.setRoleName("管理员");
    	role.setRemark("");
    	roleMapper.insert(role);
    	
    	role.setId(2L);
    	role.setRoleName("普通用户");
    	role.setRemark("");
    	roleMapper.insert(role);
    	
    	role.setId(3L);
    	role.setRoleName("专家");
    	role.setRemark("");
    	roleMapper.insert(role);
    	
    	role.setId(4L);
    	role.setRoleName("机构管理员");
    	role.setRemark("");
    	roleMapper.insert(role);
    }
    
    @Test
    public void createUserRoleData(){
    	Long userId = 97724834917781504L;
    	
    	SusrRolePermiss rolePer = new SusrRolePermiss();
    	
    	rolePer.setCreateTime(DbTypeUtils.getDateTime());
    	rolePer.setUpdateTime(rolePer.getCreateTime());
    	rolePer.setCreateUserId(0L);
    	rolePer.setUpdateUserId(0L);
    	rolePer.setUserId(userId);
    	
    	rolePer.setId(idService.nextId());
    	rolePer.setRoleId(1L);
    	rolePremissMapper.insert(rolePer);
    	
    	rolePer.setId(idService.nextId());
    	rolePer.setRoleId(2L);
    	rolePremissMapper.insert(rolePer);
    	
    	rolePer.setId(idService.nextId());
    	rolePer.setRoleId(3L);
    	rolePremissMapper.insert(rolePer);
    }
}
