package com.icfans.paltform.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class DownloadUtil {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static ResponseEntity<String> downloadFile(
			HttpServletRequest request, HttpServletResponse response,
			File file, String downloadName) throws IOException {

		FileInputStream fin = null;
		OutputStream output = null;
		try {

			if (file.exists()) {
				int fSize = Integer.parseInt(String.valueOf(file.length()));
				fin = new FileInputStream(file);
				// response.reset(); //设置为没有缓存
				response.setCharacterEncoding("utf-8");
				response.setContentType("application/x-download");
				response.setHeader("Accept-Ranges", "bytes");
				response.setHeader("Content-Length", String.valueOf(fSize));
				response.setHeader(
						"Content-Disposition",
						"attachment;filename="
								+ new String(downloadName.getBytes("UTF-8"),
										"iso-8859-1"));

				long pos = 0;
				long posEnd = fSize - 1;
				if (null != request.getHeader("Range")) {
//					System.out.println("Range=" + request.getHeader("Range"));
					// 断点续传
					response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
					try {
						pos = Long.parseLong(request.getHeader("Range")
										.substring(6,request.getHeader("Range").indexOf("-")));// 截取
						posEnd = Long.parseLong(request.getHeader("Range")
										.substring(request.getHeader("Range").indexOf("-") + 1));// 截取
					} catch (NumberFormatException e) {
						pos = 0;
					}
				}

				long length = posEnd - pos;

				String contentRange = new StringBuffer("bytes ")
						.append(pos + "").append("-").append((fSize - 1) + "")
						.append("/").append(fSize + "").toString();
				response.setHeader("Content-Range", contentRange);
//				System.out.println("Content-Range=" + contentRange);
				fin.skip(pos);

				/*
				 * attachment是以附件下载的形式。 当点击“保存”的时候都可以下载，
				 * 当点击“打开”的时候attachment是在本地机里打开。
				 */
				output = response.getOutputStream();
				byte[] buf = new byte[1024];
				int r = 0;

				while ((r = fin.read(buf, 0, buf.length)) != -1) {
					output.write(buf, 0, r);

					length = length - r;
					if (posEnd <= 0) {
						break;
					}
				}
			} else {
				throw new FileNotFoundException();
			}
		} catch (IOException e) {
//			ExceptionUtil.getExceptionMessage(e);
			e.printStackTrace();
		} finally {
			try {
				if (output != null)
					output.flush();
				if (output != null)
					output.close();
				if (fin != null)
					fin.close();
			} catch (IOException e) {
			}
		}

		return new ResponseEntity(null, HttpStatus.OK);
	}

	public static String setContentType(String returnFileName) {
		String contentType = "application/octet-stream";
		if (returnFileName.lastIndexOf(".") < 0)
			return contentType;
		returnFileName = returnFileName.toLowerCase();
		returnFileName = returnFileName.substring(returnFileName
				.lastIndexOf(".") + 1);

		if (returnFileName.equals("html") || returnFileName.equals("htm")
				|| returnFileName.equals("shtml")) {
			contentType = "text/html";
		} else if (returnFileName.equals("css")) {
			contentType = "text/css";
		} else if (returnFileName.equals("xml")) {
			contentType = "text/xml";
		} else if (returnFileName.equals("gif")) {
			contentType = "image/gif";
		} else if (returnFileName.equals("jpeg")
				|| returnFileName.equals("jpg")) {
			contentType = "image/jpeg";
		} else if (returnFileName.equals("js")) {
			contentType = "application/x-javascript";
		} else if (returnFileName.equals("atom")) {
			contentType = "application/atom+xml";
		} else if (returnFileName.equals("rss")) {
			contentType = "application/rss+xml";
		} else if (returnFileName.equals("mml")) {
			contentType = "text/mathml";
		} else if (returnFileName.equals("txt")) {
			contentType = "text/plain";
		} else if (returnFileName.equals("jad")) {
			contentType = "text/vnd.sun.j2me.app-descriptor";
		} else if (returnFileName.equals("wml")) {
			contentType = "text/vnd.wap.wml";
		} else if (returnFileName.equals("htc")) {
			contentType = "text/x-component";
		} else if (returnFileName.equals("png")) {
			contentType = "image/png";
		} else if (returnFileName.equals("tif")
				|| returnFileName.equals("tiff")) {
			contentType = "image/tiff";
		} else if (returnFileName.equals("wbmp")) {
			contentType = "image/vnd.wap.wbmp";
		} else if (returnFileName.equals("ico")) {
			contentType = "image/x-icon";
		} else if (returnFileName.equals("jng")) {
			contentType = "image/x-jng";
		} else if (returnFileName.equals("bmp")) {
			contentType = "image/x-ms-bmp";
		} else if (returnFileName.equals("svg")) {
			contentType = "image/svg+xml";
		} else if (returnFileName.equals("jar") || returnFileName.equals("var")
				|| returnFileName.equals("ear")) {
			contentType = "application/java-archive";
		} else if (returnFileName.equals("doc")) {
			contentType = "application/msword";
		} else if (returnFileName.equals("pdf")) {
			contentType = "application/pdf";
		} else if (returnFileName.equals("rtf")) {
			contentType = "application/rtf";
		} else if (returnFileName.equals("xls")) {
			contentType = "application/vnd.ms-excel";
		} else if (returnFileName.equals("ppt")) {
			contentType = "application/vnd.ms-powerpoint";
		} else if (returnFileName.equals("7z")) {
			contentType = "application/x-7z-compressed";
		} else if (returnFileName.equals("rar")) {
			contentType = "application/x-rar-compressed";
		} else if (returnFileName.equals("swf")) {
			contentType = "application/x-shockwave-flash";
		} else if (returnFileName.equals("rpm")) {
			contentType = "application/x-redhat-package-manager";
		} else if (returnFileName.equals("der") || returnFileName.equals("pem")
				|| returnFileName.equals("crt")) {
			contentType = "application/x-x509-ca-cert";
		} else if (returnFileName.equals("xhtml")) {
			contentType = "application/xhtml+xml";
		} else if (returnFileName.equals("zip")) {
			contentType = "application/zip";
		} else if (returnFileName.equals("mid")
				|| returnFileName.equals("midi")
				|| returnFileName.equals("kar")) {
			contentType = "audio/midi";
		} else if (returnFileName.equals("mp3")) {
			contentType = "audio/mpeg";
		} else if (returnFileName.equals("ogg")) {
			contentType = "audio/ogg";
		} else if (returnFileName.equals("m4a")) {
			contentType = "audio/x-m4a";
		} else if (returnFileName.equals("ra")) {
			contentType = "audio/x-realaudio";
		} else if (returnFileName.equals("3gpp")
				|| returnFileName.equals("3gp")) {
			contentType = "video/3gpp";
		} else if (returnFileName.equals("mp4")) {
			contentType = "video/mp4";
		} else if (returnFileName.equals("mpeg")
				|| returnFileName.equals("mpg")) {
			contentType = "video/mpeg";
		} else if (returnFileName.equals("mov")) {
			contentType = "video/quicktime";
		} else if (returnFileName.equals("flv")) {
			contentType = "video/x-flv";
		} else if (returnFileName.equals("m4v")) {
			contentType = "video/x-m4v";
		} else if (returnFileName.equals("mng")) {
			contentType = "video/x-mng";
		} else if (returnFileName.equals("asx") || returnFileName.equals("asf")) {
			contentType = "video/x-ms-asf";
		} else if (returnFileName.equals("wmv")) {
			contentType = "video/x-ms-wmv";
		} else if (returnFileName.equals("avi")) {
			contentType = "video/x-msvideo";
		}

		return contentType;
	}

	public static ResponseEntity<String> downloadFile(
			HttpServletRequest request, HttpServletResponse response, File file)
			throws IOException {
		return downloadFile(request, response, file, file.getName());
	}
}
