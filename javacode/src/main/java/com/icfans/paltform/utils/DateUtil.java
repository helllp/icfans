package com.icfans.paltform.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 日期工具类
 * 
 * 
 */
public class DateUtil {

	/**
	 * 日期格式字符串
	 */
	public static final String FORMAT_PATTERN_LONG = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_PATTERN_SHORT = "yyyy-MM-dd";
	public static final String FORMAT_PATTERN_MONTH = "yyyy-MM";
	public static final String FORMAT_PATTERN_YYYYMMDD = "yyyyMMdd";
	public static final String FORMAT_PATTERN_MMDDHHMM = "MM-dd HH:mm";

	/**
	 * 日期差异比较类型
	 */
	public static enum DiffType
	{
		YEAR, MONTH, DAY, HOUR, MINUTE, SECOND;
	}
	
    public static boolean checkLongDateStr(String dateStr) {
		try {
			SimpleDateFormat sf = null;
			
			sf = new SimpleDateFormat(FORMAT_PATTERN_LONG);
			
			sf.setLenient(false);
			sf.parse(dateStr);
			if(FORMAT_PATTERN_LONG.length() != dateStr.length()) {
				return true;
			}
			return false;
		} catch(Exception e) {
			return true;
		}
    }
	
    public static boolean checkShortDateStr(String dateStr) {
		try {
			SimpleDateFormat sf = null;
			
			sf = new SimpleDateFormat(FORMAT_PATTERN_SHORT);
			
			sf.setLenient(false);
			sf.parse(dateStr);
			if(FORMAT_PATTERN_SHORT.length() != dateStr.length()) {
				return true;
			}
			return false;
		} catch(Exception e) {
			return true;
		}
    }
    
    public static boolean checkMonthDateStr(String dateStr) {
		try {
			SimpleDateFormat sf = null;
			
			sf = new SimpleDateFormat(FORMAT_PATTERN_MONTH);
			
			sf.setLenient(false);
			sf.parse(dateStr);
			if(FORMAT_PATTERN_MONTH.length() != dateStr.length()) {
				return true;
			}
			return false;
		} catch(Exception e) {
			return true;
		}
    }
	/**
	 * 获取当前日期（Date）
	 * @return Date
	 */
	public static Date getDate(){
		return new Date();
	}
	
	/**
	 * 获得当前年份
	 * @return
	 */
	public static String getYyyy(){
		return "" + Calendar.getInstance().get(Calendar.YEAR);
	}
	
	/**
	 * 获取当前日期（Timestamp）
	 * @return Timestamp
	 */
	public static Timestamp getTimestamp(){
		return new Timestamp((new Date()).getTime());
	}

	/**
	 * 日期格式化，默认格式yyyy-MM-dd HH:mm:ss
	 * @param date 支持Date，Timestamp类型
	 * @return
	 */
	public static String format(Object date) {
		return format(date, FORMAT_PATTERN_LONG);
	}
	
	/**
	 * 日期格式化，默认格式yyyy-MM-dd
	 * @param date 支持Date，Timestamp类型
	 * @return
	 */
	public static String formatShort(Object date) {
		return format(date, FORMAT_PATTERN_SHORT);
	}

	/**
	 * 日期格式化，指定格式化字符串
	 * @param date 支持Date，Timestamp类型
	 * @param pattern 自定义格式化字符串
	 * @return
	 */
	public static String format(Object date, String pattern) {
		if (date == null) {
			return null;
		}

		if (!( date instanceof Date || date instanceof Timestamp )) {
			throw new IllegalArgumentException("Please input Date or Timestamp.");
		}
		
		if (pattern == null || pattern.equals("") || pattern.equals("null")) {
			pattern = FORMAT_PATTERN_LONG;
		}
		return new java.text.SimpleDateFormat(pattern).format(date);
	}

	/**
	 * 将字符串转换为Date类型
	 * @param date 支持10位和19位两种长度日期字符串
	 * @return
	 */
	public static Date toDate(String date) {
		if (date == null)
			return null;
		
		if(date.length() == 7) {
			return toDate(date,FORMAT_PATTERN_MONTH);
		} else if ( date.length() <= 10 ) {
			return toDate(date,FORMAT_PATTERN_SHORT);
		} else { 
			return toDate(date,FORMAT_PATTERN_LONG);
		}
	}

	/**
	 * 将字符串转换为Timestamp类型
	 * @param date 支持10位和19位两种长度日期字符串
	 * @return
	 */
	public static Timestamp toTimestamp(String date) {
		Date d = toDate(date);

		if (d == null)
			return null;

		return new Timestamp(d.getTime());
	}

	/**
	 * 将Date类型转换为Timestamp类型
	 * @param date
	 * @return
	 */
	public static Timestamp toTimestamp(Date date) {
		if (date == null) {
			return null;
		}

		Timestamp t = new Timestamp(date.getTime());
		return t;
	}

	/**
	 * 将字符串转换为Date类型，指定格式化字符串
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static Date toDate(String date, String pattern) {
		if (pattern == null || pattern.equals("") || pattern.equals("null")) {
			pattern = "yyyy-MM-dd HH:mm:ss";
		}
		if (date == null || date.equals("") || date.equals("null")) {
			return null;
		}
		Date d = null;
		try {
			d = new java.text.SimpleDateFormat(pattern).parse(date);
		} catch (ParseException pe) {
			throw new IllegalArgumentException("Please input correct date string.");
		}
		return d;
	}

	/**
	 * 比较两个日期大小，返回间隔时间（指定单位）
	 * @param d1
	 * @param d2
	 * @param type
	 * @return
	 */
	public static long getTimeDiff(Date d1, Date d2, DiffType type) {

		long value = 0;
		switch (type) {
		case DAY:
			value = Math.abs(d1.getTime() - d2.getTime())/3600000/24;
			break;
		case HOUR:
			value = Math.abs(d1.getTime() - d2.getTime())/3600000;
			break;
		case MINUTE:
			value = Math.abs(d1.getTime() - d2.getTime())/60000;
			break;
		case SECOND:
			value = Math.abs(d1.getTime() - d2.getTime())/1000;
			break;
		default:
			break;
		}
		// 因为t1-t2得到的是毫秒级,1秒=1000毫秒，所以要初3600000得出小时。
		// int hours=(int) ((t1 - t2)/3600000);
		//long minutes = (long) ((t1 - t2) / 60000); // 相差多少分钟
		// int second=(int) ((t1 - t2)/1000-hours*3600-minutes*60);
		return value;
	}
	
	/**
	 * 返回日期增加days天数后的日期，正数表示将来,负数表示过去
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addDay(Date date, int days) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date); 
	    calendar.add(Calendar.DAY_OF_MONTH,days);//正数表示将来,负数表示过去 
	  //calendar.add(Calendar.DATE, n);
	  //calendar.add(Calendar.MONTH, n)
	    return calendar.getTime();
	}
	
	/**
	 * 返回日期增加months月数后的日期，正数表示将来,负数表示过去
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date addMonth(Date date, int months) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date); 
	  //calendar.add(Calendar.DAY_OF_MONTH,days);//整数往后推,负数往前移动 
	  //calendar.add(Calendar.DATE, n);
	    calendar.add(Calendar.MONTH, months);
	    return calendar.getTime();
	}
	
	/**
	 * 获得日期所在年的周数
	 * @param date
	 * @return
	 */
	public static int getWeekNumOfYear(Date date){  
	    Calendar calendar = Calendar.getInstance(); 
	    calendar.setFirstDayOfWeek(Calendar.MONDAY);  //设置周一为每周第一天
	    calendar.setTime(date); 
	    int iWeekNum = calendar.get(Calendar.WEEK_OF_YEAR);  
	    return iWeekNum;  
	}
	
//	@Test
//	public void testFormat() throws Exception {
//		System.out.println(format(getDate(),"yyyy111aaadd"));
//		System.out.println(format(getTimestamp(),"1"));
//		System.out.println(format(getDate()));
//		System.out.println(formatShort(getTimestamp()));
//		System.out.println(toDate("2015-01-15"));
//		System.out.println(toTimestamp("2015-01-15"));
//		System.out.println(toTimestamp("2015-01-15 12:01:01"));
//		System.out.println(getTimeDiff(new Date(),toTimestamp("2015-07-15"),DiffType.DAY));
//		System.out.println(getTimeDiff(new Date(),toTimestamp("2015-07-2 17:00:00"),DiffType.HOUR));
//		System.out.println(addDay(new Date(),-1));
//		System.out.println(addMonth(new Date(),1));
//		System.out.println(getWeekNumOfYear(toDate("2013-01-15")));
//	}
	/**
	 * 
	 * @MethodName: compareScope
	 * @Description:  比较日期在时间范围的关系
	 * @author ZhangZh
	 * @date 2015年12月1日 上午8:38:00
	 * @param date
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	public static int compareScope(Date date,String startTime,String endTime) {
		String nowTime = DateUtil.format(DateUtil.getDate(),"HH:mm"); 
		if ( startTime.compareTo(endTime) <= 0 ) {
			if (nowTime.compareTo(startTime) >= 0 && nowTime.compareTo(endTime) <= 0) {
				return 0;
			}
		} else {
			
		}
		return -1;
	}
	
	/**
	 * 与当前时间进行比较
	 * 
	 * @param startTime		起始时:分
	 * @return	<0 : 表示当前时间在规定时间之后
	 */
	public static int comapreTime(String startTime){
		String nowTime = DateUtil.format(DateUtil.getDate(),"HH:mm");
		
		return startTime.compareTo(nowTime);
	}
	
	/**
	 * 获取上月的天数 
	 */
	public static int getPrevMonthDays(){
		//取得系统当前时间
		Calendar cal = Calendar.getInstance();
		//取得系统当前时间所在月第一天时间对象
		cal.set(Calendar.DAY_OF_MONTH, 1);
		//日期减一,取得上月最后一天时间对象
		cal.add(Calendar.DAY_OF_MONTH, -1);
		//上月最后一天日期
		return cal.get(Calendar.DAY_OF_MONTH); 
	}

	/**
	 * 获取月的天数 
	 */
	public static int getMonthDays(int yyyy,int mm){
		Calendar cal = Calendar.getInstance();
		cal.set(yyyy,mm ,1);
		//日期减一,取得上月最后一天时间对象
		cal.add(Calendar.DAY_OF_MONTH, -1);
		
		return cal.get(Calendar.DAY_OF_MONTH); 
	}
	
	/**
	 * 获取上一天的信息
	 */
	public static String getPrevDays(){
		//取得系统当前时间
		Calendar cal = Calendar.getInstance();
		//日期减一
		cal.add(Calendar.DAY_OF_MONTH, -1);
		//上月最后一天日期
		return format(cal.getTime(),"yyyyMMdd"); 
	}

	/**
	 * 两个日期之间有几天
	 * 
	 * @param start		开始日期 yyyy-MM-dd HH:mm:ss
	 * @param end		结束日期 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int daysBetween(String start, String end) {
		long time1 = toDate(start, null).getTime();
		long time2 = toDate(end, null).getTime();
		
		int iHalfDay = 0;
		
		while(time2 >= time1){
			iHalfDay++;
			time1 += 1000 * 3600 * 12;
		}

		return iHalfDay;
	}
	
	static final String[] weekDays = {"周日", "周一", "周二", "周三", "周四", "周五", "周六"};
	
	/**
	 * 获得日期所在年的周数
	 * @param date
	 * @return
	 */
	public static String getDayNameOfWeek(Date date){  
		int dayOfWeek = getDayOfWeek(date);
	    return weekDays[dayOfWeek];
	}
	
	/**
	 * 获得日期所在年的周数
	 * @param date
	 * @return
	 */
	public static int getDayOfWeek(Date date){  
	    Calendar calendar = Calendar.getInstance(); 
	    calendar.setFirstDayOfWeek(Calendar.MONDAY);  //设置周一为每周第一天
	    calendar.setTime(date); 
	    int iDay = calendar.get(Calendar.DAY_OF_WEEK) - 1;  
	    if( iDay < 0) iDay=0;
	    return iDay;
	}
	
	/**
	 * stamp1>stamp2 时返回1；stamp1=stamp2时返回0；stamp1<stamp2时返回-1
	 * @param stamp1
	 * @param stamp2
	 * @return
	 */
	public static int compare(Timestamp stamp1, Timestamp stamp2) {
		String str1 = format(stamp1, FORMAT_PATTERN_SHORT);
		String str2 = format(stamp2, FORMAT_PATTERN_SHORT);
		if(str1 == null) {
			return -1;
		} else if(str2 == null) {
			return 1;
		}
		return str1.compareTo(str2);
	}
	
	//===================================  报表统计用开始 ==========================================
	public static final String[] seasons = {"一季度", "二季度", "三季度", "四季度"};
	
	/**
	 * 时间段内有多少个年
	 * @param minDate 开始时间
	 * @param maxDate 结束时间
	 * @return
	 */
	public static List<String> getYears(String minDate, String maxDate)  {
	    List<String> result = new ArrayList<String>();

	    Calendar min = Calendar.getInstance();
	    Calendar max = Calendar.getInstance();

	    min.setTime(toDate(minDate));
	    min.set(min.get(Calendar.YEAR), 0, 1);

	    max.setTime(toDate(maxDate));
	    max.set(max.get(Calendar.YEAR), 0, 2);

	    Calendar curr = min;
		while (curr.before(max)) {
			result.add(format(curr.getTime(), FORMAT_PATTERN_MONTH));
			curr.add(Calendar.YEAR, 1);
		}

	    return result;
	}
	
	/**
	 * 时间段内有多少个季度
	 * @param minDate 开始时间
	 * @param maxDate 结束时间
	 * @return
	 */
	public static List<String> getSeasons(String minDate, String maxDate) {
	    List<String> result = new ArrayList<String>();

	    Calendar min = Calendar.getInstance();
	    Calendar max = Calendar.getInstance();
	    
	    String strmin = getFirstDateOfSeason(toDate(minDate));
	    String strmax = getLastDateOfSeason(toDate(maxDate));

	    min.setTime(toDate(strmin));
	    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

	    max.setTime(toDate(strmax));
	    max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

	    Calendar curr = min;
	    while (curr.before(max)) {
	    	result.add(format(curr.getTime(), FORMAT_PATTERN_MONTH));
		    curr.add(Calendar.MONTH, 3);
	    }

	    return result;
	}
	
	/**
	 * 时间段内有多少个月
	 * @param minDate 开始时间
	 * @param maxDate 结束时间
	 * @return
	 */
	public static List<String> getMonths(String minDate, String maxDate)  {
	    List<String> result = new ArrayList<String>();

	    Calendar min = Calendar.getInstance();
	    Calendar max = Calendar.getInstance();

	    min.setTime(toDate(minDate));
	    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

	    max.setTime(toDate(maxDate));
	    max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

	    Calendar curr = min;
	    while (curr.before(max)) {
	    	result.add(format(curr.getTime(), FORMAT_PATTERN_MONTH));
		    curr.add(Calendar.MONTH, 1);
	    }

	    return result;
	}
	
	/**
	 * 返回时间段内所有日期
	 * @param minDate 开始时间
	 * @param maxDate 结束时间
	 * @return
	 */
	public static List<String> getDays(String minDate, String maxDate)  {
	    List<String> result = new ArrayList<String>();

	    Calendar min = Calendar.getInstance();
	    Calendar max = Calendar.getInstance();

	    min.setTime(toDate(minDate));
//	    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);

	    max.setTime(toDate(maxDate));
//	    max.set(max.get(Calendar.YEAR), max.get(Calendar.MONTH), 2);

	    Calendar curr = min;
	    while (curr.before(max)||curr.equals(max)) {
	    	result.add(format(curr.getTime(), FORMAT_PATTERN_YYYYMMDD));
		    curr.add(Calendar.DAY_OF_MONTH, 1);
	    }

	    return result;
	}
	
	/**
	 * 
	 * @MethodName: getMonths
	 * @Description: 获取区间年月 
	 * @author GongYu
	 * @date 2016年11月2日 上午7:54:46
	 * @param yearMonth
	 * @param months
	 * @return
	 */
	public static List<String> getMonths(String yearMonth, int months)  {
	    List<String> result = new ArrayList<String>();

	    Calendar min = Calendar.getInstance();

	    min.setTime(toDate(yearMonth));
//	    min.set(min.get(Calendar.YEAR), min.get(Calendar.MONTH), 1);
	    Calendar curr = min;
	    for(int i=0;i< months;i++){
	    	result.add(format(curr.getTime(), FORMAT_PATTERN_MONTH));
		    curr.add(Calendar.MONTH, -1);
	    }

	    return result;
	}
	
    /** 
     * 取得日期：年 
     * @param date 
     * @return 
     */  
    public static int getYear(Date date) {  
        Calendar c = Calendar.getInstance();  
        c.setTime(date);  
        int year = c.get(Calendar.YEAR);  
        return year;  
    }
    
    public static String getYearName(Date date) {
    	return getYear(date) + "年";
    }
    
    
    public static String getSeasonName(Date date) {
    	return getYearName(date) +  seasons[getSeason(date) - 1];
    }  
    
    /** 
     *  
     * 1 一季度 2 二季度 3 三季度 4 四季度 
     *  
     * @param date 
     * @return 
     */  
    public static int getSeason(Date date) {  
  
        int season = 0;  
  
        Calendar c = Calendar.getInstance();  
        c.setTime(date);  
        int month = c.get(Calendar.MONTH);  
        switch (month) {  
        case Calendar.JANUARY:  
        case Calendar.FEBRUARY:  
        case Calendar.MARCH:  
            season = 1;  
            break;  
        case Calendar.APRIL:  
        case Calendar.MAY:  
        case Calendar.JUNE:  
            season = 2;  
            break;  
        case Calendar.JULY:  
        case Calendar.AUGUST:  
        case Calendar.SEPTEMBER:  
            season = 3;  
            break;  
        case Calendar.OCTOBER:  
        case Calendar.NOVEMBER:  
        case Calendar.DECEMBER:  
            season = 4;  
            break;  
        default:  
            break;  
        }  
        return season;  
    }  
    
    /** 
     * 取得日期：月 
     * @param date 
     * @return 
     */  
    public static int getMonth(Date date) {  
        Calendar c = Calendar.getInstance();
        c.setTime(date);  
        int month = c.get(Calendar.MONTH);  
        return month + 1;  
    } 
    
    /**
     * 取得日期：月
     * @param date
     * @return
     */
    public static String getMonthName(Date date) {
    	return getMonth(date) + "月";
    }
	
    /**
     * 获取某年第一天日期
     * @param year 年份
     * @return Date
     */
    public static String getFirstDateOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
//        Date currYearFirst = calendar.getTime();
//        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//        String result = sf.format(currYearFirst);
        return format(calendar.getTime(), FORMAT_PATTERN_SHORT);
    }
    
    /**
     * 获取某年最后一天日期
     * @param year 年份
     * @return Date
     */
    public static String getLastDateOfYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.roll(Calendar.DAY_OF_YEAR, - 1);
//        Date currYearLast = calendar.getTime();
//        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//        String result = sf.format(currYearLast); 
        return format(calendar.getTime(), FORMAT_PATTERN_SHORT);
    }
    
    /** 
     * 取得季度第一天 
     *  
     * @param date 
     * @return 
     */  
    public static String getFirstDateOfSeason(Date date) {  
        return getFirstDateOfMonth(getSeasonDate(date)[0]);  
    }
    
    /** 
     * 取得季度最后一天 
     *  
     * @param date 
     * @return 
     */  
    public static String getLastDateOfSeason(Date date) {  
        return getLastDateOfMonth(getSeasonDate(date)[2]);  
    }  
    
    /** 
     * 取得月第一天 
     *  
     * @param date 
     * @return 
     */  
    public static String getFirstDateOfMonth(Date date) {  
        Calendar c = Calendar.getInstance();  
        c.setTime(date);  
        c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));  
//        Date d = c.getTime(); 
//        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//        String result = sf.format(d); 
        return format(c.getTime(), FORMAT_PATTERN_SHORT);
    }  
  
    /** 
     * 取得月最后一天 
     *  
     * @param date 
     * @return 
     */  
    public static String getLastDateOfMonth(Date date) {  
        Calendar c = Calendar.getInstance();  
        c.setTime(date);  
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));  
//        Date d = c.getTime(); 
//        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
//        String result = sf.format(d); 
//        return result;
        return format(c.getTime(), FORMAT_PATTERN_SHORT);
    } 
    
    /** 
     * 取得季度月 
     *  
     * @param date 
     * @return 
     */  
    public static Date[] getSeasonDate(Date date) {  
        Date[] season = new Date[3];  
  
        Calendar c = Calendar.getInstance();  
        c.setTime(date);  
  
        int nSeason = getSeason(date);  
        if (nSeason == 1) {// 第一季度  
            c.set(Calendar.MONTH, Calendar.JANUARY);  
            season[0] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.FEBRUARY);  
            season[1] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.MARCH);  
            season[2] = c.getTime();  
        } else if (nSeason == 2) {// 第二季度  
            c.set(Calendar.MONTH, Calendar.APRIL);  
            season[0] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.MAY);  
            season[1] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.JUNE);  
            season[2] = c.getTime();  
        } else if (nSeason == 3) {// 第三季度  
            c.set(Calendar.MONTH, Calendar.JULY);  
            season[0] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.AUGUST);  
            season[1] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.SEPTEMBER);  
            season[2] = c.getTime();  
        } else if (nSeason == 4) {// 第四季度  
            c.set(Calendar.MONTH, Calendar.OCTOBER);  
            season[0] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.NOVEMBER);  
            season[1] = c.getTime();  
            c.set(Calendar.MONTH, Calendar.DECEMBER);  
            season[2] = c.getTime();  
        }  
        return season;  
    }  
    //===================================  报表统计用结束==========================================
    
	public static void main(String[] agrs) {
//		System.out.println(checkShortDateStr("2010-12-02"));
//		System.out.println(daysBetween("2016-06-20 08:00:00","2016-06-20 20:00:00"));

//		System.out.println(compare(toTimestamp("2010-09-12"), toTimestamp(a)));
//		System.out.println(compare(toTimestamp("2010-09-12"), toTimestamp("")));
//		System.out.println(compare(toTimestamp(""), toTimestamp("2010-09-12")));
//		System.out.println(compare(toTimestamp(a), toTimestamp("2010-09-12")));
//		System.out.println(compare(toTimestamp("2010-09-12"), toTimestamp("2010-09-13")));
//		System.out.println(compare(toTimestamp("2010-09-12"), toTimestamp("2010-09-12")));
//		System.out.println(compare(toTimestamp("2010-09-13"), toTimestamp("2010-09-12")));
		
		String s = getPrevMonth("201502",-4);
		System.out.println(s);
	}
	
	public static List<String[]> getInfo(String str1, String str2, String type) {
		List<String[]> list = new ArrayList<String[]>();
		if("1".equals(type)) {			//年
			List<String> aaa = getYears(str1, str2);
			for (String str : aaa) {
				String[] strs = new String[3];
				Date date = toDate(str);
				strs[0] = getYearName(date);
				strs[1] = getFirstDateOfYear(date);
				strs[2] = getLastDateOfYear(date);
				list.add(strs);
			}
		} else if("2".equals(type)) {	//季度
			List<String> aaa = getSeasons(str1, str2);
			for (String str : aaa) {
				String[] strs = new String[3];
				Date date = toDate(str);
				strs[0] = getSeasonName(date);
				strs[1] = getFirstDateOfSeason(date);
				strs[2] = getLastDateOfSeason(date);
				list.add(strs);
			}
		} else if("3".equals(type)) {	//月
			List<String> aaa = getMonths(str1, str2);
			for (String str : aaa) {
				String[] strs = new String[3];
				Date date = toDate(str);
				strs[0] = getYearName(date)+getMonthName(date);
				strs[1] = getFirstDateOfMonth(date);
				strs[2] = getLastDateOfMonth(date);
				list.add(strs);
			}
		}
		
		return list;
	}
	
	/**
	 * 根据目前的年月获得前后几个月的年月值
	 * 
	 * @param yearMonth
	 * @param prevMonth
	 * @return
	 */
	public static String getPrevMonth(String yearMonth,int prevMonth){
		return DateUtil.format(addMonth(toDate(yearMonth + "01", "yyyyMMdd"), prevMonth), "yyyyMM");
	}
	

}
