package com.icfans.paltform.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public class Phone {

	/** 电话格式验证 **/
	private static final String PHONE_CALL_PATTERN = "^(\\d{3,4}|\\d{3,4}-)?\\d{7,8}$";

	/**
	 * 中国电信号码格式验证 手机段： 133,153,180,181,189,177,1700
	 * **/
	private static final String CHINA_PATTERN = "(^(((13[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))\\d{8})$)";
//	private static final String CHINA_TELECOM_PATTERN = "(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";

	/**
	 * 中国联通号码格式验证 手机段：130,131,132,155,156,185,186,145,176,1709
	 * **/
//	private static final String CHINA_UNICOM_PATTERN = "(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";

	/**
	 * 中国移动号码格式验证
	 * 手机段：134,135,136,137,138,139,150,151,152,157,158,159,182,183,184
	 * ,187,188,147,178,1705
	 * **/
//	private static final String CHINA_MOBILE_PATTERN = "(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";

	/**
	 * 验证电话号码的格式
	 * 
	 * @param str
	 *            校验电话字符串
	 * @return 返回true,否则为false
	 */
	private static boolean isPhoneCallNum(String str) {

		return  match(PHONE_CALL_PATTERN, str);
	}

	
	private static boolean isMobilePhoneNum(String str) {
		return match(CHINA_PATTERN, str);
	}
//	/**
//	 * 验证【电信】手机号码的格式
//	 * 
//	 * @param str
//	 *            校验手机字符串
//	 * @return 返回true,否则为false
//	 */
//	private static boolean isChinaTelecomPhoneNum(String str) {
//
//		return match(CHINA_TELECOM_PATTERN, str);
//	}
//
//	/**
//	 * 验证【联通】手机号码的格式
//	 * 
//	 * @param str
//	 *            校验手机字符串
//	 * @return 返回true,否则为false
//	 */
//	private static boolean isChinaUnicomPhoneNum(String str) {
//
//		return match(CHINA_UNICOM_PATTERN, str);
//	}
//
//	/**
//	 * 验证【移动】手机号码的格式
//	 * 
//	 * @param str
//	 *            校验手机字符串
//	 * @return 返回true,否则为false
//	 */
//	private static boolean isChinaMobilePhoneNum(String str) {
//
//		return match(CHINA_MOBILE_PATTERN, str);
//	}

	/**
	 * 执行正则表达式
	 * 
	 * @param pat
	 *            表达式
	 * @param str
	 *            待验证字符串
	 * @return 返回true,否则为false
	 */
	private static boolean match(String pat, String str) {
		Pattern pattern = Pattern.compile(pat);
		Matcher match = pattern.matcher(str);
		return match.find();
	}
	
	/**
	 * 验证手机
	 * 
	 * @param str
	 *            校验手机字符串
	 * @return 返回true,否则为false
	 */
	public static boolean isPhoneNum(String str) {
		if (StringUtils.isBlank(str)) {
			return false;
		} else {
//			return (isChinaTelecomPhoneNum(str)
//					|| isChinaUnicomPhoneNum(str) 
//					|| isChinaMobilePhoneNum(str)) ? true
//					: false;
			return (isMobilePhoneNum(str)) ? true : false;
		}
	}
	
	/**
	 * 验证电话
	 * @param str
	 * @return
	 */
	public static boolean isHomeNum(String str) {
		if (StringUtils.isBlank(str)) {
			return false;
		} else {
			return isPhoneCallNum(str); 
		}
	}

	public static void main(String[] args) {

		System.out.println(isPhoneNum("177505813691"));
		System.out.println(isPhoneNum("16306061248"));
		System.out.println(isPhoneNum("17750581369"));
		System.out.println(isPhoneNum("17750581369"));
		System.out.println(isPhoneNum("17750581369"));
		System.out.println(isPhoneNum("12750581369"));
		System.out.println(isHomeNum("12333701653"));

	}

}
