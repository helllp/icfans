package com.icfans.paltform.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 进行Cookie的操作工具类
 * 
 * @author LinXM
 *
 */
public class CookieUtil {

	/**
	 * 根据名称读取cookie
	 * @param name		名称
	 * @param request	请求流
	 * @return
	 */
	public static Object getCookie(String name, HttpServletRequest request) {
		Object object = null;
		
		Cookie[] cookie = request.getCookies();
		
		if(cookie == null){
			return object;
		}
		
		for (int i = 0; i < cookie.length; i++) {
			if (cookie[i].getName().equals(name) == true) {
				object = cookie[i].getValue();
				break;
			}
		}
		return object;
	}

	/**
	 * 设置cookie数据
	 * 
	 * @param name			设置cookie名称，必须设置
	 * @param value			设置cookie的值，必须设置
	 * @param cookieTime	设置cookie的存活时间，必须设置
	 * @param domain		设置cookie的域名，可以设置null
	 * @param path			设置cookie适用的路径，可以设置null
	 * @param response		请求返回流
	 */
	public static void setCookie(String name, String value, int cookieTime,
			String domain, String path, HttpServletResponse response) {
		Cookie _cookie = new Cookie(name, value);
		_cookie.setMaxAge(cookieTime);
		
		if(!"".equals(domain) && domain != null){
			_cookie.setDomain(domain);
		}
		
		if(!"".equals(path) && path != null){
			_cookie.setPath(path);
		}
		
		response.addCookie(_cookie);
	}
}
