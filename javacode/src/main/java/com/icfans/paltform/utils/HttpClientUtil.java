package com.icfans.paltform.utils;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 * HttpClientUtil
 *
 */
public class HttpClientUtil {
	
	/**
	 * 获取URL的内容，内容是UTF-8编码
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String GetMethod(String url) throws Exception{
		CloseableHttpClient httpClient = HttpClients.custom().build();
		
		HttpGet httpGet = new HttpGet(url);
		
		byte[] dataByte = null;
		
		// 执行请求
		HttpResponse httpResponse = httpClient.execute(httpGet);
		// 获取返回的数据
		HttpEntity httpEntity = httpResponse.getEntity();
		if (httpEntity != null) {
			byte[] responseBytes = getData(httpEntity);
			dataByte = responseBytes;
			httpGet.abort();
		}
		// 将字节数组转换成为字符串
		String result = bytesToString(dataByte);
		return result;
	}
	
	/**
	 * 以Post方法访问
	 * 
	 * @param url
	 *            请求地址
	 * @param argsMap
	 *            携带的参数
	 * @param content
	 *            内容
	 * @return String 返回结果
	 * @throws Exception
	 */
	public static String POSTMethod(String url, Map<String, Object> argsMap,
			String content) throws Exception {
		byte[] dataByte = null;
		
	    CloseableHttpClient httpClient = HttpClients.custom().build();
	    
		HttpPost httpPost = new HttpPost(url);
		
		httpPost.addHeader("Content-type","application/json");  
		//httpPost.setHeader("Accept", "application/json");  
         
		if (MapUtils.isNotEmpty(argsMap)) {
			// 设置参数
			UrlEncodedFormEntity encodedFormEntity = new UrlEncodedFormEntity(
					setHttpParams(argsMap), "UTF-8");
			httpPost.setEntity(encodedFormEntity);
		}
		if (StringUtils.isNotEmpty(content)) {
			httpPost.setEntity(new ByteArrayEntity(content.getBytes()));
		}
		// 执行请求
		HttpResponse httpResponse = httpClient.execute(httpPost);
		// 获取返回的数据
		HttpEntity httpEntity = httpResponse.getEntity();
		if (httpEntity != null) {
			byte[] responseBytes = getData(httpEntity);
			dataByte = responseBytes;
			httpPost.abort();
		}
		// 将字节数组转换成为字符串
		String result = bytesToString(dataByte);
		return result;
	}
	
	/**
	 * 设置HttpPost请求参数
	 * 
	 * @param argsMap
	 * @return BasicHttpParams
	 */
	private static List<BasicNameValuePair> setHttpParams(
			Map<String, Object> argsMap) {
		List<BasicNameValuePair> nameValuePairList = new ArrayList<BasicNameValuePair>();
		// 设置请求参数
		if (argsMap != null && !argsMap.isEmpty()) {
			Set<Entry<String, Object>> set = argsMap.entrySet();
			Iterator<Entry<String, Object>> iterator = set.iterator();
			while (iterator.hasNext()) {
				Entry<String, Object> entry = iterator.next();
				BasicNameValuePair basicNameValuePair = new BasicNameValuePair(
						entry.getKey(), entry.getValue().toString());
				nameValuePairList.add(basicNameValuePair);
			}
		}
		return nameValuePairList;
	}
	
	/**
	 * 获取Entity中数据
	 * 
	 * @param httpEntity
	 * @return
	 * @throws Exception
	 */
	private static byte[] getData(HttpEntity httpEntity) throws Exception {
		BufferedHttpEntity bufferedHttpEntity = new BufferedHttpEntity(
				httpEntity);
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		bufferedHttpEntity.writeTo(byteArrayOutputStream);
		byte[] responseBytes = byteArrayOutputStream.toByteArray();
		return responseBytes;
	}
	
	/**
	 * 将字节数组转换成字符串
	 * 
	 * @param bytes
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	private static String bytesToString(byte[] bytes)
			throws UnsupportedEncodingException {
		if (bytes != null) {
			String returnStr = new String(bytes, "utf-8");
			returnStr = StringUtils.trim(returnStr);
			return returnStr;
		}
		return null;
	}
}
