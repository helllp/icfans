package com.icfans.paltform.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.icfans.paltform.common.Define;



/**
 * 转换工具类
 * 
 * 
 */
public class ConvertUtil {

	/**
	 * 用于进行用户登录认证Token的生成
	 * 
	 * @return
	 */
	public static String createToken(){
		return UUID.randomUUID().toString().replaceAll("-", "");	
	}
	
	/**
	 * 保留2为小数，如果为null，返回默认值
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static String Round2(Double value, String defaultValue) {
		if (value == null) return defaultValue;
		return String.format("%.2f", value);
	}
	
	/**
	 * 保留2为小数，如果为null，返回默认值(适合数字类型)
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static String Round2c(Object value, String defaultValue) {
		if (value == null) {
			return defaultValue;
		} else if(!(value instanceof Number)) {
			return "";
		}
		return String.format("%.2f", value);
	}

	/**
	 * 保留2为小数
	 * @param value
	 * @return
	 */
	public static String Round2(Double value) {
		if (value == null) return null;
		return String.format("%.2f", value);
	}
	
	/**
	 * 保留1为小数
	 * @param value
	 * @return
	 */
	public static String Round1(Double value) {
		if (value == null) return null;
		return String.format("%.1f", value);
	}
	
	/**
	 * 保留1为小数，如果为null，返回默认值(适合数字类型)
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static String Round1c(Object value, String defaultValue) {
		if (value == null) {
			return defaultValue;
		} else if(!(value instanceof Number)) {
			return "";
		}
		return String.format("%.1f", value);
	}
	
	/**
	 * 保留2为小数
	 * @param value
	 * @return
	 */
	public static BigDecimal Round2(BigDecimal value) {
		if (value == null ) return null;
		return value.setScale(2, RoundingMode.HALF_UP);		
	}
	
	/**
	 * 保留1为小数
	 * @param value
	 * @return
	 */
	public static BigDecimal Round1(BigDecimal value) {
		if (value == null ) return null;
		return value.setScale(1, RoundingMode.HALF_UP);		
	}
	
	/**
	 * Object返回对应字符串，如果为null返回默认值
	 * @param value
	 * @param defaultValue
	 * @return
	 */
	public static String toString(Object value,String defaultValue) {
		if (value == null) return defaultValue;
		return value.toString();
	}
	/**
	 * Object返回对应字符串，如果为null返回null
	 * @param value
	 * @return
	 */
	public static String toString(Object value) {
		if (value == null) return null;
		return value.toString();
	}

	/**
	 * Integer类型转换为Long类型
	 * 注：自动转换只会发生在long和int(或Integer)之间
	 * @param value
	 * @return
	 */
	public static Long toLong(Integer value) {
		if (value == null)
			return null;
		return value.longValue();
	}

	/**
	 * BigInteger类型转换为Long类型
	 * @param value
	 * @return
	 */
	public static Long toLong(BigInteger value) {
		if (value == null)
			return null;
		return value.longValue();
	}

	/**
	 * String类型转换为Long类型
	 * @param value
	 * @return
	 */
	public static Long toLong(String value) {
		if (value == null || "".equals(value))
			return null;
		return Long.valueOf(value);
	}
	
	/**
	 * Object类型转换为Long类型
	 * @param value 只能是Integer或BigInteger
	 * @return
	 */
	public static Long toLong(Object value) {
		if (value == null) return null;
		
		if (value instanceof Integer) {
			return ((Integer) value).longValue();
		}
		if (value instanceof BigInteger) {
			return ((BigInteger) value).longValue();
		}
		
		Long i = (Long) value;
		return i.longValue();
	}

	/**
	 * String类型转换为Integer类型
	 * @param value
	 * @return
	 */
	public static Integer toInt(String value) {
		if (value == null || "".equals(value))
			return null;
		return Integer.valueOf(value);
	}
	
	/**
	 * Object类型转换为Integer类型
	 * @param value 只能是Long，BigInteger，BigDecimal或Double
	 * @return
	 */
	public static Integer toInt(Object value) {
		if (value == null) return null;

		if (value instanceof Long) {
			return ((Long) value).intValue();
		}
		if (value instanceof BigInteger) {
			return ((BigInteger) value).intValue();
		}
		if (value instanceof BigDecimal) {
			return ((BigDecimal) value).intValue();
		}
		if (value instanceof Double) {
			return ((Double) value).intValue();
		}
		Integer i = (Integer) value;
		return i;
	}
	
	public static Double toDouble(String value) {
		if (value == null)
			return null;
		return Double.valueOf(value);
	}
	
	public static Double toDouble(Object value) {
		if (value == null)
			return null;
		Double d = (Double) value;
		return d;
	}
	
	public static BigDecimal toIntBigDecimal(Object value) {
		if (value == null ) return null;
		
		String val = value.toString();
		if ("".equals(value)) return null;
		
		return new BigDecimal(val);
	}
	
	public static List<BigDecimal> StringToBigDecList(String ids) {
		String[] idsArray = StringUtils.split(ids, ",");
		List<BigDecimal> idList = new ArrayList<BigDecimal>();
		for (String id : idsArray) {
			if ( !idList.contains(ConvertUtil.toIntBigDecimal(id)) ) {
				idList.add(ConvertUtil.toIntBigDecimal(id));
			}
		}
		
		return idList;
	}
	
	/**
	 * 自动截取字符串，后面接...
	 * 注：HTML有对应标签
	 * @param value
	 * @param length
	 * @return
	 */
	@Deprecated
	public static String smartSubstring(String value,Integer length) {
		String str = value.length() <= length ? value : value.substring(0, length) + "…";
		return str;
	}
	
	/**
	 * 自动链接List中元素，返回以逗号链接的字符串
	 * @param list
	 * @return
	 */
	public static String smartCombine(List<?> list) {
		return smartCombine(list,",");
	}
	
	/**
	 * 自动链接List中元素，返回以指定分隔符链接的字符串
	 * @param list
	 * @return
	 */
	public static String smartCombine(List<?> list,String Separation) {
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0 ; i < list.size(); i++) {
		sb.append(list.get(i).toString());
			sb.append((i+1) == list.size() ? "" : Separation);
		}
		return sb.toString();
	}
	
	public static boolean isEmptyString(String src){
		if(src == null || "".equals(src)){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 创建UUID，返回hashcode
	 * @return
	 */
	public static String createUUID(){
		return "" + UUID.randomUUID().hashCode();
	}
	
	/**
	 * 
	 * @MethodName: firstToLowerCase
	 * @Description: 将字符串的首字母转换成小写 
	 * @author GongYu
	 * @date 2016年5月31日 下午2:40:48
	 * @param str
	 * @return
	 */
	public static String firstToLowerCase(String str){
		char[] chars=new char[1];  
		chars[0]=str.charAt(0);  
		String temp=new String(chars); 
		if(chars[0]>='A'  &&  chars[0]<='Z')  
        {  
			return str = str.replaceFirst(temp,temp.toLowerCase());  
        }else{
        	return str;
        }
	}
	
	/**
     * 全角转半角
     * @param input String.
     * @return 半角字符串
     */
    public static String ToDBC(String input) {
    	if(StringUtils.isBlank(input)) {
    		return "";
    	}
        char c[] = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
    	    if (c[i] == '\u3000') {
    	    	c[i] = ' ';
            } else if (c[i] > '\uFF00' && c[i] < '\uFF5F') {
            	c[i] = (char) (c[i] - 65248);
            }
        }
        return  new String(c);
    }
    
    /**
     * 半角转全角
     * @param input String
     * @return 全角字符串
     */
    public static String ToSBC(String input) {
    	if(StringUtils.isBlank(input)) {
    		return "";
    	}
    	char c[] = input.toCharArray();
        for (int i = 0; i < c.length; i++) {
        	if (c[i] == ' ') {
        		c[i] = '\u3000';
            } else if (c[i] < '\177') {
                c[i] = (char) (c[i] + 65248);

            }
        }
        return new String(c);
    }
    
    /**  
     * 检查浮点数  
     * @param value 
     * @param type "0+":非负浮点数； "+":正浮点数 ；"-0":非正浮点数； "-":负浮点数； "":浮点数  
     * @return 满足：true；不满足：false
     */  
    public static boolean checkFloat(Object value, String type){  
    	String num = toString(value);
    	if(StringUtils.isBlank(num)) {
    		return false;
    	}
		String eL = "";
		if ("0+".equals(type)) {
			eL = "^\\d+(\\.\\d+)?$";												 	// 非负浮点数
		} else if ("+".equals(type)) {
			eL = "^((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*))$";	// 正浮点数
		} else if ("-0".equals(type)) {
			eL = "^((-\\d+(\\.\\d+)?)|(0+(\\.0+)?))$";									// 非正浮点数
		} else if ("-".equals(type)) {
			eL = "^(-((\\d+\\.\\d*[1-9]\\d*)|(\\d*[1-9]\\d*\\.\\d+)|(\\d*[1-9]\\d*)))$";// 负浮点数
		} else {
			eL = "^(-?\\d*)(\\.\\d+)?$";												// 浮点数
		}																				
		Pattern p = Pattern.compile(eL);
		Matcher m = p.matcher(num);
		boolean b = m.matches();
		return b;
    }
    
    /**
     * 验证字符串是否是数字
     * @param str 字符串
     * @return true ： 是数字 ；false：不是数字
     */
    public static boolean checkNumber(String str) {
    	if(StringUtils.isBlank(str)) {
    		return false;
    	}
    	
		Pattern pattern = Pattern.compile("-?[0-9]*");   
		Matcher matcher = pattern.matcher(str);   
		          
	    return matcher.matches();
    }
    
    /**
     * 验证字符串是否是手机号
     * @param str 字符串
     * @return true ： 是手机号 ；false：不是手机号
     */
    public static boolean checkMobile(String str) {
		return Phone.isPhoneNum(str);
    }
    
    /**
     * 验证字符串是否是家庭电话
     * @param str 字符串
     * @return true ： 是家庭电话 ；false：不是家庭电话
     */
    public static boolean checkCallHome(String str) {
    	return Phone.isHomeNum(str);
    }
    
    /**
     * 字符串自动补零和小数点
     * @param str 字符串
     * @param i 小数位数
     * @return 格式后的字符串
     */
    public static String formatData(String str, int i) {
		String result = "";
		if(StringUtils.isBlank(str)) {
			result = "-";
		} else {
			if(checkFloat(str, "")) {
				String pointZero = String.format("%1$0" + i + "d", 0);
				String pattern = "#0." + pointZero;
				DecimalFormat df = new DecimalFormat(pattern);
				result = df.format(Float.valueOf(str));
			} else {
				result = str;
			}
		}
		return result;
	}
    
    /**
     * 去除字符串尾部的字符
     * exceptCh("1110001000", '0') 结果【1110001】
     * @param str 字符串
     * @param ch 字符
     * @return
     */
    public static String exceptCh(String str,char ch) {
		if(str == null) {
			return null;
		}
		char[] chs = str.toCharArray();
		int index = chs.length;
		for(;index > 0 && chs[index-1] == ch; index--);
		return str.substring(0, index);
    }
    
    /**
     * 
     * @MethodName: formatDBName
     * @Description:  格式化处理表名和字段名：转为小写，去掉下划线，首字母大写
     * @author YAO
     * @date 2016年9月22日 上午8:36:59
     * @param s
     * @param initCap true－整个名字首字母大写（类名,方法名）；false－整个名字首字母不大写（字段名）
     * @return
     */
    public static String formatDBName(String s, boolean initCap){
        StringBuffer name = new StringBuffer();
        String[] tmp = s.trim().toLowerCase().split("_");
        for(int i=0; i<tmp.length; i++)
            name.append(tmp[i].substring(0, 1).toUpperCase()).append(tmp[i].substring(1));

        if(initCap){
            return name.toString();
        }else{
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }
    }

    /**
     * 返回每页的显示数量
     * 
     * @param pageSize
     * @return
     */
    public static int getPageSize(int pageSize){
    	return pageSize > Define.MAX_PAGE_SIZE ? Define.MAX_PAGE_SIZE : pageSize;
    }
}
