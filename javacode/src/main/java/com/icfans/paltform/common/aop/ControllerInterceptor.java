package com.icfans.paltform.common.aop;

import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.icfans.paltform.common.execption.IcfansSystemException;
import com.icfans.paltform.common.vo.ResultEnum;
import com.icfans.paltform.common.vo.ResultGenerator;


/** 
 * 拦截器：记录用户操作日志 
 */  
@Aspect
@Component  
public class ControllerInterceptor {  
    private static final Logger logger = LoggerFactory.getLogger(ControllerInterceptor.class);  
      
    @Value("${spring.profiles.active}")  
    private String env;  
     
	@Autowired
	private ResultGenerator generator;
	
    /** 
     * 定义拦截规则：拦截logic包下面的所有类中，有@RequestMapping注解的方法。 
     */  
    @Pointcut("execution(* com.icfans.paltform.logic..*(..)) and @annotation(org.springframework.web.bind.annotation.RequestMapping)")  
    public void controllerMethodPointcut(){}  
      
    @Before("controllerMethodPointcut()")
    public void before(JoinPoint joinPoint) {
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = joinPoint.getSignature().getName();
        logger.info(className + "的" + methodName + "执行了");
        
        //	保存所有请求参数，用于输出到日志中
        Set<Object> allParams = new LinkedHashSet<>();
        
        Object[] args = joinPoint.getArgs();
        
        for(Object arg : args){  
            if (arg instanceof Map<?, ?>) {  
                //提取方法中的MAP参数，用于记录进日志中  
                @SuppressWarnings("unchecked")  
                Map<String, Object> map = (Map<String, Object>) arg;  
  
                allParams.add(map);  
            }else if(arg instanceof HttpServletRequest){  
                HttpServletRequest request = (HttpServletRequest) arg;  
                  
                //获取query string 或 posted form data参数  
                Map<String, String[]> paramMap = request.getParameterMap();  
                if(paramMap!=null && paramMap.size()>0){  
                    allParams.add(paramMap);  
                }  
            }else if(arg instanceof HttpServletResponse){  
                //do nothing...  
            }else{  
                allParams.add(arg);  
            }  
        }  
          
        logger.info("请求参数：{}",JSON.toJSONString(allParams));
    }

    @AfterReturning(value = "controllerMethodPointcut()", returning = "returnVal")
    public void afterReturin(Object returnVal) {
        logger.info("方法正常结束了,方法的返回值:" + returnVal);
    }
    
    /** 
     * 拦截器具体实现 
     * @param pjp 
     * @return 
     */  
    @Around("controllerMethodPointcut()")   
    public Object Interceptor(ProceedingJoinPoint pjp){  
          
        MethodSignature signature = (MethodSignature) pjp.getSignature();  
        Method method = signature.getMethod(); //获取被拦截的方法  
        String methodName = method.getName(); //获取被拦截的方法名  
          
        Object result = null;  
 
        long beginTime = System.currentTimeMillis();
        
        try {  
            if(result == null){  
                // 一切正常的情况下，继续执行被拦截的方法  
                result = pjp.proceed();  
            }  
        }catch (IcfansSystemException e){
        	result = generator.genErrorResult(ResultEnum.RETURN_SYS_ERROR.getCode(), e.getMsg());
        }catch (Throwable e) {  
            logger.error("exception: ", e);  
            throw new IcfansSystemException(e.getMessage());
        } 
          
        long costMs = System.currentTimeMillis() - beginTime;  
        logger.info("{}请求结束，耗时：{}ms", methodName, costMs);  
          
        return result;  
    }  
      
}  