package com.icfans.paltform.common.vo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import com.icfans.paltform.common.Define;
import com.icfans.paltform.core.service.LocaleMessageSourceService;

@Component
public class ResultGenerator {
	@Autowired
	private LocaleMessageSourceService messageService;
	
	private static String getKey(int code){
		return Define.RETURN_INFO_HEAD + code;
	}
	
	private String getValue(int code){
		return messageService.getMessage(getKey(code));
	}
	
    public Result genSuccessResult() {
        return new Result()
                .setCode(ResultEnum.RETURN_NORMAL.getCode())
                .setMessage(getValue(ResultEnum.RETURN_NORMAL.getCode()));
    }

    public Result genSuccessResult(Object data) {
        return new Result()
                .setCode(ResultEnum.RETURN_NORMAL.getCode())
                .setMessage(getValue(ResultEnum.RETURN_NORMAL.getCode()))
                .setData(data)
                .setType(data.getClass().getName());
    }

    public Result genErrorResult(int code) {
        return new Result()
                .setCode(code)
                .setMessage(getValue(code));
    } 
    
    public Result genErrorResult(int code, String message) {
        return new Result()
                .setCode(code)
                .setMessage(message);
    } 
    
    public Result genCheckResult(BindingResult checkRusult) {
		if (checkRusult.hasErrors()) {
			List<ObjectError> list = checkRusult.getAllErrors();

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < list.size(); i++) {
				if (i > 0) {
					sb.append(",");
				}
				sb.append(list.get(i).getDefaultMessage());
			}

			//	输入校验失败
			return genErrorResult(ResultEnum.RETURN_INPUT_ERROR.getCode(),sb.toString());
		}else{
			return genSuccessResult();
		}
    }
}
