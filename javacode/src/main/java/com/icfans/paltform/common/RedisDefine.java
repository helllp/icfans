package com.icfans.paltform.common;

import java.util.HashMap;
import java.util.Map;

/**
 * redis缓存中使用的key
 * 
 * @author 林晓明
 *
 */
public class RedisDefine {
	/**
	 * 保持永久有效
	 */
	public static final String KeepValid = "KeepValid";
	public static final String No003 = "No003";
	public static final String NO030 = "NO030";
	public static final String NO031 = "NO031";
	public static final String NO032 = "NO032";
	public static final String NO033 = "NO033";
	
	/**
	 * 缓存的Key
	 */
	private static final String[] CACHE_KEY_ARR = {
			KeepValid,
			No003,
			NO030,
			NO031,
			NO032,
			NO033
	};
	
	/**
	 * 缓存对应的失效时间
	 */
	private static final long[] CACHE_EXPRISE_ARR = {
			Integer.MAX_VALUE,
			Integer.MAX_VALUE,
			30,
			30,
			30,
			30
	};
	
	/**
	 * 构造失效列表
	 * 
	 * @return
	 */
	public static Map<String,Long> createExpriseMap(){
		Map<String, Long> map = new HashMap<String, Long>();
		
		for(int i=0; i<CACHE_KEY_ARR.length; i++){
			map.put(CACHE_KEY_ARR[i], CACHE_EXPRISE_ARR[i]);
		}
		
		return map;
	}
}
