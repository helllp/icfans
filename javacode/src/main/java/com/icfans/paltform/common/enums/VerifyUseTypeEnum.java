package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 验证码用途类型枚举
 * 
 * @author 林晓明
 *
 */
public enum VerifyUseTypeEnum implements IEnumRuleCheck, Serializable{
	登录验证(0),
	修改资料(1),
	更改手机(2),
	
	;

	private int value;
	
	private VerifyUseTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(VerifyUseTypeEnum e : VerifyUseTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(VerifyUseTypeEnum e : VerifyUseTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(VerifyUseTypeEnum e : VerifyUseTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
