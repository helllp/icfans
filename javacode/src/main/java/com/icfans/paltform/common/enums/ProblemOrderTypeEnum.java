package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 问题排序枚举
 * 
 * @author 林晓明
 *
 */
public enum ProblemOrderTypeEnum implements IEnumRuleCheck, Serializable{
	有解答(0),
	更新时间(1),
	评论数量(2),
//	按收藏量(3),
//	按发布时间(4),
	
	;

	private int value;
	
	private ProblemOrderTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(ProblemOrderTypeEnum e : ProblemOrderTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(ProblemOrderTypeEnum e : ProblemOrderTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(ProblemOrderTypeEnum e : ProblemOrderTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
