package com.icfans.paltform.common.inters;

/**
 * 发送短信的接口
 * 
 * @author 林晓明
 *
 */
public interface ISmsService {
	/**
	 * 给指定手机发送短信
	 * 
	 * @param phone
	 * @param text
	 * @return
	 */
	public String sendSms(String phone, String text);
	
	/**
	 * 发送短信给批量手机号
	 * 
	 * @param phones
	 * @param text
	 * @return
	 */
	public String sendBatchSms(String[] phones, String text);
}
