package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 专家查询方法枚举
 * 
 * @author 林晓明
 *
 */
public enum MasterSearchTypeEnum implements IEnumRuleCheck, Serializable{
	领域查询(0),
	姓名查询(1),
	
	;

	private int value;
	
	private MasterSearchTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(MasterSearchTypeEnum e : MasterSearchTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(MasterSearchTypeEnum e : MasterSearchTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(MasterSearchTypeEnum e : MasterSearchTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
