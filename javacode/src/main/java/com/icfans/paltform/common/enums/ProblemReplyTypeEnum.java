package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 问题的回复类型枚举
 * 
 * @author 林晓明
 *
 */
public enum ProblemReplyTypeEnum implements IEnumRuleCheck, Serializable{
	回复问题信息(0),
	回复具体的人(1),
	
	;

	private int value;
	
	private ProblemReplyTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(ProblemReplyTypeEnum e : ProblemReplyTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(ProblemReplyTypeEnum e : ProblemReplyTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(ProblemReplyTypeEnum e : ProblemReplyTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
