package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

public enum SexTypeEnum implements IEnumRuleCheck, Serializable{
	MALE(1),
	FEMALE(2),
	UNKNOWN(-1),
	
	;

	private int value;
	
	private SexTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(SexTypeEnum e : SexTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(SexTypeEnum e : SexTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(SexTypeEnum e : SexTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
