package com.icfans.paltform.common.vo;

/**
 * 错误码信息
 *
 * @author 林晓明
 *
 */
public enum ResultEnum {
	RETURN_NORMAL(0, "返还值正常"),
	RETURN_INPUT_ERROR(1, "输入校验失败"),
	RETURN_UNAUTHORIZED(2, "未授权"),
	RETURN_NOT_LOGIN(3, "用户未登录"),
	RETURN_SYS_ERROR(4, "发生系统异常"),
	RETURN_USER_SMS_ERROR(5, "发送短信过于频繁"),
	RETURN_UPDATE_USER_ERROR(6, "更新失败"),
	RETURN_USER_REGISTER_ERROR(7, "用户注册失败"),
	RETURN_UPLOAD_PHOTO_ERROR(8, "上传图片失败"),
	RETURN_VARIFY_CODE_ERROR(9, "手机验证码校验失败"),
	RETURN_VARIFY_TOKEN_ERROR(10, "Token校验失败 "),
	RETURN_PAGE_404_ERROR(11, "404不存在的资源请求"),
	RETURN_PAGE_500_ERROR(12, "服务器发生错误"),
	RETURN_USER_FINDINFO_ERROR(13, "查询信息失败"),
	RETURN_INSERT_ERROR(13, "添加信息失败"),
	;
	
	private int code;
	private String message;
	
	ResultEnum(int code, String message){
		this.code = code;
		this.message = message;
	}
	
	public int getCode() {
		return this.code;
	}

	public String getMessage() {
		return message;
	}
}
