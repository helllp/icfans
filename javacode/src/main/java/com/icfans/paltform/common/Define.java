package com.icfans.paltform.common;

/**
 * 定义系统级常量
 * 
 * @author 林晓明
 *
 */
public class Define {
	
	/**
	 * 默认的redis缓存过期时间，单位为秒
	 */
	public static final int REDIS_EXPIRE = 60; 
	
	/**
	 * 返回信息头
	 */
	public static final String RETURN_INFO_HEAD = "RETURN_INFO_";
	
	/**
	 * 手机验证码的有效时间
	 */
	public static final long REDIS_PHONECODE = 30;
	
	/**
	 * 缓存中存手机号及发送短信次数的有效时间
	 */
	public static final long REDIS_SENDINFO_COUNT = 60;
	
	/**
	 * 登录Token的有效时间
	 */
	public static final long REDIS_TOKEN_EXPIRETIME = 7*24*60*60;
	
	/**
	 * 用于构造手机检测验证码生成频次的reids key
	 */
	public static final String REDIS_VERIFY_TIMES_KEY = "_code";
	
	/**
	 * 所有请求都需要带的验证信息
	 */
	public static final String TOKEN_PARAM_NAME = "token";
	
	/**
	 * 校验token失败的时候，返回的地址
	 */
	public static final String COMMON_URL_TOKEN = "/comm/token";
	
	public static final String COMMON_URL_500 = "/comm/500";
	
	public static final String COMMON_URL_404 = "/comm/404";
	
	/**
	 * 默认的最大每页数量
	 */
	public static final int MAX_PAGE_SIZE = 200;
}
