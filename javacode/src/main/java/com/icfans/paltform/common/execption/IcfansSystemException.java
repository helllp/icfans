package com.icfans.paltform.common.execption;

/**
 * 发生系统异常
 * 
 * @author 林晓明
 *
 */
public class IcfansSystemException extends RuntimeException {

	private static final long serialVersionUID = -6300541211595790930L;

    /**
     * 错误信息
     */
    private String msg;

	public IcfansSystemException(String msg) {
        this.msg = msg;
    }

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
