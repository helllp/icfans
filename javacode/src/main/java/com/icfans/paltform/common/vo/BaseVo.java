package com.icfans.paltform.common.vo;

import com.alibaba.fastjson.JSON;

/**
 * 所有输入，输出的基类
 * 
 * @author 林晓明
 *
 */
public class BaseVo {
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
