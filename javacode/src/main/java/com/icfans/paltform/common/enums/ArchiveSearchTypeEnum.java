package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 文章查询方法枚举
 * 
 * @author 林晓明
 *
 */
public enum ArchiveSearchTypeEnum implements IEnumRuleCheck, Serializable{
	领域(0),
	姓名(1),
	发布者ID(2),
	推荐文献(3),	//	无查询条件，最新的在前
	
	;

	private int value;
	
	private ArchiveSearchTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(ArchiveSearchTypeEnum e : ArchiveSearchTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(ArchiveSearchTypeEnum e : ArchiveSearchTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(ArchiveSearchTypeEnum e : ArchiveSearchTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
