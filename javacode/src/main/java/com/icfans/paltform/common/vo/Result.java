package com.icfans.paltform.common.vo;

import com.alibaba.fastjson.JSON;

/**
 * 统一API响应结果封装
 * 
 * @author 林晓明
 *
 */
public class Result {
	/**
	 * 错误码
	 */
    private int code;
    
    /**
     * 错误信息
     */
    private String message;
    
    /**
     * 返回的数据
     */
    private Object data;

    /**
     * 返回的数据类型
     */
    private String type;
    
    public boolean hasError(){
    	return this.code == ResultEnum.RETURN_NORMAL.getCode() ? true : false;
    }
    
    public Result setCode(ResultEnum resultCode) {
        this.code = resultCode.getCode();
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    public String getType() {
		return type;
	}

	public Result setType(String type) {
		this.type = type;
		return this;
	}

	@Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
