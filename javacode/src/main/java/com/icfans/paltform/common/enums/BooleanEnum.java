package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 用户类型枚举
 * 
 * @author 林晓明
 *
 */
public enum BooleanEnum implements IEnumRuleCheck, Serializable{
	TRUE(1),
	FALSE(0),
	
	;

	private int value;
	
	private BooleanEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(BooleanEnum e : BooleanEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(BooleanEnum e : BooleanEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(BooleanEnum e : BooleanEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
