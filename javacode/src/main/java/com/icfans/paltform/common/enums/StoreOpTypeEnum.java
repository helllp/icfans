package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 收藏/关注操作的方法枚举
 * 
 * @author 林晓明
 *
 */
public enum StoreOpTypeEnum implements IEnumRuleCheck, Serializable{
	操作(0),
	取消(1),
	
	;

	private int value;
	
	private StoreOpTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(StoreOpTypeEnum e : StoreOpTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(StoreOpTypeEnum e : StoreOpTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(StoreOpTypeEnum e : StoreOpTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
