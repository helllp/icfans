package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 专家查询排序枚举
 * 
 * @author 林晓明
 *
 */
public enum MasterOrderTypeEnum implements IEnumRuleCheck, Serializable{
	关注度(0),
	文献数(1),
	
	;

	private int value;
	
	private MasterOrderTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(MasterOrderTypeEnum e : MasterOrderTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(MasterOrderTypeEnum e : MasterOrderTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(MasterOrderTypeEnum e : MasterOrderTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
