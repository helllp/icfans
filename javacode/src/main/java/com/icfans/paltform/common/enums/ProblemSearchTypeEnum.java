package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 问题查询方法枚举
 * 
 * @author 林晓明
 *
 */
public enum ProblemSearchTypeEnum implements IEnumRuleCheck, Serializable{
	提出者ID(0),
	解答者ID(1),
	收藏者ID(2),
	
	;

	private int value;
	
	private ProblemSearchTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(ProblemSearchTypeEnum e : ProblemSearchTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(ProblemSearchTypeEnum e : ProblemSearchTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(ProblemSearchTypeEnum e : ProblemSearchTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
