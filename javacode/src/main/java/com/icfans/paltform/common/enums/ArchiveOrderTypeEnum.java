package com.icfans.paltform.common.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.icfans.paltform.core.check.IEnumRuleCheck;

/**
 * 文章查询排序枚举
 * 
 * @author 林晓明
 *
 */
public enum ArchiveOrderTypeEnum implements IEnumRuleCheck, Serializable{
	按阅读量(0),
	按发布时间(1),

	
	;

	private int value;
	
	private ArchiveOrderTypeEnum(int value){
		this.value = value;
	}
	
	@Override
	public boolean check(int value) {
		for(ArchiveOrderTypeEnum e : ArchiveOrderTypeEnum.values()){
			if(value == e.value){
				return true;
			}
		}
		return false;
	}

	@Override
	public String getName(int value) {
		for(ArchiveOrderTypeEnum e : ArchiveOrderTypeEnum.values()){
			if(value == e.value){
				return e.name();
			}
		}
		return "";
	}

	@Override
	public Map<Integer, String> getList() {
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		for(ArchiveOrderTypeEnum e : ArchiveOrderTypeEnum.values()){
			map.put(e.value, e.name());
		}

		return map;
	}

	@Override
	public int getValue() {
		return this.value;
	}
	

}
