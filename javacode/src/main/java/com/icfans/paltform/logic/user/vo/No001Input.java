package com.icfans.paltform.logic.user.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.UserTypeEnum;
import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.core.check.CheckEnum;

public class No001Input extends BaseVo {
	/**
	 * 登录手机号
	 */
	@NotEmpty(message = "手机号不能为空")
	@Pattern(regexp="((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0-9]))\\d{8}", message="手机号不正确")
	private String phone;

	/**
	 * 登录验证码
	 */
	@NotEmpty(message = "验证码不能为空")
	@Pattern(regexp="[0-9]\\d{5}", message="验证码不正确")
	private String verifyCode;

	/**
	 * 用户类型
	 */
	@NotNull(message = "用户类型不能为空")
	@CheckEnum(UserTypeEnum.class)
	private int userType;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getVerifyCode() {
		return verifyCode;
	}

	public void setVerifyCode(String verifyCode) {
		this.verifyCode = verifyCode;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
}
