package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No008Output extends BaseVo {
	/**
	 * 有回答时通知我
	 */
	private boolean answerNoticeFlag;
	
	/**
	 * 有人回复时通知我
	 */
	private boolean replyNoticeFlag;
	
	/**
	 * 关注的人有新动态通知我
	 */
	private boolean activityNoticeFlag;
	
	/**
	 * 有新评论时通知我
	 */
	private boolean discussNoticeFlag;
	
	/**
	 * 有新关注时通知我
	 */
	private boolean noticedNoticeFlag;

	public boolean isAnswerNoticeFlag() {
		return answerNoticeFlag;
	}

	public void setAnswerNoticeFlag(boolean answerNoticeFlag) {
		this.answerNoticeFlag = answerNoticeFlag;
	}

	public boolean isReplyNoticeFlag() {
		return replyNoticeFlag;
	}

	public void setReplyNoticeFlag(boolean replyNoticeFlag) {
		this.replyNoticeFlag = replyNoticeFlag;
	}

	public boolean isActivityNoticeFlag() {
		return activityNoticeFlag;
	}

	public void setActivityNoticeFlag(boolean activityNoticeFlag) {
		this.activityNoticeFlag = activityNoticeFlag;
	}

	public boolean isDiscussNoticeFlag() {
		return discussNoticeFlag;
	}

	public void setDiscussNoticeFlag(boolean discussNoticeFlag) {
		this.discussNoticeFlag = discussNoticeFlag;
	}

	public boolean isNoticedNoticeFlag() {
		return noticedNoticeFlag;
	}

	public void setNoticedNoticeFlag(boolean noticedNoticeFlag) {
		this.noticedNoticeFlag = noticedNoticeFlag;
	}
}
