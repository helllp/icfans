package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.enums.UserTypeEnum;
import com.icfans.paltform.common.vo.BaseVo;

public class No001Output extends BaseVo {
	/**
	 * token作为后续查询接口的键值
	 */
	private String token;

	/**
	 * 用户ID
	 */
	private long userId;

	/**
	 * 用户类型
	 */
	private int userType = UserTypeEnum.普通用户.getValue();

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}
}
