package com.icfans.paltform.logic.resource.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.icfans.paltform.common.RedisDefine;
import com.icfans.paltform.common.enums.MasterOrderTypeEnum;
import com.icfans.paltform.common.enums.MasterSearchTypeEnum;
import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.resource.bean.MasterInfo;
import com.icfans.paltform.logic.resource.mapper.ResourceDaoMapper;
import com.icfans.paltform.logic.resource.service.IMasterService;
import com.icfans.paltform.logic.resource.vo.No030Input;
import com.icfans.paltform.logic.resource.vo.No030Output;
import com.icfans.paltform.logic.resource.vo.No031Input;
import com.icfans.paltform.utils.ConvertUtil;

@Service
public class MasterServiceImpl implements IMasterService {
	/**
	 * 系统通用的服务
	 */
	// 生成返回信息服务
	@Autowired
	private ResultGenerator generator;

	// Redis服务
	@Autowired
	private RedisTemplateService redisService;

	// 数据库ID生成服务
	private IdService idService;

	// 获取配置信息服务
	@Autowired
	private PropertiesConfig config;

	@Autowired
	private ResourceDaoMapper resourceDaoMapper;

	private static final Logger logger = LoggerFactory.getLogger(MasterServiceImpl.class);

	@Override
	@Cacheable(RedisDefine.NO030)
	public Result runNo030Logic(No030Input input) {

		No030Output no030Output = new No030Output();

		PageHelper.startPage(input.getPage(), ConvertUtil.getPageSize(input.getSize()));

		// 0:按领域查询,1:按姓名查询
		if (input.getQueryType() == MasterSearchTypeEnum.领域查询.getValue()) {

			// 0:按关注度排序,1:按文献数排序
			if (input.getOrderType() == MasterOrderTypeEnum.关注度.getValue()) {
				no030Output.setMasterInfoList(resourceDaoMapper.findMasterInfoByFieldZero(input.getQueryKey()));

			} else if (input.getOrderType() == MasterOrderTypeEnum.文献数.getValue()) {
				no030Output.setMasterInfoList(resourceDaoMapper.findMasterInfoByFieldOne(input.getQueryKey()));
			}

		} else if (input.getQueryType() == MasterSearchTypeEnum.姓名查询.getValue()) {

			if (input.getOrderType() == MasterOrderTypeEnum.关注度.getValue()) {
				no030Output.setMasterInfoList(resourceDaoMapper.findMasterInfoByNameZero(input.getQueryKey()));

			} else if (input.getOrderType() == MasterOrderTypeEnum.文献数.getValue()) {
				no030Output.setMasterInfoList(resourceDaoMapper.findMasterInfoByNameOne(input.getQueryKey()));
			}
		}

		return generator.genSuccessResult(no030Output.getMasterInfoList());
	}

	@Override
	@Cacheable(RedisDefine.NO031)
	public Result runNo031Logic(No031Input input) {

		MasterInfo masterInfo = resourceDaoMapper.findMasterInfoByUserId(input.getUserId());
		if (null != masterInfo) {
			return generator.genSuccessResult(masterInfo);
		}
		return generator.genSuccessResult();
	}

}
