package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No008Input extends BaseVo {

	/**
	 * 用户令牌
	 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
