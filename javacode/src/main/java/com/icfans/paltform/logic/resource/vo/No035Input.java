package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No035Input extends BaseVo {
	/**
	 * 问题id
	 */
	private Long problemsId;

	public Long getProblemsId() {
		return problemsId;
	}

	public void setProblemsId(Long problemsId) {
		this.problemsId = problemsId;
	}

}
