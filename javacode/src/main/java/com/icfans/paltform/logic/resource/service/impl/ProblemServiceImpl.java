package com.icfans.paltform.logic.resource.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.resource.service.IProblemService;
import com.icfans.paltform.logic.resource.vo.No034Input;
import com.icfans.paltform.logic.resource.vo.No035Input;
import com.icfans.paltform.logic.resource.vo.No036Input;
import com.icfans.paltform.logic.resource.vo.No037Input;
import com.icfans.paltform.logic.resource.vo.No038Input;

@Service
public class ProblemServiceImpl implements IProblemService{
	/**
	 * 系统通用的服务
	 */
	//	生成返回信息服务
	@Autowired
	private ResultGenerator generator;
	
	//	Redis服务
	@Autowired
	private RedisTemplateService redisService;
	
	//	数据库ID生成服务
	@Autowired
	private IdService idService;
	
	//	获取配置信息服务
	@Autowired
	private PropertiesConfig config;
	
	private static final Logger logger = LoggerFactory.getLogger(ProblemServiceImpl.class);
	
	@Override
	public Result runNo034Logic(No034Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo035Logic(No035Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo036Logic(No036Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo037Logic(No037Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo038Logic(No038Input input) {
		return generator.genSuccessResult();
	}

}
