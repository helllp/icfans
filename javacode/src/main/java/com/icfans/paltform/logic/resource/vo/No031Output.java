package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.MasterInfo;

public class No031Output extends BaseVo {
	/**
	 * 专家基本信息
	 */
	private MasterInfo master;

	public MasterInfo getMaster() {
		return master;
	}

	public void setMaster(MasterInfo master) {
		this.master = master;
	}
}
