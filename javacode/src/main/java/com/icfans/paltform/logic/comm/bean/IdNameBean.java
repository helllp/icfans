package com.icfans.paltform.logic.comm.bean;

/**
 * 公共的ID，Name实体
 * 
 * @author 林晓明
 *
 */
public class IdNameBean {
	/**
	 * 标识符
	 */
	private Long id;
	
	/**
	 * 名称
	 */
	private String name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
