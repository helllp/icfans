package com.icfans.paltform.logic.resource.mapper;

import java.util.List;

import com.icfans.paltform.logic.resource.bean.ArchiveInfo;
import com.icfans.paltform.logic.resource.bean.MasterInfo;

/**
 * 行业专家信息Mapper
 * 
 * @author Administrator
 *
 */
public interface ResourceDaoMapper {

	/**
	 * 根据专家领域查询,按关注度排序
	 * 
	 * @param queryKey
	 * @param page
	 * @param size
	 * @return
	 */
	List<MasterInfo> findMasterInfoByFieldZero(String queryKey);

	/**
	 * 根据专家领域查询,按文献数排序
	 *
	 * @param queryKey
	 * @param page
	 * @param size
	 * @return
	 */
	List<MasterInfo> findMasterInfoByFieldOne(String queryKey);

	/**
	 * 根据专家姓名查询,按关注度排序
	 *
	 * @param queryKey
	 * @param page
	 * @param size
	 * @return
	 */
	List<MasterInfo> findMasterInfoByNameZero(String queryKey);

	/**
	 * 根据专家姓名查询,按文献数排序
	 *
	 * @param queryKey
	 * @param page
	 * @param size
	 * @return
	 */
	List<MasterInfo> findMasterInfoByNameOne(String queryKey);

	/**
	 * 根据专家ID查询专家信息
	 * 
	 * @param userid
	 * @return
	 */
	MasterInfo findMasterInfoByUserId(Long userId);

	/**
	 * 按领域查询文章基本信息，按阅读量排序
	 * 
	 * @param areaId
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByAreaIdZero(String areaId);

	/**
	 * 按领域查询文章基本信息，按发布时间排序
	 * 
	 * @param areaId
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByAreaIdOne(String areaId);

	/**
	 * 按发布者姓名查询文章基本信息，按阅读量排序
	 * 
	 * @param userName
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByUserNameZero(String userName);

	/**
	 * 按发布者姓名查询文章基本信息，按发布时间排序
	 * 
	 * @param userName
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByUserNameOne(String userName);

	/**
	 * 按发布者ID查询文章基本信息，按阅读量排序
	 * 
	 * @param userId
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByUserIdZero(String userId);

	/**
	 * 按发布者ID查询文章基本信息，按发布时间排序
	 * 
	 * @param userId
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByUserIdOne(String userId);

	/**
	 * 按推荐查询文章基本信息，注：暂时没有推荐功能，此处用倒序阅读量方式展示文章
	 * 
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByTestZero();

	/**
	 * 按推荐查询文章基本信息，注：暂时没有推荐功能，此处用倒序时间的范式展示文章
	 * 
	 * @return
	 */
	List<ArchiveInfo> findArchiveInfoByTestOne();
	
	/**
	 * 根据文章ID查询文章详情
	 * @param archiveId
	 * @return
	 */
	ArchiveInfo findArchiveInfoByArchiveId(Long archiveId);

}
