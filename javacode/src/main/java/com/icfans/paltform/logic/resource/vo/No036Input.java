package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No036Input extends BaseVo {
	/**
	 * 解答者的ID
	 */
	private Long userId;

	/**
	 * 问题的标题
	 */
	private String problemsTitle;

	/**
	 * 问题的描述
	 */
	private String problemsDesc;

	/**
	 * 问题的图片URL列表，逗号分隔
	 */
	private String problemsImgUrl;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProblemsTitle() {
		return problemsTitle;
	}

	public void setProblemsTitle(String problemsTitle) {
		this.problemsTitle = problemsTitle;
	}

	public String getProblemsDesc() {
		return problemsDesc;
	}

	public void setProblemsDesc(String problemsDesc) {
		this.problemsDesc = problemsDesc;
	}

	public String getProblemsImgUrl() {
		return problemsImgUrl;
	}

	public void setProblemsImgUrl(String problemsImgUrl) {
		this.problemsImgUrl = problemsImgUrl;
	}
}
