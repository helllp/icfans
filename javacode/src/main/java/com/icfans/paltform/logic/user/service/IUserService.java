package com.icfans.paltform.logic.user.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.user.vo.No001Input;
import com.icfans.paltform.logic.user.vo.No002Input;
import com.icfans.paltform.logic.user.vo.No004Input;
import com.icfans.paltform.logic.user.vo.No005Input;
import com.icfans.paltform.logic.user.vo.No006Input;
import com.icfans.paltform.logic.user.vo.No007Input;
import com.icfans.paltform.logic.user.vo.No008Input;
import com.icfans.paltform.logic.user.vo.No009Input;
import com.icfans.paltform.logic.user.vo.No010Input;
import com.icfans.paltform.logic.user.vo.No011Input;

public interface IUserService {
	/**
	 * NO_001:登录/注册
	 *  
	 * @param input
	 * @return
	 */
	public Result runNo001Logic(No001Input input);
	
	/**
	 * NO_002:发送手机验证码
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo002Logic(No002Input input);
	
	/**
	 * NO_003:获取用户协议
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo003Logic();
	
	/**
	 * NO_004:获取个人信息
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo004Logic(No004Input input);
	
	/**
	 * NO_005:设置头像
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo005Logic(No005Input input);
	
	/**
	 * NO_006:设置用户个人信息
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo006Logic(No006Input input);
	
	/**
	 * NO_007:退出登录
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo007Logic(No007Input input);
	
	/**
	 * NO_008:获取个人系统设置
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo008Logic(No008Input input);
	
	/**
	 * NO_009:更换手机
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo009Logic(No009Input input);
	
	/**
	 * NO_010:写入意见反馈
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo010Logic(No010Input input);
	
	/**
	 * NO_011:个人系统设置
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo011Logic(No011Input input);
	
	/**
	 * NO_012:获取产品信息
	 * 
	 * @param input
	 * @return
	 */
	public Result runNo012Logic();
}
