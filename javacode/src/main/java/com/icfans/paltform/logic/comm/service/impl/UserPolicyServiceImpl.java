package com.icfans.paltform.logic.comm.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.comm.bean.UserInfo;
import com.icfans.paltform.logic.comm.mapper.UserPolicyDaoMapper;
import com.icfans.paltform.logic.comm.service.UserPolicyService;
import com.icfans.paltform.model.SusrRole;

@Service
public class UserPolicyServiceImpl implements UserPolicyService{

	@Autowired
	private RedisTemplateService redisService;
	
	@Autowired
	private UserPolicyDaoMapper policyMapper;
	
	@Override
	public Long checkToken(String token) {
		if(token == null){
			return null;
		}
		
		Object o = redisService.get(token);
		
		if( o == null){
			return null;
		}
		
		UserInfo outPut = (UserInfo)o;
		
		return outPut.getUserId();
	}

	@Override
	public Collection<String> gerUserRole(Long userId) {
		List<SusrRole> list = policyMapper.queryByUserId(userId);
		
		Collection<String> coll = new ArrayList<String>();
		
		for (SusrRole roleBean : list) {
			coll.add(roleBean.getRoleName());
		}
		
		return coll;
	}

}
