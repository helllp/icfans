package com.icfans.paltform.logic.resource.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.icfans.paltform.common.RedisDefine;
import com.icfans.paltform.common.enums.ArchiveOrderTypeEnum;
import com.icfans.paltform.common.enums.ArchiveSearchTypeEnum;
import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.resource.bean.ArchiveInfo;
import com.icfans.paltform.logic.resource.mapper.ResourceDaoMapper;
import com.icfans.paltform.logic.resource.service.IArchivesService;
import com.icfans.paltform.logic.resource.vo.No032Input;
import com.icfans.paltform.logic.resource.vo.No032Output;
import com.icfans.paltform.logic.resource.vo.No033Input;
import com.icfans.paltform.utils.ConvertUtil;

@Service
public class ArchivesServiceImpl implements IArchivesService {
	/**
	 * 系统通用的服务
	 */
	// 生成返回信息服务
	@Autowired
	private ResultGenerator generator;

	// Redis服务
	@Autowired
	private RedisTemplateService redisService;

	// 数据库ID生成服务
	@Autowired
	private IdService idService;

	// 获取配置信息服务
	@Autowired
	private PropertiesConfig config;

	@Autowired
	private ResourceDaoMapper resourceDaoMapper;

	private static final Logger logger = LoggerFactory.getLogger(ArchivesServiceImpl.class);

	@Override
	@Cacheable(RedisDefine.NO032)
	public Result runNo032Logic(No032Input input) {

		PageHelper.startPage(input.getPage(), ConvertUtil.getPageSize(input.getSize()));

		No032Output no032Output = new No032Output();

		// 0:按领域查询,1按姓名,2:按发布者ID,3:推荐文献(暂无)
		if (input.getQueryType() == ArchiveSearchTypeEnum.领域.getValue()) {

			// 0:按收藏量,1:按发布时间,2:按评论数量
			if (input.getOrderType() == ArchiveOrderTypeEnum.按阅读量.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByAreaIdZero(input.getQueryKey()));

			} else if (input.getOrderType() == ArchiveOrderTypeEnum.按发布时间.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByAreaIdOne(input.getQueryKey()));

			}

		} else if (input.getQueryType() == ArchiveSearchTypeEnum.姓名.getValue()) {

			if (input.getOrderType() == ArchiveOrderTypeEnum.按阅读量.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByUserNameZero(input.getQueryKey()));

			} else if (input.getOrderType() == ArchiveOrderTypeEnum.按发布时间.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByUserNameOne(input.getQueryKey()));

			}

		} else if (input.getQueryType() == ArchiveSearchTypeEnum.发布者ID.getValue()) {

			if (input.getOrderType() == ArchiveOrderTypeEnum.按阅读量.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByUserIdZero(input.getQueryKey()));

			} else if (input.getOrderType() == ArchiveOrderTypeEnum.按发布时间.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByUserIdOne(input.getQueryKey()));

			}

		} else if (input.getQueryType() == ArchiveSearchTypeEnum.推荐文献.getValue()) {

			if (input.getOrderType() == ArchiveOrderTypeEnum.按阅读量.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByTestZero());

			} else if (input.getOrderType() == ArchiveOrderTypeEnum.按发布时间.getValue()) {

				no032Output.setArchiveInfoList(resourceDaoMapper.findArchiveInfoByTestOne());

			}

		}

		// TODO 待写入缓存

		return generator.genSuccessResult(no032Output.getArchiveInfoList());
	}

	@Override
	@Cacheable(RedisDefine.NO033)
	public Result runNo033Logic(No033Input input) {

		ArchiveInfo archiveInfo = resourceDaoMapper.findArchiveInfoByArchiveId(input.getArchivesId());
		if (null != archiveInfo) {
			return generator.genSuccessResult(archiveInfo);
		}
		return generator.genSuccessResult();
	}
}
