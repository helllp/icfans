package com.icfans.paltform.logic.system.bean;

import java.util.List;

public class AreaInfo {
	/**
	 * 标识符
	 */
	private Long id;
	
	/**
	 * 名称
	 */
	private String name;
	
	/**
	 * 子领域列表
	 */
	private List<AreaInfo> chileList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<AreaInfo> getChileList() {
		return chileList;
	}

	public void setChileList(List<AreaInfo> chileList) {
		this.chileList = chileList;
	}
}
