package com.icfans.paltform.logic.resource.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.ArchiveInfo;

public class No032Output extends BaseVo {
	/**
	 * 文章的信息列表
	 */
	private List<ArchiveInfo> archiveInfoList;

	public List<ArchiveInfo> getArchiveInfoList() {
		return archiveInfoList;
	}

	public void setArchiveInfoList(List<ArchiveInfo> archiveInfoList) {
		this.archiveInfoList = archiveInfoList;
	}

}
