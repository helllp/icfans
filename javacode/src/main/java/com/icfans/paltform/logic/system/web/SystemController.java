package com.icfans.paltform.logic.system.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.system.service.ISystemService;
import com.icfans.paltform.logic.system.vo.No070Input;
import com.icfans.paltform.logic.system.vo.No071Input;
import com.icfans.paltform.logic.system.vo.No072Input;

@RestController
@RequestMapping("/system")
public class SystemController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(SystemController.class);
	
	@Autowired
	private ISystemService sysService;
	
	/**
	 * NO_070:查询热门关键字
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no070")
	public Result postNo070(@Valid @RequestBody No070Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No070：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return sysService.runNo070Logic(input);
	}
	
	/**
	 * NO_071:查询分类标准
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no071")
	public Result postNo071(@Valid @RequestBody No071Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No071：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return sysService.runNo071Logic(input);
	}
	
	/**
	 * NO_072:上传图片接口
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no072")
	public Result postNo072(@Valid @RequestBody No072Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No072：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return sysService.runNo072Logic(input);
	}
}
