package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No033Input extends BaseVo {
	/**
	 * 文章ID
	 */
	private Long archivesId;

	public Long getArchivesId() {
		return archivesId;
	}

	public void setArchivesId(Long archivesId) {
		this.archivesId = archivesId;
	}
}
