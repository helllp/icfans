package com.icfans.paltform.logic.person.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.person.service.IPersonService;
import com.icfans.paltform.logic.person.vo.No050Input;
import com.icfans.paltform.logic.person.vo.No051Input;
import com.icfans.paltform.logic.person.vo.No052Input;
import com.icfans.paltform.logic.person.vo.No053Input;
import com.icfans.paltform.logic.person.vo.No054Input;
import com.icfans.paltform.logic.person.vo.No055Input;

@RestController
@RequestMapping("/person")
public class PersonController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(PersonController.class);
	
	@Autowired
	private IPersonService personService;
	
	/**
	 * NO_050:个人收藏文章操作
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no050")
	public Result postNo050(@Valid @RequestBody No050Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No050：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo050Logic(input);
	}
	
	/**
	 * NO_051:个人收藏文章的查询
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no051")
	public Result postNo051(@Valid @RequestBody No051Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No051：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo051Logic(input);
	}
	
	/**
	 * NO_052:个人关注他人的操作
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no052")
	public Result postNo052(@Valid @RequestBody No052Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No052：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo052Logic(input);
	}
	
	/**
	 * NO_053:个人关注他人的查询
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no053")
	public Result postNo053(@Valid @RequestBody No053Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No053：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo053Logic(input);
	}
	
	/**
	 * NO_054:个人收藏问题的操作
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no054")
	public Result postNo054(@Valid @RequestBody No054Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No054：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo054Logic(input);
	}
	
	/**
	 * NO_055:个人收藏问题的查询
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no055")
	public Result postNo055(@Valid @RequestBody No055Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No055：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return personService.runNo055Logic(input);
	}
}
