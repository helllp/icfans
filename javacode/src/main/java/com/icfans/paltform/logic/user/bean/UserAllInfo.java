package com.icfans.paltform.logic.user.bean;

import com.icfans.paltform.logic.comm.bean.UserInfo;

/**
 * 用户所有信息
 * 
 * @author Administrator
 *
 */
public class UserAllInfo {
	
	/**
	 * 我的关注
	 */
	private int attentionCount;
	
	/**
	 * 我的收藏
	 */
	private int enshrineCount;
	
	/**
	 * 该用户所提问的总数
	 */
	private int issueCount;
	
	/**
	 * 用户的基本信息
	 */
	private UserInfo userInfo;

	public int getAttentionCount() {
		return attentionCount;
	}

	public void setAttentionCount(int attentionCount) {
		this.attentionCount = attentionCount;
	}

	public int getEnshrineCount() {
		return enshrineCount;
	}

	public void setEnshrineCount(int enshrineCount) {
		this.enshrineCount = enshrineCount;
	}

	public int getIssueCount() {
		return issueCount;
	}

	public void setIssueCount(int issueCount) {
		this.issueCount = issueCount;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}