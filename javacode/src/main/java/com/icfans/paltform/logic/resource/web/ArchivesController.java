package com.icfans.paltform.logic.resource.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.resource.service.IArchivesService;
import com.icfans.paltform.logic.resource.vo.No032Input;
import com.icfans.paltform.logic.resource.vo.No033Input;

/**
 * 文章相关的API
 * 
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/res")
public class ArchivesController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(ArchivesController.class);
	
	@Autowired
	private IArchivesService archivesService;
	
	/**
	 * NO_032:查询文章
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no032")
	public Result postNo032(@Valid @RequestBody No032Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No032：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return archivesService.runNo032Logic(input);
	}
	
	/**
	 * NO_033:查看文章详情
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no033")
	public Result postNo033(@Valid @RequestBody No033Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No033：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return archivesService.runNo033Logic(input);
	}
}
