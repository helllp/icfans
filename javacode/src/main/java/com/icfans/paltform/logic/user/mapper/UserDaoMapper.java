package com.icfans.paltform.logic.user.mapper;

import com.icfans.paltform.logic.user.bean.UserAllInfo;

/**
 * 用户所有信息mapper接口
 * @author Administrator
 *
 */
public interface UserDaoMapper {
	/**
	 * 查询用户所有信息
	 * 
	 * @param userId
	 * @return
	 */
	UserAllInfo findUserAllInfo(long userId);
}
