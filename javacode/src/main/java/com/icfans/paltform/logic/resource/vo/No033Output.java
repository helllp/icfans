package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.ArchiveInfo;

public class No033Output extends BaseVo {
	/**
	 * 文章的基本信息
	 */
	private ArchiveInfo archive;

	public ArchiveInfo getArchive() {
		return archive;
	}

	public void setArchive(ArchiveInfo archive) {
		this.archive = archive;
	}
}
