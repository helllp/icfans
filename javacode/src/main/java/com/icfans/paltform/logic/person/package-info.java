/**
 * 个人相关的服务
 * 
 * @author 林晓明
 *
 */
package com.icfans.paltform.logic.person;

/**
 * NO_050:个人收藏文章操作
 * NO_051:个人收藏文章的查询
 * NO_052:个人关注他人的操作
 * NO_053:个人关注他人的查询
 * NO_054:个人收藏问题的操作
 * NO_055:个人收藏问题的查询
 */
