package com.icfans.paltform.logic.comm.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultEnum;
import com.icfans.paltform.core.BaseController;

/**
 * 全局公用的控制器
 * 
 * @author 林晓明
 *
 */
@RestController
@RequestMapping("/comm")
public class CommonController extends BaseController{
	
	/**
	 * 校验Token失败
	 * 
	 * @return
	 */
	@GetMapping(value = "/token")
	public Result checkTokenError(){
		return this.genErrorResult(ResultEnum.RETURN_VARIFY_TOKEN_ERROR);
	}

	@GetMapping(value = "/404")
	public Result html404(){
		return this.genErrorResult(ResultEnum.RETURN_PAGE_404_ERROR);
	}
	
	@GetMapping(value = "/500")
	public Result html500(){
		return this.genErrorResult(ResultEnum.RETURN_PAGE_500_ERROR);
	}

//	@Cacheable(value="aa")
//	@CachePut
//	@CacheEvict
}
