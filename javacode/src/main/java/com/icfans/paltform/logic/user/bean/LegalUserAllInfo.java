package com.icfans.paltform.logic.user.bean;

/**
 * 法人用户的所有信息
 * @author Administrator
 *
 */
public class LegalUserAllInfo {
    private Long id;

    private String telephone;

    private String email;

    private Integer userType;

    private String companyName;

    private String companyHomepage;

    private String logo;

    private String slogan;

    private String empiricalRange;

    private String companyIntroduce;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyHomepage() {
		return companyHomepage;
	}

	public void setCompanyHomepage(String companyHomepage) {
		this.companyHomepage = companyHomepage;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getSlogan() {
		return slogan;
	}

	public void setSlogan(String slogan) {
		this.slogan = slogan;
	}

	public String getEmpiricalRange() {
		return empiricalRange;
	}

	public void setEmpiricalRange(String empiricalRange) {
		this.empiricalRange = empiricalRange;
	}

	public String getCompanyIntroduce() {
		return companyIntroduce;
	}

	public void setCompanyIntroduce(String companyIntroduce) {
		this.companyIntroduce = companyIntroduce;
	}

//    private Long createUserId;
//
//    private Long createTime;
//
//    private Long lastUpdatePerson;
//
//    private Long lastUpdateTime;
    
}