package com.icfans.paltform.logic.comm.bean;

/**
 * 获取用户信息的缓存对象
 * 
 * @author 林晓明
 *
 */
public class UserInfo {
	/**
	 * 用户ID
	 */
	private Long userId;
	
	/**
	 * 手机号
	 */
	private String telephone;

	/**
	 * 用户类型
	 */
	private Integer userType;

	/**
	 * 用户姓名
	 */
	private String username;

	/**
	 * 头像
	 */
	private String headPhoto;
	
	/**
	 * 签名
	 */
	private String sign;
	
	/**
	 * 性别
	 */
	private Integer sex;

	/**
	 * 毕业学校
	 */
	private String graduateSchool;
	
	/**
	 * 工作
	 */
	private String job;

	/**
	 * 职位
	 */
	private String position;

	/**
	 * 职务
	 */
	private String business;

	/**
	 * 个人主页
	 */
	private String personalHomepage;
	
	private String token;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHeadPhoto() {
		return headPhoto;
	}

	public void setHeadPhoto(String headPhoto) {
		this.headPhoto = headPhoto;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getGraduateSchool() {
		return graduateSchool;
	}

	public void setGraduateSchool(String graduateSchool) {
		this.graduateSchool = graduateSchool;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getPersonalHomepage() {
		return personalHomepage;
	}

	public void setPersonalHomepage(String personalHomepage) {
		this.personalHomepage = personalHomepage;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
