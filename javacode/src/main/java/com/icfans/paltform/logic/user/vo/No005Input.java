package com.icfans.paltform.logic.user.vo;

import org.springframework.web.multipart.MultipartFile;

import com.icfans.paltform.common.vo.BaseVo;

public class No005Input extends BaseVo {
	/**
	 * 上传的图片文件
	 */
	private MultipartFile headPhoto;

	public MultipartFile getHeadPhoto() {
		return headPhoto;
	}

	public void setHeadPhoto(MultipartFile headPhoto) {
		this.headPhoto = headPhoto;
	}
}
