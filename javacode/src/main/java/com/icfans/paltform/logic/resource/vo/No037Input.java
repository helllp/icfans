package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No037Input extends BaseVo {
	/**
	 * 问题ID
	 */
	private Long problemsId;

	/**
	 * 被回复的用户ID
	 */
	private Long repiedUserId;

	/**
	 * 文本内容
	 */
	private String replyText;

	public Long getProblemsId() {
		return problemsId;
	}

	public void setProblemsId(Long problemsId) {
		this.problemsId = problemsId;
	}

	public Long getRepiedUserId() {
		return repiedUserId;
	}

	public void setRepiedUserId(Long repiedUserId) {
		this.repiedUserId = repiedUserId;
	}

	public String getReplyText() {
		return replyText;
	}

	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}
}
