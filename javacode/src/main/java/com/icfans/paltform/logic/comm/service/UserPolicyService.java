package com.icfans.paltform.logic.comm.service;

import java.util.Collection;

public interface UserPolicyService {
	/**
	 * 校验Token的合法性
	 * 
	 * @param token
	 * @return
	 */
	public Long checkToken(String token);
	
	/**
	 * 根据登录用户获取用户的角色
	 * 
	 * @param userId
	 * @return
	 */
	public Collection<String> gerUserRole(Long userId);
	
	
}
