package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.enums.SexTypeEnum;
import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.core.check.CheckEnum;

public class No006Input extends BaseVo {
	/**
	 * 所在单位
	 */
	private String job;
	
	/**
	 * 职位
	 */
	private String position;
	
	/**
	 * 个人简介
	 */
	private String personalIntro;
	
	/**
	 * 姓名
	 */
	private String userName;
	
	/**
	 * 昵称
	 */
	private String nickName;
	
	/**
	 * 性别
	 */
	@CheckEnum(SexTypeEnum.class)
	private int sex;
	
	/**
	 * 年龄
	 */
	private String age;
	
	/**
	 * 用户令牌
	 */
	private String token;

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPersonalIntro() {
		return personalIntro;
	}

	public void setPersonalIntro(String personalIntro) {
		this.personalIntro = personalIntro;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
}
