package com.icfans.paltform.logic.demo.web;

import java.io.Serializable;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.demo.vo.UserValidatorModel;
import com.icfans.paltform.model.SusrUser;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


/* @Api用在类上，说明该类的作用。可以标记一个Controller类做为swagger文档资源,与Controller注解并列使用。 属性配置：
 *  value--url的路径值
 *   tags--如果设置这个值 value的值会被覆盖
 *   produces--For example, "application/json, application/xml"
 *   consumes--Forexample,"application/json, application/xml"
 *   protocols--设置特定协议，如:http,https, ws,wss.
 *   authorizations--高级特性认证时配置
 *   hidden-- 配置为true将隐藏此资源下的操作
 */
@Api(value = "DemoController", tags = "用户相关api")
@RestController
public class Swagger2Annotation extends BaseController {
//	@Autowired
//	private UserMapper usrMapper;

	
	 /* @ApiOperation：用在方法上，说明方法的作用，每一个url资源的定义,属性配置：
	  *  value--url的路径值 
	  *  notes--操作的详细描述
	  *  tags--如果设置这个值value的值会被覆盖
	  *  response--返回的对象
	  *  responseContainer--响应容器，这些对象是有效的：List,set,map;其他值将被忽略
	  *  responseReference--指定对响应类型的引用。 指定的引用可以是本地的和远程的，将被原样使用，并且将覆盖任何指定的response（）类。
	  *  httpMethod--http方法："GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS" and "PATCH"
	  *  nickname--第三方工具使用的operationId来唯一标识此操作。 在Swagger 2.0中，这不再是强制性的，如果没有提供将保持为空。
	  *	 produces--For example,"application/json, application/xml"
	  *  consumes--For example,"application/json, application/xml"
	  *  protocols--Possible values:http, https, ws, wss.
	  *  authorizations--高级特性认证时配置
	  *  hidden--配置为true将隐藏此资源下的操作
	  *  responseHeaders--返回响应标题
	  *  code--http的状态码默认 200
	  */
	@ApiOperation(value = "获取用户信息", tags = { "获取用户信息copy" }, notes = "注意问题点", produces = "application/json")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "uname", value = "用户名", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/getU", method = RequestMethod.POST)
	public Result queryUserByName(@RequestBody String uname) {
//		User user = MyLogicMapper.findUserByName(uname);
		return this.genSuccessResult("");
	}

	/*
	 * @ApiParam() 用于方法 name–参数名 value–参数说明 required–是否必填
	 */
	@ApiOperation(value = "获取用户信息", tags = { "获取用户信息copy" }, notes = "注意问题点")
	@GetMapping("/demo/getUserInfo")
	public Result getUserInfo(@ApiParam(name = "id", value = "用户id", required = true) Long id,
			@ApiParam(name = "username", value = "用户名") String username) {
		// userService可忽略，是业务逻辑
//		User user = usrMapper.selectByPrimaryKey(id);
		return this.genSuccessResult("");
	}

	@ApiOperation(value = "更改用户信息")
	@PostMapping("/demo/updateUserInfo")
	public Result getUserInfo(@RequestBody @ApiParam(name = "user", value = "传入json格式", required = true) SusrUser user) {
		// userService可忽略，是业务逻辑
//		int num = usrMapper.updateByPrimaryKey(user);
		return this.genSuccessResult(1);
	}

	/*
	 * @ApiResponses响应头设置 name-响应头名称, description-头描述,response-默认响应类 Void
	 * responseContainer-参考ApiOperation中配置
	 */
	@RequestMapping(value = "/addUser", method = RequestMethod.POST)
	@ApiOperation(value = "添加一个用户", response = SusrUser.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "输入无效", response = SusrUser.class) })
	// @ApiResponse(code = 400, message = "用户提供的无效")
	public ResponseEntity<String> placeOrder(@ApiParam(value = "保存用户", required = true) SusrUser user) {
//		usrMapper.insert(user);
		return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/*
	 * @ApiImplicitParam 属性配置： 
	 * name--参数名称
	 * value--参数的说明
	 * defaultValue：描述参数的默认值
	 * allowableValues--设置此参数可接受的值
	 * required--参数是否必须传 
	 * dataType--参数类型
	 * paramType--参数类型有： body、path、query、header、form
	 */
	@ApiImplicitParams({
			@ApiImplicitParam(name = "valModel", value = "valModel", required = true, paramType = "body", dataType = "UserValidatorModel")})
	@RequestMapping(value = "/demo/demoAdd", method = RequestMethod.POST)
	public Result demoAdd(@RequestBody @Valid UserValidatorModel valModel, BindingResult result) {
		if (result.hasErrors()) {
			List<ObjectError> list = result.getAllErrors();
			for (ObjectError error : list) {
				System.out.println(error.getCode() + "---" + error.getArguments() + "---" + error.getDefaultMessage());
			}
			this.genSuccessResult(result);
		}
		return this.genSuccessResult(result);
	}
}

// @ApiModel()用于类 ；表示对类进行说明，用于参数用实体类接收。value–表示对象名;description–描述
@ApiModel(value = "UserTest", description = "用户对象user")
class UserTest implements Serializable {
	private static final long serialVersionUID = 1L;
	/*
	 * @ApiModelProperty()用于方法，字段； 表示对model属性的说明或者数据操作更改: value–字段说明 name–重写属性名字
	 * dataType–重写属性类型 required–是否必填 example–举例说明 hidden–隐藏
	 */
	@ApiModelProperty(value = "用户名", name = "username", example = "zhangsan")
	private String username;
	@ApiModelProperty(value = "状态", name = "state", required = true)
	private Integer state;
	private String password;
	private String nickName;
	private Integer isDeleted;

	@ApiModelProperty(value = "id数组", hidden = true)
	private String[] ids;
	private List<String> idList;
	// 省略get/set
}
