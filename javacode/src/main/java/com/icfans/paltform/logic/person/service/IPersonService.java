package com.icfans.paltform.logic.person.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.person.vo.No050Input;
import com.icfans.paltform.logic.person.vo.No051Input;
import com.icfans.paltform.logic.person.vo.No052Input;
import com.icfans.paltform.logic.person.vo.No053Input;
import com.icfans.paltform.logic.person.vo.No054Input;
import com.icfans.paltform.logic.person.vo.No055Input;

public interface IPersonService {

	public Result runNo050Logic(No050Input input);

	public Result runNo051Logic(No051Input input);

	public Result runNo052Logic(No052Input input);

	public Result runNo053Logic(No053Input input);

	public Result runNo054Logic(No054Input input);

	public Result runNo055Logic(No055Input input);

}
