package com.icfans.paltform.logic.resource.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No031Input extends BaseVo {
	/**
	 * 专家用户ID
	 */
	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
