package com.icfans.paltform.logic.resource.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.resource.service.IMasterService;
import com.icfans.paltform.logic.resource.vo.No030Input;
import com.icfans.paltform.logic.resource.vo.No031Input;

/**
 * 专家相关API
 * 
 * @author 林晓明
 *
 */
@RestController
@RequestMapping("/res")
public class MasterController extends BaseController {

	private static final Logger logger = LoggerFactory.getLogger(MasterController.class);

	@Autowired
	private IMasterService masterService;

	/**
	 * NO_030:查询行业专家
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no030")
	public Result postNo030(@Valid @RequestBody No030Input input, BindingResult result) {
		if (result.hasErrors()) {
			logger.info("No030：输入参数有误！");
			return this.genCheckResult(result);
		}

		return masterService.runNo030Logic(input);
	}

	/**
	 * NO_031:获取专家基本信息
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no031")
	public Result postNo031(@Valid @RequestBody No031Input input, BindingResult result) {
		if (result.hasErrors()) {
			logger.info("No031：输入参数有误！");
			return this.genCheckResult(result);
		}

		return masterService.runNo031Logic(input);
	}
}
