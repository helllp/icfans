package com.icfans.paltform.logic.system.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.system.vo.No070Input;
import com.icfans.paltform.logic.system.vo.No071Input;
import com.icfans.paltform.logic.system.vo.No072Input;

public interface ISystemService {

	Result runNo070Logic(No070Input input);

	Result runNo071Logic(No071Input input);

	Result runNo072Logic(No072Input input);

}
