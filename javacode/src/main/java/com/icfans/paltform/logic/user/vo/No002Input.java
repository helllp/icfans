package com.icfans.paltform.logic.user.vo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.VerifyUseTypeEnum;
import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.core.check.CheckEnum;

public class No002Input extends BaseVo {
	/**
	 * 验证码电话
	 */
	@NotEmpty(message = "手机号不能为空")
	@Pattern(regexp="((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0-9]))\\d{8}", message="手机号不正确")
	private String phone;
	
	/**
	 * 验证码用途
	 */
	@NotNull(message = "用途不能为空")
	@CheckEnum(VerifyUseTypeEnum.class)
	private int useType;

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getUseType() {
		return useType;
	}

	public void setUseType(int useType) {
		this.useType = useType;
	}
}
