/**
 * 文章，问题相关的服务
 * 
 * @author 林晓明
 *
 */
package com.icfans.paltform.logic.resource;

/**
 * NO_030:查询行业专家
 * NO_031:获取专家基本信息
 * NO_032:查询文章
 * NO_033:查看文章详情
 * NO_034:查询问题
 * NO_035:获取问题详情
 * NO_036:发布问题
 * NO_037:发表问题评论
 * NO_038:获取问题的评论信息
 */
