package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No003Output extends BaseVo {
	/**
	 * 用户协议文本
	 */
	private String protocol;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	
}
