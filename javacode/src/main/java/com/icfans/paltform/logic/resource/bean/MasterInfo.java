package com.icfans.paltform.logic.resource.bean;

import java.util.List;

import com.icfans.paltform.logic.comm.bean.IdNameBean;

/**
 * 专家的个人信息
 * 
 * @author 林晓明
 *
 */
public class MasterInfo {
	/**
	 * 编号
	 */
	private Long id;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 姓名
	 */
	private String name;

	/**
	 * 性别
	 */
	private Integer sex;

	/**
	 * 头像地址
	 */
	private String photoUrl;

	/**
	 * 个人签名
	 */
	private String personSign;

	/**
	 * 工作
	 */
	private String job;

	/**
	 * 职位
	 */
	private String position;

	/**
	 * 文章数量
	 */
	private String archivesNum;

	/**
	 * 被关注次数
	 */
	private String followNum;

	/**
	 * 个人的领域列表
	 */
	private List<IdNameBean> areaList;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public List<IdNameBean> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<IdNameBean> areaList) {
		this.areaList = areaList;
	}

	public String getArchivesNum() {
		return archivesNum;
	}

	public void setArchivesNum(String archivesNum) {
		this.archivesNum = archivesNum;
	}

	public String getFollowNum() {
		return followNum;
	}

	public void setFollowNum(String followNum) {
		this.followNum = followNum;
	}
}
