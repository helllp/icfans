package com.icfans.paltform.logic.user.service.impl;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.icfans.paltform.common.Define;
import com.icfans.paltform.common.inters.ISmsService;
import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultEnum;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.LocalSessionFactory;
import com.icfans.paltform.core.service.QiniuService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.comm.bean.UserInfo;
import com.icfans.paltform.logic.user.bean.UserAllInfo;
import com.icfans.paltform.logic.user.mapper.UserDaoMapper;
import com.icfans.paltform.logic.user.service.IUserService;
import com.icfans.paltform.logic.user.vo.No001Input;
import com.icfans.paltform.logic.user.vo.No002Input;
import com.icfans.paltform.logic.user.vo.No004Input;
import com.icfans.paltform.logic.user.vo.No005Input;
import com.icfans.paltform.logic.user.vo.No006Input;
import com.icfans.paltform.logic.user.vo.No007Input;
import com.icfans.paltform.logic.user.vo.No008Input;
import com.icfans.paltform.logic.user.vo.No008Output;
import com.icfans.paltform.logic.user.vo.No009Input;
import com.icfans.paltform.logic.user.vo.No010Input;
import com.icfans.paltform.logic.user.vo.No011Input;
import com.icfans.paltform.mapper.SbasFeedbackMapper;
import com.icfans.paltform.mapper.SbasSettingMapper;
import com.icfans.paltform.mapper.SbasUserSettingMapper;
import com.icfans.paltform.mapper.SusrUserMapper;
import com.icfans.paltform.mapper.SusrUserinfoMapper;
import com.icfans.paltform.model.SbasFeedback;
import com.icfans.paltform.model.SbasSetting;
import com.icfans.paltform.model.SbasUserSetting;
import com.icfans.paltform.model.SusrUser;
import com.icfans.paltform.model.SusrUserinfo;
import com.icfans.paltform.utils.ConvertUtil;
import com.icfans.paltform.utils.DbTypeUtils;
import com.qiniu.common.QiniuException;
import com.qiniu.storage.model.DefaultPutRet;

@Service
public class UserService implements IUserService{
	/**
	 * 系统通用的服务
	 */
	//	生成返回信息服务
	@Autowired
	private ResultGenerator generator;
	
	//	Redis服务
	@Autowired
	private RedisTemplateService redisService;
	
	//	数据库ID生成服务
	@Autowired
	private IdService idService;
	
	//	获取配置信息服务
	@Autowired
	private PropertiesConfig config;
	
//	获取短信发送服务
	@Autowired
	private ISmsService iSmsService;
	
	//上传图片到七牛
	@Autowired
	private QiniuService service;
	
	/**
	 * 单表的操作
	 */
	@Autowired
	private SusrUserMapper userMapper;

	@Autowired
	private SusrUserinfoMapper userInfoMapper;
	
	@Autowired
	private SbasUserSettingMapper userSettingMapper;
	
	@Autowired
	private SbasFeedbackMapper sbasFeedbackMapper;
	
	@Autowired
	private SbasSettingMapper sbasSettingMapper;
	/**
	 * 多表联合
	 */
	@Autowired
	private UserDaoMapper userDaoMapper;
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	/**
	 * 登录注册相关逻辑
	 */
	@Override
	@Transactional
	public Result runNo001Logic(No001Input input) {
		Result ret = null;
		String phone = input.getPhone();
		// 验证码是否正确
		if(input.getVerifyCode().equals(redisService.get(phone))){
			redisService.remove(phone);
			
			String token = ConvertUtil.createToken();
			UserInfo userInfoBean = new UserInfo();
			//查看数据库是否存在该用户，不存在即注册
			SusrUser userModel = userMapper.findUserByPhone(phone);
			if(userModel != null){
				SusrUserinfo userInfo = userInfoMapper.findUserInfoByUserId(userModel.getId());
				
				if(userInfo != null){
					BeanUtils.copyProperties(userInfo, userInfoBean);	
				}else{
					logger.error("读取登录用户的详细信息发生错误：用户ID=" + userModel.getId());
				}
				userInfoBean.setUserType(userModel.getUserType());
				userInfoBean.setToken(token);
				//将用户令牌和个人信息放到缓存中
				redisService.set(token, userInfoBean, Define.REDIS_TOKEN_EXPIRETIME);
				
				return generator.genSuccessResult(userInfoBean);
			}else{
				//注册用户
				long userId = addUser(phone, input.getUserType());
				if(userId != 0){
					SusrUserinfo regUserInfo = userInfoMapper.findUserInfoByUserId(userId);
					if(regUserInfo != null){
						BeanUtils.copyProperties(regUserInfo, userInfoBean);	
					}else{
						logger.error("读取注册用户的详细信息发生错误：用户ID=" + userId);
					}
					userInfoBean.setUserType(input.getUserType());
					userInfoBean.setToken(token);
					redisService.set(token, userInfoBean, Define.REDIS_TOKEN_EXPIRETIME);
					
					return generator.genSuccessResult(userInfoBean);
				}
			}
			ret = generator.genErrorResult(ResultEnum.RETURN_INPUT_ERROR.getCode());
		}else{
			ret = generator.genErrorResult(ResultEnum.RETURN_VARIFY_CODE_ERROR.getCode());	
		}
		return ret == null ? generator.genSuccessResult() : ret;
	}

	/**
	 * 添加普通用户相关信息
	 * 
	 * @param phone
	 * @param userType
	 * @return
	 */
	private long addUser(String phone,int userType){
		//添加用户到用户登录表
		long userId = insertUser(phone,userType);
		
		//添加用户基本信息
		SusrUserinfo userInfo = new SusrUserinfo();
		userInfo.setId(idService.nextId());
		userInfo.setUserId(userId);
		userInfo.setUserName(phone);	//	默认用户名为手机号
		userInfo.setHeadPhoto(config.getHeadPhotoUri());
		
		DbTypeUtils.setCommonProp(userInfo);
		
		userInfoMapper.insert(userInfo);
		
		return userId;
	}
	
	/**
	 * 添加登录用户
	 * @param userModel
	 * @return
	 */
	private long insertUser(String phone,int userType){
		long time = System.currentTimeMillis();
		long id = idService.nextId();
		
		SusrUser userModel = new SusrUser();
		
		userModel.setId(id);
		userModel.setTelephone(phone);
		userModel.setUserType(userType);
		userModel.setLastLoginTime(time);
		
		//	进行基本的设置工作
		DbTypeUtils.setCommonProp(userModel);

		userMapper.insert(userModel);
		return id;
	}
	
	/**
	 * 手机发送验证码与频率验证
	 */
	@Override
	public Result runNo002Logic(No002Input input) {
		if(redisService.exists(input.getPhone())){
			return generator.genErrorResult(ResultEnum.RETURN_USER_SMS_ERROR.getCode());
		}else{
			String code = createRandomVcode();
			// TODO 手机号发送短信 
			iSmsService.sendSms(input.getPhone(), code);
			redisService.set(input.getPhone(), code, Define.REDIS_PHONECODE);
			return generator.genSuccessResult();
		}
	}

	/**
	 * 随机生成6位随机验证码
	 * 
	 * @return
	 */
	private String createRandomVcode() {
		if(config.isDebug()){
			return "111111";
		}
		// 验证码
		String vcode = "";
		for (int i = 0; i < 6; i++) {
			vcode = vcode + (int) (Math.random() * 9);
		}
		return vcode;
	}
	
	/**
	 * 获取用户协议
	 */
	@Override
	public Result runNo003Logic() {
		String url = config.getUserContract();
		return generator.genSuccessResult(url);
	}

	/**
	 * 获取用户个人信息
	 */
	@Override
	public Result runNo004Logic(No004Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		//查询用户的个人信息及关注、收藏、提问题的数量
		UserAllInfo userAll = userDaoMapper.findUserAllInfo(userId);
		if(userAll != null){
			return generator.genSuccessResult(userAll);
		}
		return generator.genErrorResult(ResultEnum.RETURN_USER_FINDINFO_ERROR.getCode());
	}

	/**
	 * 设置用户头像
	 */
	@Override
	public Result runNo005Logic(No005Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		String uuidImg = ConvertUtil.createUUID();
		String fileName = input.getHeadPhoto().getOriginalFilename();
		int index = fileName.lastIndexOf(".");
		String suffix = "";
		if (index != -1) {
			suffix = fileName.substring(index);// 文件后缀
		}
		//图片名称
		String imgname = uuidImg+suffix;
		//	上传图片
		String imgKey = uploadImage(input.getHeadPhoto(),imgname);
		if(imgKey.equals(imgname)){
			//查询用户之前的头像
			SusrUserinfo userhead = userInfoMapper.findUserInfoByUserId(userId);
			try {
				//删除之前的旧的头像
				service.deleteFile(config.getBucketName(), userhead.getHeadPhoto());
			} catch (QiniuException e) {
				logger.error("删除图片失败：{}",e);
			}
			//更新用户头像
			SusrUserinfo userInfoInput = new SusrUserinfo();
			userInfoInput.setHeadPhoto(imgname);
			userInfoInput.setUserId(userId);
			int num = modifyUser(userInfoInput);
			if(1 == num){
//				SusrUserinfo userInfo = userInfoMapper.findUserInfoByUserId(userId);
				return generator.genSuccessResult();
			}
		}
		return generator.genErrorResult(ResultEnum.RETURN_UPDATE_USER_ERROR.getCode());
	}

	/**
	 * 上传图片
	 * 
	 * @param uploadFile
	 * @param key 文件名
	 */
	private String uploadImage(MultipartFile uploadFile,String key){
		DefaultPutRet putRet = null;
		try {
			String response = service.uploadFile(uploadFile.getInputStream(), config.getBucketName(), key);
			//解析上传成功的结果
		    putRet = new Gson().fromJson(response, DefaultPutRet.class);
		} catch (IOException e) {
			logger.error("上传图片失败：{}",e);
		}
		return putRet.key;
	}
	
//	private String uploadImg(MultipartFile uploadFile) {
//		StringBuffer filePath = new StringBuffer();
//		try {
//			// 文件原名称
//			String fileName = uploadFile.getOriginalFilename();
//			int index = fileName.lastIndexOf(".");
//			String suffix = "";
//			if (index != -1) {
//				suffix = fileName.substring(index);// 文件后缀
//			}
//			File dirFile = new File(config.getPhoneUrl());
//			if (!dirFile.exists()) {
//				dirFile.mkdirs();
//			}
//			filePath.append(config.getPhoneUrl()).append("/").append(ConvertUtil.createUUID()).append(suffix);
//			// 上传文件的路径
//			uploadFile.transferTo(new File(filePath.toString()));
//		} catch (IllegalStateException e) {
//			logger.error("图片上传：无效状态异常:{}", e);
//		} catch (IOException e) {
//			logger.error("图片上传：IO异常:{}", e);
//		}
//		return filePath.toString();
//	}
	 
	/**
	 * 更新用户相关信息
	 */
	private int modifyUser(SusrUserinfo updateUserInfo) {
//		SusrUserinfo userInfo = userInfoMapper.findUserInfoByUserId(userId);
//		BeanUtils.copyProperties(updateUserInfo, userInfo);
		// 设置共通信息
		DbTypeUtils.setCommonProp(updateUserInfo);
		
		return userInfoMapper.updateUserByUserId(updateUserInfo);
	}
	 
	/**
	 * 设置用户个人信息 
	 */
	@Override
	public Result runNo006Logic(No006Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		SusrUserinfo userInfo = new SusrUserinfo();
		BeanUtils.copyProperties(input, userInfo);
		userInfo.setUserId(userId);
		int num = modifyUser(userInfo);
		if(1 == num){
			//更新完成后查询
//			SusrUserinfo user = userInfoMapper.findUserInfoByUserId(userId);
			return generator.genSuccessResult();
		}
		return generator.genErrorResult(ResultEnum.RETURN_UPDATE_USER_ERROR.getCode());
	}

	/**
	 * 退出登录
	 */
	@Override
	public Result runNo007Logic(No007Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		//清除token
		redisService.remove(LocalSessionFactory.getToken());
		return generator.genSuccessResult();
	}

	/**
	 * 获取个人系统设置
	 */
	@Override
	public Result runNo008Logic(No008Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		SbasUserSetting userSettingInfo = userSettingMapper.selectByUserId(userId);
		No008Output userSettingOutput = new No008Output();
		userSettingOutput.setAnswerNoticeFlag(userSettingInfo.getPushAnswerFlag());
		userSettingOutput.setReplyNoticeFlag(userSettingInfo.getPushReplyFlag());
		userSettingOutput.setActivityNoticeFlag(userSettingInfo.getPushDynamicFlag());
		userSettingOutput.setDiscussNoticeFlag(userSettingInfo.getPushDicussFlag());
		userSettingOutput.setNoticedNoticeFlag(userSettingInfo.getPushFlowFlag());
		return generator.genSuccessResult(userSettingOutput);
	}

	/**
	 * 更换手机号
	 */
	@Override
	public Result runNo009Logic(No009Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		if(input.getOldCode().equals(redisService.get(input.getOldPhone()))){
			redisService.remove(input.getOldPhone());
			if(input.getNewcode().equals(redisService.get(input.getNewPhone()))){
				redisService.remove(input.getNewPhone());
				SusrUser user = new SusrUser();
				user.setId(userId);
				user.setTelephone(input.getNewPhone());
				// 设置共通信息
				DbTypeUtils.setCommonProp(user);
				int num = userMapper.updateByPrimaryKeySelective(user);
				if(1 == num){
					return generator.genSuccessResult();
				}else{
					return generator.genErrorResult(ResultEnum.RETURN_UPDATE_USER_ERROR.getCode());
				}
			}
		}
		return generator.genErrorResult(ResultEnum.RETURN_VARIFY_CODE_ERROR.getCode());
	}

	/**
	 * 意见反馈
	 */
	@Override
	public Result runNo010Logic(No010Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		SbasFeedback feed = new SbasFeedback();
		feed.setId(idService.nextId());
		feed.setUserId(userId);
		feed.setMemo(input.getAdviseText());
		feed.setContact(input.getContactWay());
		feed.setPictures(input.getAdviseImg());
		feed.setStatus(0);
		// 设置共通信息
		DbTypeUtils.setCommonProp(feed);
		sbasFeedbackMapper.insert(feed);
		return generator.genSuccessResult();
	}

	/**
	 * 个人系统设置
	 */
	@Override
	public Result runNo011Logic(No011Input input) {
		Long userId = LocalSessionFactory.getUserId();
		if(userId == null){
			logger.error("无法获取正确的userId，可能是没有登录！");
			return generator.genErrorResult(ResultEnum.RETURN_NOT_LOGIN.getCode());	
		}
		SbasUserSetting userSetting = new SbasUserSetting();
		BeanUtils.copyProperties(input, userSetting);
		// 设置共通信息
		DbTypeUtils.setCommonProp(userSetting);
		int num = userSettingMapper.updateUserSettingByUserID(userSetting);
		if(1 == num){
			SbasUserSetting userSettingInfo = userSettingMapper.selectByUserId(userId);
			return generator.genSuccessResult(userSettingInfo);
		}
		return generator.genErrorResult(ResultEnum.RETURN_UPDATE_USER_ERROR.getCode());
	}

	/**
	 * 获取产品信息
	 */
	@Override
	public Result runNo012Logic() {
		SbasSetting sbasSetting = sbasSettingMapper.selectSbasSetting();
		if(sbasSetting !=null){
			return generator.genSuccessResult(sbasSetting);
		}
		return generator.genErrorResult(ResultEnum.RETURN_UPDATE_USER_ERROR.getCode());
	}

}
