package com.icfans.paltform.logic.resource.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.ProblemOrderTypeEnum;
import com.icfans.paltform.common.enums.ProblemSearchTypeEnum;
import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.core.check.CheckEnum;

public class No034Input extends BaseVo {
	/**
	 * 查询类型
	 */
	@NotEmpty(message = "查询类型不能为空")
	@CheckEnum(ProblemSearchTypeEnum.class)
	private Integer queryType;

	/**
	 * 查询参数
	 */
	private String queryKey = "";

	/**
	 * 排序类型
	 */
	@NotEmpty(message = "排序类型不能为空")
	@CheckEnum(ProblemOrderTypeEnum.class)
	private Integer orderType;

	/**
	 * 当前页
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer page;

	/**
	 * 显示记录条数
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer size;

	public Integer getQueryType() {
		return queryType;
	}

	public void setQueryType(Integer queryType) {
		this.queryType = queryType;
	}

	public String getQueryKey() {
		return queryKey;
	}

	public void setQueryKey(String queryKey) {
		this.queryKey = queryKey;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
}
