package com.icfans.paltform.logic.resource.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.vo.BaseVo;

public class No038Input extends BaseVo {
	/**
	 * 问题ID
	 */
	@NotEmpty(message = "问题ID不能为空")
	private Long problemId;

	/**
	 * 当前页
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer page;

	/**
	 * 显示记录条数
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer size;

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

}
