package com.icfans.paltform.logic.system.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.comm.bean.IdNameBean;

public class No070Output extends BaseVo {
	/**
	 * 热门搜索列表
	 */
	private List<IdNameBean> hotkeyList;

	public List<IdNameBean> getHotkeyList() {
		return hotkeyList;
	}

	public void setHotkeyList(List<IdNameBean> hotkeyList) {
		this.hotkeyList = hotkeyList;
	}
}
