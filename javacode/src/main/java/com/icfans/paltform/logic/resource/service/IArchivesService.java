package com.icfans.paltform.logic.resource.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.resource.vo.No032Input;
import com.icfans.paltform.logic.resource.vo.No033Input;

public interface IArchivesService {

	public Result runNo032Logic(No032Input input);

	public Result runNo033Logic(No033Input input);

}
