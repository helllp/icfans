package com.icfans.paltform.logic.resource.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.DiscussInfo;

public class No038Output extends BaseVo {
	/**
	 * 具体问题的评论信息列表
	 */
	private List<DiscussInfo> descussList;

	public List<DiscussInfo> getDescussList() {
		return descussList;
	}

	public void setDescussList(List<DiscussInfo> descussList) {
		this.descussList = descussList;
	}
}
