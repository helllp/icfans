package com.icfans.paltform.logic.system.vo;

import org.springframework.web.multipart.MultipartFile;

import com.icfans.paltform.common.vo.BaseVo;

public class No072Input extends BaseVo {
	/**
	 * 上传的图片文件
	 */
	private MultipartFile photoFile;

	public MultipartFile getPhotoFile() {
		return photoFile;
	}

	public void setPhotoFile(MultipartFile photoFile) {
		this.photoFile = photoFile;
	}
}
