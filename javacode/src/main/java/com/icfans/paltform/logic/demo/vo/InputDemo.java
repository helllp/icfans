package com.icfans.paltform.logic.demo.vo;

import java.io.Serializable;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.SexTypeEnum;
import com.icfans.paltform.core.check.CheckEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="输入测试对象",description="这是一个输入测试的对象")
public class InputDemo implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value="ID",name="id",example="12222")
	private long id;

	@ApiModelProperty(value="用户名",name="name",example="linxm")
	@NotEmpty(message = "{welcome}")
	private String name;

	@NotEmpty(message = "{password}")
	@Length(min = 6, message = "密码长度太短")
	private String password;

	private String dataType;
	
	@CheckEnum(value = SexTypeEnum.class, message="错误的性别")
	private int sex;
	
	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
	       this.id = id;
	    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	@Override
	public String toString() {
		return "Demo [id=" + id + ", name=" + name + ", password=" + password + "]";
	}
}
