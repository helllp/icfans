package com.icfans.paltform.logic.comm.mapper;

import java.util.List;

import com.icfans.paltform.model.SusrRole;

public interface UserPolicyDaoMapper {
	/**
	 * 根据用户ID，查询拥有的角色
	 * 
	 * @param userId
	 * @return
	 */
	List<SusrRole> queryByUserId(Long userId);
}
