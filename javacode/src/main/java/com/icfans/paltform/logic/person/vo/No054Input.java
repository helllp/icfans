package com.icfans.paltform.logic.person.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.StoreOpTypeEnum;
import com.icfans.paltform.core.check.CheckEnum;

public class No054Input {
	/**
	 * 被收藏问题id
	 */
	@NotEmpty(message = "问题ID不能为空")
	private Long problemId;
	
	/**
	 * 收藏操作类型：
	 */
	@CheckEnum(StoreOpTypeEnum.class)
	private int storeOpType;

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public int getStoreOpType() {
		return storeOpType;
	}

	public void setStoreOpType(int storeOpType) {
		this.storeOpType = storeOpType;
	}
}
