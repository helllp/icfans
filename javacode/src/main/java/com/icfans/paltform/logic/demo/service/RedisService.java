package com.icfans.paltform.logic.demo.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service  
public class RedisService {  
	private static final Logger logger = LoggerFactory.getLogger(RedisService.class);
	
    @Autowired  
    RedisTemplate<?, ?> redisTemplate; 
    
    @Cacheable(value="usercache")  
//  @Cacheable(value = "usercache", keyGenerator = "mykeyGenerator",condition="#name!=null", unless = "#result==null")  
//    @CachePut
//    @CacheEvict
    public String getUser(String no,String name){  
    	System.out.println("测试一下是否打印出来了！");
        return "test" + no + ";" + name + ";" + new Date();  
    }  
    
    /**获得客户端列表 */  
    public List<?> getClients(){  
        return redisTemplate.getClientList();  
    }  
    /**设置有超时时间的KV */  
    public Long set(String key, String value, long seconds) {
    	return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection c) throws DataAccessException {
				c.set(key.getBytes(), value.getBytes());  
	            c.expire(key.getBytes(), seconds);  
	            return 1L;  
			}
		});
    }  
    /** 
     *存入不会超时的KV 
     */  
    public Long set(String key, String value) {  
    	return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection c) throws DataAccessException {
				c.set(key.getBytes(), value.getBytes());  
	            return 1L;  
			}
		});    	
    }  
    /** 
     * redis数据库条数 
     */  
    public Long dbSize() {  
    	return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection c) throws DataAccessException {
	            return c.dbSize();  
			}
		});            
    }  
  
    public String ping() {  
    	return redisTemplate.execute(new RedisCallback<String>() {
			@Override
			public String doInRedis(RedisConnection c) throws DataAccessException {
	            return c.ping();  
			}
		});        
    }  
    /** 
     * 删除所有指定数据库的数据 
     */  
    public long flushDB() {  
    	return redisTemplate.execute(new RedisCallback<Long>() {
			@Override
			public Long doInRedis(RedisConnection c) throws DataAccessException {
				c.flushDb();  
	            return 1L; 
			}
		});    	
    }  
    /**判断redis数据库是否有对应的key*/  
    public boolean exist(String key){  
//        return redisTemplate.execute(c->c.exists(key.getBytes()));  
    	return redisTemplate.execute(new RedisCallback<Boolean>() {
			@Override
			public Boolean doInRedis(RedisConnection c) throws DataAccessException {
				return c.exists(key.getBytes());
			}
		});         
    }  
    
//    /**获得redis数据库所有的key*/  
//    public Set<String> keys(String pattern){  
////    	return redisTemplate.execute(new RedisCallback<Set<String>>() {
////			@Override
////			public Long doInRedis(RedisConnection c) throws DataAccessException {
////				c.set(key.getBytes(), value.getBytes());  
////	            return 1L;  
////			}
////		}); 
//        return redisTemplate.execute(c->c.keys(pattern.getBytes()).stream().map(this::getUTF).collect(Collectors.toSet()));  
//    }
    
    private String getUTF(byte[] data){  
        try {  
            return new String(data, "utf-8");  
        } catch (UnsupportedEncodingException e) {
        	logger.error("parse bytes err:{}", e);
            return null;  
        }  
    }  
}  