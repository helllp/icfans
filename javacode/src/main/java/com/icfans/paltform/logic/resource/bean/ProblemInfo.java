package com.icfans.paltform.logic.resource.bean;

public class ProblemInfo {
	/**
	 * 问题ID
	 */
	private Long id;
	
	/**
	 * 提出问题用户ID
	 */
	private Long userId;
	
	/**
	 * 问题的文字描述
	 */
	private String problemsDes;
	
	/**
	 * 问题的图片URL地址，逗号进行分隔
	 */
	private String picUrls;
	
	/**
	 * 问题的关键字，逗号进行分隔
	 */
	private String problemsKeys;
	
	/**
	 * 创建时间
	 */
	private String createTime;
	
	/**
	 * 问题解答者ID
	 */
	private Long answerUserId;

	/**
	 * 问题的评论数量
	 */
	private int problemsDiscussCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProblemsDes() {
		return problemsDes;
	}

	public void setProblemsDes(String problemsDes) {
		this.problemsDes = problemsDes;
	}

	public String getPicUrls() {
		return picUrls;
	}

	public void setPicUrls(String picUrls) {
		this.picUrls = picUrls;
	}

	public String getProblemsKeys() {
		return problemsKeys;
	}

	public void setProblemsKeys(String problemsKeys) {
		this.problemsKeys = problemsKeys;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Long getAnswerUserId() {
		return answerUserId;
	}

	public void setAnswerUserId(Long answerUserId) {
		this.answerUserId = answerUserId;
	}

	public int getProblemsDiscussCount() {
		return problemsDiscussCount;
	}

	public void setProblemsDiscussCount(int problemsDiscussCount) {
		this.problemsDiscussCount = problemsDiscussCount;
	}
}
