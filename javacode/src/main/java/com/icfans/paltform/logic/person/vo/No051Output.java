package com.icfans.paltform.logic.person.vo;

import java.util.List;

import com.icfans.paltform.logic.resource.bean.ArchiveInfo;

public class No051Output {
	/**
	 * 文章信息列表
	 */
	private List<ArchiveInfo> archiveList;

	public List<ArchiveInfo> getArchiveList() {
		return archiveList;
	}

	public void setArchiveList(List<ArchiveInfo> archiveList) {
		this.archiveList = archiveList;
	}
}
