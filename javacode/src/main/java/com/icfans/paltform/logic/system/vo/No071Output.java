package com.icfans.paltform.logic.system.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.system.bean.AreaInfo;

public class No071Output extends BaseVo {
	/**
	 * 领域信息
	 */
	private List<AreaInfo> areaList;

	public List<AreaInfo> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<AreaInfo> areaList) {
		this.areaList = areaList;
	}
}
