package com.icfans.paltform.logic.resource.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.MasterInfo;

public class No030Output extends BaseVo {
	/**
	 * 专家的信息列表
	 */
	private List<MasterInfo> masterInfoList;

	public List<MasterInfo> getMasterInfoList() {
		return masterInfoList;
	}

	public void setMasterInfoList(List<MasterInfo> masterInfoList) {
		this.masterInfoList = masterInfoList;
	}
}
