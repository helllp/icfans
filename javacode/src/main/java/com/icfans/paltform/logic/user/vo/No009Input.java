package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No009Input extends BaseVo {
	/**
	 * 新手机
	 */
	private String newPhone;
	
	/**
	 * 新的校验码
	 */
	private String newcode;
	
	/**
	 * 旧手机
	 */
	private String oldPhone;
	
	/**
	 * 旧的校验码
	 */
	private String oldCode;
	/**
	 * 用户令牌
	 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNewPhone() {
		return newPhone;
	}

	public void setNewPhone(String newPhone) {
		this.newPhone = newPhone;
	}

	public String getNewcode() {
		return newcode;
	}

	public void setNewcode(String newcode) {
		this.newcode = newcode;
	}

	public String getOldPhone() {
		return oldPhone;
	}

	public void setOldPhone(String oldPhone) {
		this.oldPhone = oldPhone;
	}

	public String getOldCode() {
		return oldCode;
	}

	public void setOldCode(String oldCode) {
		this.oldCode = oldCode;
	}
}
