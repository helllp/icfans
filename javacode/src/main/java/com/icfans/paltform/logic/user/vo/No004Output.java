package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.comm.bean.UserInfo;

public class No004Output extends BaseVo {
	/**
	 * 该用户所关注他人的总数
	 */
	private int attentionCount;
	
	/**
	 * 该用户所收藏的总数
	 */
	private int enshrineCount;
	
	/**
	 * 该用户所提问的总数
	 */
	private int issueCount;

	/**
	 * 用户的基本信息
	 */
	private UserInfo userInfo;

	public int getAttentionCount() {
		return attentionCount;
	}

	public void setAttentionCount(int attentionCount) {
		this.attentionCount = attentionCount;
	}

	public int getEnshrineCount() {
		return enshrineCount;
	}

	public void setEnshrineCount(int enshrineCount) {
		this.enshrineCount = enshrineCount;
	}

	public int getIssueCount() {
		return issueCount;
	}

	public void setIssueCount(int issueCount) {
		this.issueCount = issueCount;
	}

	public UserInfo getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}
}
