package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No012Input extends BaseVo {
	/**
	 * 产品logo（小）
	 */
	private String logoMin;
	
	/**
	 * 轮换图
	 */
	private String rotateImg;
	
	/**
	 * 产品logo（大）
	 */
	private String logoMax;
	
	/**
	 * 关于
	 */
	private String asRegards;

	public String getLogoMin() {
		return logoMin;
	}

	public void setLogoMin(String logoMin) {
		this.logoMin = logoMin;
	}

	public String getRotateImg() {
		return rotateImg;
	}

	public void setRotateImg(String rotateImg) {
		this.rotateImg = rotateImg;
	}

	public String getLogoMax() {
		return logoMax;
	}

	public void setLogoMax(String logoMax) {
		this.logoMax = logoMax;
	}

	public String getAsRegards() {
		return asRegards;
	}

	public void setAsRegards(String asRegards) {
		this.asRegards = asRegards;
	}
}
