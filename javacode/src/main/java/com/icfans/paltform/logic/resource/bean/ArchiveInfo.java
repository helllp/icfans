package com.icfans.paltform.logic.resource.bean;

/**
 * 文章的基本信息
 * 
 * @author 林晓明
 *
 */
public class ArchiveInfo {
	/**
	 * 文章ID 
	 */
	private Long id;
	
	/**
	 * 作者ID
	 */
	private Long userId;
	
	/**
	 * 文章领域ID
	 */
	private Long areaId;
	
	/**
	 * 文章领域名称
	 */
	private String areaName;
	
	/**
	 * 文章关键字，逗号分隔
	 */
	private String archivesKey;
	
	/**
	 * 文章摘要
	 */
	private String archivesSummary;
	
	/**
	 * 文章封面URL
	 */
	private String archivesCover;
	
	/**
	 * 创建时间
	 */
	private String createTime;
	
	/**
	 * 文章URL
	 */
	private String archivesURL;
	
	/**
	 * 文章被阅读次数
	 */
	private String readCount;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAreaId() {
		return areaId;
	}

	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getArchivesKey() {
		return archivesKey;
	}

	public void setArchivesKey(String archivesKey) {
		this.archivesKey = archivesKey;
	}

	public String getArchivesSummary() {
		return archivesSummary;
	}

	public void setArchivesSummary(String archivesSummary) {
		this.archivesSummary = archivesSummary;
	}

	public String getArchivesCover() {
		return archivesCover;
	}

	public void setArchivesCover(String archivesCover) {
		this.archivesCover = archivesCover;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTIme(String createTime) {
		this.createTime = createTime;
	}

	public String getArchivesURL() {
		return archivesURL;
	}

	public void setArchivesURL(String archivesURL) {
		this.archivesURL = archivesURL;
	}

	public String getReadCount() {
		return readCount;
	}

	public void setReadCount(String readCount) {
		this.readCount = readCount;
	}
}
