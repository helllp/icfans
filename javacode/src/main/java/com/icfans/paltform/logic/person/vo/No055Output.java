package com.icfans.paltform.logic.person.vo;

import java.util.List;

import com.icfans.paltform.logic.resource.bean.ProblemInfo;

public class No055Output {
	/**
	 * 个人收藏的问题列表
	 */
	private List<ProblemInfo> problemList;

	public List<ProblemInfo> getProblemList() {
		return problemList;
	}

	public void setProblemList(List<ProblemInfo> problemList) {
		this.problemList = problemList;
	}
	
}
