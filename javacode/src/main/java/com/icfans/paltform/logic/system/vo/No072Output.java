package com.icfans.paltform.logic.system.vo;

import com.icfans.paltform.common.vo.BaseVo;

public class No072Output extends BaseVo {
	/**
	 * 图片的存储路径
	 */
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
