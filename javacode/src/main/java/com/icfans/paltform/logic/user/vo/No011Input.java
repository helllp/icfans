package com.icfans.paltform.logic.user.vo;

import com.icfans.paltform.common.enums.BooleanEnum;
import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.core.check.CheckEnum;

public class No011Input extends BaseVo {
	/**
	 * 有回答时通知我
	 */
	@CheckEnum(BooleanEnum.class)
	private int answerNotice;
	
	/**
	 * 有人回复时通知我
	 */
	@CheckEnum(BooleanEnum.class)
	private int replyNotice;
	
	/**
	 * 关注的人有新动态
	 */
	@CheckEnum(BooleanEnum.class)
	private int activityNotice;
	
	/**
	 * 有新评论时通知我
	 */
	@CheckEnum(BooleanEnum.class)
	private int discussNotice;
	
	/**
	 * 有新关注时通知我
	 */
	@CheckEnum(BooleanEnum.class)
	private int noticedNotice;
	
	/**
	 * 用户令牌
	 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getAnswerNotice() {
		return answerNotice;
	}

	public void setAnswerNotice(int answerNotice) {
		this.answerNotice = answerNotice;
	}

	public int getReplyNotice() {
		return replyNotice;
	}

	public void setReplyNotice(int replyNotice) {
		this.replyNotice = replyNotice;
	}

	public int getActivityNotice() {
		return activityNotice;
	}

	public void setActivityNotice(int activityNotice) {
		this.activityNotice = activityNotice;
	}

	public int getDiscussNotice() {
		return discussNotice;
	}

	public void setDiscussNotice(int discussNotice) {
		this.discussNotice = discussNotice;
	}

	public int getNoticedNotice() {
		return noticedNotice;
	}

	public void setNoticedNotice(int noticedNotice) {
		this.noticedNotice = noticedNotice;
	}
}
