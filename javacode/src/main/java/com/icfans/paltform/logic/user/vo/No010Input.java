package com.icfans.paltform.logic.user.vo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.vo.BaseVo;

public class No010Input extends BaseVo {
	/**
	 * 意见文本
	 */
	@NotEmpty(message = "文本内容不能为空")
	@Size(max=200,min=1,message = "文字大小不符合标准")
	private String adviseText;
	
	/**
	 * 意见图片地址列表，以逗号分割
	 */
	private String adviseImg;
	
	/**
	 * 联系方式
	 */
	@NotEmpty(message = "联系方式不能为空")
	private String contactWay;
	
	/**
	 * 用户令牌
	 */
	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getAdviseText() {
		return adviseText;
	}

	public void setAdviseText(String adviseText) {
		this.adviseText = adviseText;
	}

	public String getAdviseImg() {
		return adviseImg;
	}

	public void setAdviseImg(String adviseImg) {
		this.adviseImg = adviseImg;
	}

	public String getContactWay() {
		return contactWay;
	}

	public void setContactWay(String contactWay) {
		this.contactWay = contactWay;
	}
}
