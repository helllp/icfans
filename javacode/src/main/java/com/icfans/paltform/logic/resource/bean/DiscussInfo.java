package com.icfans.paltform.logic.resource.bean;

/**
 * 评论信息
 * 
 * @author 林晓明
 *
 */
public class DiscussInfo {
	/**
	 * 评论ID
	 */
	private Long id;
	
	/**
	 * 问题ID
	 */
	private Long problemId;
	
	/**
	 * 对问题进行评论的用户ID
	 */
	private Long replyUserId;
	
	/**
	 * 对问题的评论内容
	 */
	private String replyMessage;
	
	/**
	 * ProblemReplyTypeEnum.class
	 */
	private int replyType;
	
	/**
	 * 被回复人的ID
	 */
	private Long repiedUserId;
	
	/**
	 * 创建时间
	 */
	private String createTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProblemId() {
		return problemId;
	}

	public void setProblemId(Long problemId) {
		this.problemId = problemId;
	}

	public Long getReplyUserId() {
		return replyUserId;
	}

	public void setReplyUserId(Long replyUserId) {
		this.replyUserId = replyUserId;
	}

	public String getReplyMessage() {
		return replyMessage;
	}

	public void setReplyMessage(String replyMessage) {
		this.replyMessage = replyMessage;
	}

	public int getReplyType() {
		return replyType;
	}

	public void setReplyType(int replyType) {
		this.replyType = replyType;
	}

	public Long getRepiedUserId() {
		return repiedUserId;
	}

	public void setRepiedUserId(Long repiedUserId) {
		this.repiedUserId = repiedUserId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
}
