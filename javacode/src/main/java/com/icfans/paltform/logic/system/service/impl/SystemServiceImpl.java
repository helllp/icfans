package com.icfans.paltform.logic.system.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.system.service.ISystemService;
import com.icfans.paltform.logic.system.vo.No070Input;
import com.icfans.paltform.logic.system.vo.No071Input;
import com.icfans.paltform.logic.system.vo.No072Input;

@Service
public class SystemServiceImpl implements ISystemService{
	/**
	 * 系统通用的服务
	 */
	//	生成返回信息服务
	@Autowired
	private ResultGenerator generator;
	
	//	Redis服务
	@Autowired
	private RedisTemplateService redisService;
	
	//	数据库ID生成服务
	@Autowired
	private IdService idService;
	
	//	获取配置信息服务
	@Autowired
	private PropertiesConfig config;
	
	private static final Logger logger = LoggerFactory.getLogger(SystemServiceImpl.class);
	
	@Override
	public Result runNo070Logic(No070Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo071Logic(No071Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo072Logic(No072Input input) {
		return generator.genSuccessResult();
	}

}
