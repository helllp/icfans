package com.icfans.paltform.logic.person.vo;

import org.hibernate.validator.constraints.NotEmpty;

public class No055Input {
	/**
	 * 当前页
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer page;
	
	/**
	 * 显示记录条数
	 */
	@NotEmpty(message = "分页信息不能为空")
	private Integer size;

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}
}
