package com.icfans.paltform.logic.person.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultGenerator;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.core.service.IdService;
import com.icfans.paltform.core.service.RedisTemplateService;
import com.icfans.paltform.logic.person.service.IPersonService;
import com.icfans.paltform.logic.person.vo.No050Input;
import com.icfans.paltform.logic.person.vo.No051Input;
import com.icfans.paltform.logic.person.vo.No052Input;
import com.icfans.paltform.logic.person.vo.No053Input;
import com.icfans.paltform.logic.person.vo.No054Input;
import com.icfans.paltform.logic.person.vo.No055Input;

@Service
public class PersonServiceImpl implements IPersonService{
	/**
	 * 系统通用的服务
	 */
	//	生成返回信息服务
	@Autowired
	private ResultGenerator generator;
	
	//	Redis服务
	@Autowired
	private RedisTemplateService redisService;
	
	//	数据库ID生成服务
	@Autowired
	private IdService idService;
	
	//	获取配置信息服务
	@Autowired
	private PropertiesConfig config;
	
	private static final Logger logger = LoggerFactory.getLogger(PersonServiceImpl.class);
	
	@Override
	public Result runNo050Logic(No050Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo051Logic(No051Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo052Logic(No052Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo053Logic(No053Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo054Logic(No054Input input) {
		return generator.genSuccessResult();
	}

	@Override
	public Result runNo055Logic(No055Input input) {
		return generator.genSuccessResult();
	}

}
