package com.icfans.paltform.logic.person.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.StoreOpTypeEnum;
import com.icfans.paltform.core.check.CheckEnum;

public class No052Input {
	/**
	 * 被关注用户ID
	 */
	@NotEmpty(message = "用户ID不能为空")
	private Long userId;
	
	/**
	 * 关注操作类型：
	 */
	@CheckEnum(StoreOpTypeEnum.class)
	private int storeOpType;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getStoreOpType() {
		return storeOpType;
	}

	public void setStoreOpType(int storeOpType) {
		this.storeOpType = storeOpType;
	}
	
}
