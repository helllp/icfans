package com.icfans.paltform.logic.person.vo;

import java.util.List;

import com.icfans.paltform.logic.comm.bean.UserInfo;

public class No053Output {
	/**
	 * 个人关注的用户列表
	 */
	private List<UserInfo> userList;

	public List<UserInfo> getUserList() {
		return userList;
	}

	public void setUserList(List<UserInfo> userList) {
		this.userList = userList;
	}
	
}
