package com.icfans.paltform.logic.resource.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.resource.vo.No030Input;
import com.icfans.paltform.logic.resource.vo.No031Input;

public interface IMasterService {

	public Result runNo030Logic(No030Input input);

	public Result runNo031Logic(No031Input input);

}
