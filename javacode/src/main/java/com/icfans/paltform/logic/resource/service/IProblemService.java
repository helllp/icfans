package com.icfans.paltform.logic.resource.service;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.logic.resource.vo.No034Input;
import com.icfans.paltform.logic.resource.vo.No035Input;
import com.icfans.paltform.logic.resource.vo.No036Input;
import com.icfans.paltform.logic.resource.vo.No037Input;
import com.icfans.paltform.logic.resource.vo.No038Input;

public interface IProblemService {
	public Result runNo034Logic(No034Input input);

	public Result runNo035Logic(No035Input input);

	public Result runNo036Logic(No036Input input);

	public Result runNo037Logic(No037Input input);

	public Result runNo038Logic(No038Input input);
}
