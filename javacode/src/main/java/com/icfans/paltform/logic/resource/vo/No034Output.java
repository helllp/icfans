package com.icfans.paltform.logic.resource.vo;

import java.util.List;

import com.icfans.paltform.common.vo.BaseVo;
import com.icfans.paltform.logic.resource.bean.ArchiveInfo;

public class No034Output extends BaseVo {
	/**
	 * 问题列表
	 */
	private List<ArchiveInfo> archiveList;

	public List<ArchiveInfo> getArchiveList() {
		return archiveList;
	}

	public void setArchiveList(List<ArchiveInfo> archiveList) {
		this.archiveList = archiveList;
	}

}
