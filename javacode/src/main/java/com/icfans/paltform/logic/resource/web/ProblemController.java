package com.icfans.paltform.logic.resource.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.resource.service.IProblemService;
import com.icfans.paltform.logic.resource.vo.No034Input;
import com.icfans.paltform.logic.resource.vo.No035Input;
import com.icfans.paltform.logic.resource.vo.No036Input;
import com.icfans.paltform.logic.resource.vo.No037Input;
import com.icfans.paltform.logic.resource.vo.No038Input;

/**
 * 问题相关API
 * 
 * @author 林晓明
 *
 */
@RestController
@RequestMapping("/res")
public class ProblemController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(ProblemController.class);
	
	@Autowired
	private IProblemService problemService;
	
	/**
	 * NO_034:查询问题
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no034")
	public Result postNo034(@Valid @RequestBody No034Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No034：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return problemService.runNo034Logic(input);
	}
	
	/**
	 * NO_035:获取问题详情
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no035")
	public Result postNo035(@Valid @RequestBody No035Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No035：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return problemService.runNo035Logic(input);
	}
	
	/**
	 * NO_036:发布问题
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no036")
	public Result postNo036(@Valid @RequestBody No036Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No036：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return problemService.runNo036Logic(input);
	}
	
	/**
	 * NO_037:发表问题评论
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no037")
	public Result postNo037(@Valid @RequestBody No037Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No037：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return problemService.runNo037Logic(input);
	}
	
	/**
	 * NO_038:获取问题的评论信息
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no038")
	public Result postNo038(@Valid @RequestBody No038Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No038：输入参数有误！");
			return this.genCheckResult(result);
		}
		
		return problemService.runNo038Logic(input);
	}
}
