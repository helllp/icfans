package com.icfans.paltform.logic.demo.vo;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class UserValidatorModel {
	/* 	Validator注解验证
	 * 	@Null	限制只能为null
	 *	@NotNull	限制必须不为null
	 *	@AssertFalse	限制必须为false
	 *	@AssertTrue	限制必须为true
	 *	@DecimalMax(value)	限制必须为一个不大于指定值的数字
	 *	@DecimalMin(value)	限制必须为一个不小于指定值的数字
	 *	@Digits(integer,fraction)	限制必须为一个小数，且整数部分的位数不能超过integer，小数部分的位数不能超过fraction
	 *	@Future	限制必须是一个将来的日期
	 *	@Max(value)	限制必须为一个不大于指定值的数字
	 *	@Min(value)	限制必须为一个不小于指定值的数字
	 *	@Past	限制必须是一个过去的日期
	 *	@Pattern(value)	限制必须符合指定的正则表达式,如：(regexp="[a-za-z0-9._%+-]+@[a-za-z0-9.-]+\\.[a-za-z]{2,4}", message="邮件格式错误")
	 *	@Size(max,min)	限制字符长度必须在min到max之间
	 *	@Past	验证注解的元素值（日期类型）比当前时间早
	 *	@NotEmpty	验证注解的元素值不为null且不为空（字符串长度不为0、集合大小不为0）
	 *	@NotBlank	验证注解的元素值不为空（不为null、去除首位空格后长度为0），不同于@NotEmpty，@NotBlank只应用于字符串且在比较时会去除字符串的空格
	 *	@Email	验证注解的元素值是Email，也可以通过正则表达式和flag指定自定义的email格式
	 */
		private long id;
		
	    @NotEmpty(message = "用户名不能为空")
	    private String username;
	    
	    @Size(min=6 ,max= 20 ,message = "密码长度不符合标准")
	    private String password;
	    
	    private String nickname;
	    
	    @Email(message="邮箱格式不对")
	    private String email;
	    
	    private String city;
	    
	    private int sex;
	    
	    public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getNickname() {
			return nickname;
		}
		public void setNickname(String nickname) {
			this.nickname = nickname;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public int getSex() {
			return sex;
		}
		public void setSex(int sex) {
			this.sex = sex;
		}
		public UserValidatorModel(long id, String username, String password, String nickname, String email, String city,
				int sex) {
			super();
			this.id = id;
			this.username = username;
			this.password = password;
			this.nickname = nickname;
			this.email = email;
			this.city = city;
			this.sex = sex;
		}
		
	}
