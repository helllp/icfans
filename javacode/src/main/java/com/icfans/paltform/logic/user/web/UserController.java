package com.icfans.paltform.logic.user.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.RedisDefine;
import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;
import com.icfans.paltform.logic.user.service.IUserService;
import com.icfans.paltform.logic.user.vo.No001Input;
import com.icfans.paltform.logic.user.vo.No002Input;
import com.icfans.paltform.logic.user.vo.No004Input;
import com.icfans.paltform.logic.user.vo.No005Input;
import com.icfans.paltform.logic.user.vo.No006Input;
import com.icfans.paltform.logic.user.vo.No007Input;
import com.icfans.paltform.logic.user.vo.No008Input;
import com.icfans.paltform.logic.user.vo.No009Input;
import com.icfans.paltform.logic.user.vo.No010Input;
import com.icfans.paltform.logic.user.vo.No011Input;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController{
	
	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private IUserService userService;
	
	/**
	 * NO_001:登录/注册
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no001")
	public Result postNo001(@Valid @RequestBody No001Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No001：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo001Logic(input);
	}
	
	/**
	 * NO_002:发送手机验证码
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no002")
	public Result postNo002(@Valid @RequestBody No002Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No002：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo002Logic(input);
	}
	
	/**
	 * NO_003:获取用户协议
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@GetMapping("/no003")
	@Cacheable(RedisDefine.No003)
	public Result postNo003(){
//		if(result.hasErrors()){
//			logger.info("No003：输入参数有误！");
//			return this.genCheckResult(result);
//		}
		return userService.runNo003Logic();
	}
	
	/**
	 * NO_004:获取个人信息
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no004")
	public Result postNo004(@Valid @RequestBody No004Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No004：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo004Logic(input);
	}
	
	/**
	 * NO_005:设置头像
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no005")
	public Result postNo005(@Valid @RequestBody No005Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No005：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo005Logic(input);
	}
	
	/**
	 * NO_006:设置用户个人信息
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no006")
	public Result postNo006(@Valid @RequestBody No006Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No006：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo006Logic(input);
	}
	
	/**
	 * NO_007:退出登录
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no007")
	public Result postNo007(@Valid @RequestBody No007Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No007：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo007Logic(input);
	}
	
	/**
	 * NO_008:获取个人系统设置
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no008")
	public Result postNo008(@Valid @RequestBody No008Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No008：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo008Logic(input);
	}
	
	/**
	 * NO_009:更换手机
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no009")
	public Result postNo009(@Valid @RequestBody No009Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No009：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo009Logic(input);
	}
	
	/**
	 * NO_010:写入意见反馈
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no010")
	public Result postNo010(@Valid @RequestBody No010Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No010：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo010Logic(input);
	}
	
	/**
	 * NO_011:个人系统设置
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@PostMapping("/no011")
	public Result postNo011(@Valid @RequestBody No011Input input,BindingResult result){
		if(result.hasErrors()){
			logger.info("No011：输入参数有误！");
			return this.genCheckResult(result);
		}
		return userService.runNo011Logic(input);
	}
	
	/**
	 * NO_012:获取产品信息
	 * 
	 * @param input
	 * @param result
	 * @return
	 */
	@GetMapping("/no012")
	public Result postNo012(){
		return userService.runNo012Logic();
	}
	
}
