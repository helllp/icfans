package com.icfans.paltform.logic.demo.web;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icfans.paltform.common.RedisDefine;
import com.icfans.paltform.common.inters.ISmsService;
import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.core.BaseController;

import io.swagger.annotations.Api;

@RestController
@Api(value="测试controller",tags={"测试操作接口"})
public class TestController extends BaseController{
	@Autowired
	private ISmsService smsService;
	
	@GetMapping("/u")
	public String getU(){
		String text = "【云片网】您的验证码是" + "123456";
		return text;
		//return smsService.sendBatchSms(new String[]{"13998832049","18609889940","18040025551"}, text);
	}
	
	@GetMapping("/join")
	public String getJoin(){
		return "utesJoin";
	}
	
	@GetMapping("/failed")
	public Result getFailed(){
		return this.genErrorResult(12, "Token校验失败的数据");
	}
	
	@GetMapping(value = "/test")
	@Cacheable(RedisDefine.KeepValid)
	public Result test(String id){
		Result ret = this.genSuccessResult();
		ret.setData(new Date());
		return ret;
	}
	
	@GetMapping(value = "/test2")
	@CacheEvict(value = RedisDefine.KeepValid, allEntries = true)
	public Result test2(){
		Result ret = this.genSuccessResult();
		ret.setData(new Date());
		return ret;
	}
	
//	private static final Logger logger = LoggerFactory.getLogger(TestController.class);
//	
//	@Autowired
//	private RedisService service;
//	
//	@Autowired
//	private JdbcServices jdbcService;
//	
//	@Autowired
//	private UserMapper usrMapper;
//	
//	@ApiOperation(value="redis的操作演示", notes="")
//	@ApiImplicitParams({ 
//		@ApiImplicitParam(name = "no", value = "用户no", required = true, dataType = "String",paramType="query"), 
//		@ApiImplicitParam(name = "name", value = "用户名称", required = true, dataType = "String",paramType="query") })
//	@GetMapping("/redis")
//	@ResponseBody
//	public Result getRedis(String no, String name){
//		String temp = service.getUser(no, name);
//		return this.genSuccessResult(temp);
//	}
//	
//	@ApiOperation(value="jdbc的操作演示", notes="")
//	@GetMapping("/jdbc")
//	@ResponseBody
//	public Result getJdbc(){
//		List<User> list = jdbcService.findAll();
//		System.out.println(list.size());
//		
//		User u = new User();
//		u.setId(3L);
//		u.setNickName("修改了");
//		u.setUsername("程序");
//		jdbcService.update(u);
//		
////		u.setId(20);
////		u.setNickName("增加了");
////		u.setUsername("增加");
////		u = jdbcService.create(u);
//		
//		u = jdbcService.findUserById(4);
//		
//		return this.genSuccessResult(u);
//	}
//	
//	@GetMapping("/del")
//	@ResponseBody
//	public Result del(int id){
//		jdbcService.delete(id);
//		return this.genSuccessResult();
//	}
//	
//	@ApiOperation(value="mybatis的操作演示", notes="")
//	@GetMapping("/user")
//	@ResponseBody
//	public Result userMapper(){
//		User u = usrMapper.selectByPrimaryKey(3L);
//		return this.genSuccessResult(u);
//	}
//	
//	@ApiOperation(value="简单的操作演示", notes="")
//	@ApiImplicitParams({ 
//		@ApiImplicitParam(name = "demo", value = "输入的信息", required = true, dataType = "InputDemo",paramType="body")})
//	@PostMapping("/")
//	@ResponseBody
//	public Result getMessage(@Valid @RequestBody InputDemo demo, BindingResult result) {
//		logger.info("这是一个测试的日志信息！");
//		
//		System.out.println("++++++++redis numer +++++++++" + service.dbSize());
//		
//		Result ret = this.genCheckResult(result);
//		
//		if (ret.hasError()) {
//			return ret;
//		}
//		//	执行业务逻辑
//		return ret;
//	}
//	
//	@Autowired
//	private MyLogicMapper mapper;
//	
//	//	可以进行分页显示
//	@GetMapping("/my")
//	@ResponseBody
//	public Result mybatisXml() {	
////		com.icfans.paltform.model.User u = usrMapper.selectByPrimaryKey(2);
//		//显示第2页3条数据
//		PageHelper.startPage(2,3);  
//
////		u = mapper.findUserByName("4@qq.com");
//		List<User> list = mapper.getAll();
//		return this.genSuccessResult(list);
//	}
//	
//	@GetMapping("/trans")
//	@ResponseBody
//	public Result trans(int id) {
//		User u = jdbcService.findUserById(id);
//		u.setNickName("测试修改昵称");
//		jdbcService.update(u);
////		List<User> list = mapper.getAll();
//		return this.genSuccessResult(u);
//	}
//	
//	/**
//	 * 上传多媒体
//	 * @param userId 用户ID
//	 * @param multimediaUploadFile 上传文件
//	 * @param mediaContent 备注
//	 * @return
//	 */
//	@RequestMapping(value = "/upload", method = RequestMethod.POST)
//	@ResponseBody
//	public String uploadAndImport(MultipartFile multimediaUploadFile) {
//		try {
//			String fileName = multimediaUploadFile.getOriginalFilename();
//            int index = fileName.lastIndexOf(".");
//            String suffix = "";
//            if(index != -1) {
//            	suffix = fileName.substring(index);//文件后缀
//            }
//			multimediaUploadFile.getContentType();
//			String filepath = "e:/temp/file/";   
//			File dirFile = new File(filepath);
//			if (!dirFile.exists()) {
//				dirFile.mkdirs();
//			}
//			
//			filepath = filepath + ConvertUtil.createUUID() + suffix;
//			File file = new File(filepath);
//
//			// 上传文件另存为
//			multimediaUploadFile.transferTo(file);
//			
//			//	TODO 在数据库中存储相关的文件信息
//		} catch (IllegalStateException e) {
//			return "系统错误（非法状态）！";
//		} catch (IOException e) {
//			return "系统错误（IO失败）！";
//		} catch (Exception e) {
//			return "系统错误！";	
//		}
//
//		return "正常完成";
//	}
//	
//	/**
//	 * 下载多媒体
//	 * @param id 多媒体ID
//	 * @param request
//	 * @param response
//	 * @return
//	 */
//	@RequestMapping(value = "/download",method = RequestMethod.GET)
//	@ResponseBody
//	public String download(String id, HttpServletRequest request, HttpServletResponse response) {
//
//		try {
//			DownloadUtil.downloadFile(request, response, new File("e:/temp/file/" + id + ".png"), id + ".png");
//			
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//		}
//		
//		return "运行正常";
//	}
//
//	@Autowired
//	private PropertiesConfig config;
//	
//	@ResponseBody
//	@GetMapping("/a")
//	public String test(){
//		return config.getPhoneUrl();
//	}
}
