package com.icfans.paltform.logic.person.vo;

import org.hibernate.validator.constraints.NotEmpty;

import com.icfans.paltform.common.enums.StoreOpTypeEnum;
import com.icfans.paltform.core.check.CheckEnum;

public class No050Input {
	/**
	 * 文章ID
	 */
	@NotEmpty(message = "文章ID不能为空")
	private Long archivesId;
	
	/**
	 * 收藏操作类型：StoreOpTypeEnum
	 */
	@CheckEnum(StoreOpTypeEnum.class)
	private int storeOpType;

	public Long getArchivesId() {
		return archivesId;
	}

	public void setArchivesId(Long archivesId) {
		this.archivesId = archivesId;
	}

	public int getStoreOpType() {
		return storeOpType;
	}

	public void setStoreOpType(int storeOpType) {
		this.storeOpType = storeOpType;
	}

}
