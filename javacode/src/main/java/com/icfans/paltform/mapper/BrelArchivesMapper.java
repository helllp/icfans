package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BrelArchives;

public interface BrelArchivesMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BrelArchives record);

    int insertSelective(BrelArchives record);

    BrelArchives selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BrelArchives record);

    int updateByPrimaryKey(BrelArchives record);
}