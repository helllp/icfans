package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SusrRole;

public interface SusrRoleMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SusrRole record);

    int insertSelective(SusrRole record);

    SusrRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SusrRole record);

    int updateByPrimaryKey(SusrRole record);
}