package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SbasSearchWords;

public interface SbasSearchWordsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SbasSearchWords record);

    int insertSelective(SbasSearchWords record);

    SbasSearchWords selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SbasSearchWords record);

    int updateByPrimaryKey(SbasSearchWords record);
}