package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BrelProblems;

public interface BrelProblemsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BrelProblems record);

    int insertSelective(BrelProblems record);

    BrelProblems selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BrelProblems record);

    int updateByPrimaryKey(BrelProblems record);
}