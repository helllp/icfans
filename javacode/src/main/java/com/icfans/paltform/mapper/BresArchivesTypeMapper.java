package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BresArchivesType;

public interface BresArchivesTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BresArchivesType record);

    int insertSelective(BresArchivesType record);

    BresArchivesType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BresArchivesType record);

    int updateByPrimaryKey(BresArchivesType record);
}