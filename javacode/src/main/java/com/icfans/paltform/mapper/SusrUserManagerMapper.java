package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SusrUserManager;

public interface SusrUserManagerMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SusrUserManager record);

    int insertSelective(SusrUserManager record);

    SusrUserManager selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SusrUserManager record);

    int updateByPrimaryKey(SusrUserManager record);
}