package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BintProblemsDiscuss;

public interface BintProblemsDiscussMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BintProblemsDiscuss record);

    int insertSelective(BintProblemsDiscuss record);

    BintProblemsDiscuss selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BintProblemsDiscuss record);

    int updateByPrimaryKey(BintProblemsDiscuss record);
}