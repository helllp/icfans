package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SbasSetting;

public interface SbasSettingMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SbasSetting record);

    int insertSelective(SbasSetting record);

    SbasSetting selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SbasSetting record);

    int updateByPrimaryKey(SbasSetting record);
    
    /**
     * 自定义方法
     */
    /**
     * 查询产品的相关信息
     * @return
     */
    SbasSetting selectSbasSetting();
}