package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SbasFeedback;

public interface SbasFeedbackMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SbasFeedback record);

    int insertSelective(SbasFeedback record);

    SbasFeedback selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SbasFeedback record);

    int updateByPrimaryKey(SbasFeedback record);
}