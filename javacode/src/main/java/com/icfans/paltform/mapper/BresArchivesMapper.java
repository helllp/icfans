package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BresArchives;

public interface BresArchivesMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BresArchives record);

    int insertSelective(BresArchives record);

    BresArchives selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BresArchives record);

    int updateByPrimaryKey(BresArchives record);
}