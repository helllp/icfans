package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SbasUserSetting;

public interface SbasUserSettingMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SbasUserSetting record);

    int insertSelective(SbasUserSetting record);

    SbasUserSetting selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SbasUserSetting record);

    int updateByPrimaryKey(SbasUserSetting record);
    
    /**
     * 以下为自定义方法
     */
    int updateUserSettingByUserID(SbasUserSetting userSetting);
    
    SbasUserSetting selectByUserId(Long userId);
}