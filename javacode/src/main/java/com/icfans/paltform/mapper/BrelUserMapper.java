package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BrelUser;

public interface BrelUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BrelUser record);

    int insertSelective(BrelUser record);

    BrelUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BrelUser record);

    int updateByPrimaryKey(BrelUser record);
}