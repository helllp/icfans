package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BresArchivesRead;

public interface BresArchivesReadMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BresArchivesRead record);

    int insertSelective(BresArchivesRead record);

    BresArchivesRead selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BresArchivesRead record);

    int updateByPrimaryKey(BresArchivesRead record);
}