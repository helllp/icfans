package com.icfans.paltform.mapper;

import com.icfans.paltform.model.BintProblems;

public interface BintProblemsMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BintProblems record);

    int insertSelective(BintProblems record);

    BintProblems selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(BintProblems record);

    int updateByPrimaryKey(BintProblems record);
}