package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SusrUserinfo;

public interface SusrUserinfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SusrUserinfo record);

    int insertSelective(SusrUserinfo record);

    SusrUserinfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SusrUserinfo record);

    int updateByPrimaryKey(SusrUserinfo record);
    
    //	自定义扩展    
	/**
	 * 根据用户ID在详细信息表中查询用户的信息
	 * 
	 * @param userId
	 * @return
	 */
	SusrUserinfo findUserInfoByUserId(Long userId);
	
	/**
	 * 更新用户根据用户ID
	 * @param susrUserinfo
	 * @return
	 */
	int updateUserByUserId(SusrUserinfo susrUserinfo);
}