package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SusrUser;

public interface SusrUserMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SusrUser record);

    int insertSelective(SusrUser record);

    SusrUser selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SusrUser record);

    int updateByPrimaryKey(SusrUser record);
    
    //	自定义扩展
	/**
	 * 根据手机查找用户ID
	 * @param phone
	 * @return
	 */
	SusrUser findUserByPhone(String phone);
}