package com.icfans.paltform.mapper;

import com.icfans.paltform.model.SusrRolePermiss;

public interface SusrRolePermissMapper {
    int deleteByPrimaryKey(Long id);

    int insert(SusrRolePermiss record);

    int insertSelective(SusrRolePermiss record);

    SusrRolePermiss selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SusrRolePermiss record);

    int updateByPrimaryKey(SusrRolePermiss record);
}