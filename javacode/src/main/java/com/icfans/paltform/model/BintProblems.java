package com.icfans.paltform.model;

public class BintProblems {
    private Long id;

    private Long userId;

    private String askTitle;

    private String askDescription;

    private String askPicUrls;

    private Long answerUserId;

    private String askKeys;

    private Long createUserId;

    private Long createTime;

    private Long updateUserId;

    private Long updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getAskTitle() {
        return askTitle;
    }

    public void setAskTitle(String askTitle) {
        this.askTitle = askTitle == null ? null : askTitle.trim();
    }

    public String getAskDescription() {
        return askDescription;
    }

    public void setAskDescription(String askDescription) {
        this.askDescription = askDescription == null ? null : askDescription.trim();
    }

    public String getAskPicUrls() {
        return askPicUrls;
    }

    public void setAskPicUrls(String askPicUrls) {
        this.askPicUrls = askPicUrls == null ? null : askPicUrls.trim();
    }

    public Long getAnswerUserId() {
        return answerUserId;
    }

    public void setAnswerUserId(Long answerUserId) {
        this.answerUserId = answerUserId;
    }

    public String getAskKeys() {
        return askKeys;
    }

    public void setAskKeys(String askKeys) {
        this.askKeys = askKeys == null ? null : askKeys.trim();
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}