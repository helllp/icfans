package com.icfans.paltform.model;

public class SbasSetting {
    private Long id;

    private String carousel;

    private String carouselUrl;

    private String logoMin;

    private String logoMax;

    private String memo;

    private Long createUserId;

    private Long createTime;

    private Long updateUserId;

    private Long updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarousel() {
        return carousel;
    }

    public void setCarousel(String carousel) {
        this.carousel = carousel == null ? null : carousel.trim();
    }

    public String getCarouselUrl() {
        return carouselUrl;
    }

    public void setCarouselUrl(String carouselUrl) {
        this.carouselUrl = carouselUrl == null ? null : carouselUrl.trim();
    }

    public String getLogoMin() {
        return logoMin;
    }

    public void setLogoMin(String logoMin) {
        this.logoMin = logoMin == null ? null : logoMin.trim();
    }

    public String getLogoMax() {
        return logoMax;
    }

    public void setLogoMax(String logoMax) {
        this.logoMax = logoMax == null ? null : logoMax.trim();
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}