package com.icfans.paltform.model;

public class SbasUserSetting {
    private Long id;

    private Long userId;

    private Boolean pushAnswerFlag;

    private Boolean pushReplyFlag;

    private Boolean pushDynamicFlag;

    private Boolean pushDicussFlag;

    private Boolean pushFlowFlag;

    private Long createUserId;

    private Long createTime;

    private Long updateUserId;

    private Long updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getPushAnswerFlag() {
        return pushAnswerFlag;
    }

    public void setPushAnswerFlag(Boolean pushAnswerFlag) {
        this.pushAnswerFlag = pushAnswerFlag;
    }

    public Boolean getPushReplyFlag() {
        return pushReplyFlag;
    }

    public void setPushReplyFlag(Boolean pushReplyFlag) {
        this.pushReplyFlag = pushReplyFlag;
    }

    public Boolean getPushDynamicFlag() {
        return pushDynamicFlag;
    }

    public void setPushDynamicFlag(Boolean pushDynamicFlag) {
        this.pushDynamicFlag = pushDynamicFlag;
    }

    public Boolean getPushDicussFlag() {
        return pushDicussFlag;
    }

    public void setPushDicussFlag(Boolean pushDicussFlag) {
        this.pushDicussFlag = pushDicussFlag;
    }

    public Boolean getPushFlowFlag() {
        return pushFlowFlag;
    }

    public void setPushFlowFlag(Boolean pushFlowFlag) {
        this.pushFlowFlag = pushFlowFlag;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}