package com.icfans.paltform.model;

public class BresArchives {
    private Long id;

    private Long userId;

    private Long archivesGroupId;

    private String archivesKeys;

    private String archivesSummary;

    private String archivesUrl;

    private Integer archivesType;

    private String archivesCoverUrl;

    private Integer archivesReadNum;

    private Long createUserId;

    private Long createTime;

    private Long updateUserId;

    private Long updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getArchivesGroupId() {
        return archivesGroupId;
    }

    public void setArchivesGroupId(Long archivesGroupId) {
        this.archivesGroupId = archivesGroupId;
    }

    public String getArchivesKeys() {
        return archivesKeys;
    }

    public void setArchivesKeys(String archivesKeys) {
        this.archivesKeys = archivesKeys == null ? null : archivesKeys.trim();
    }

    public String getArchivesSummary() {
        return archivesSummary;
    }

    public void setArchivesSummary(String archivesSummary) {
        this.archivesSummary = archivesSummary == null ? null : archivesSummary.trim();
    }

    public String getArchivesUrl() {
        return archivesUrl;
    }

    public void setArchivesUrl(String archivesUrl) {
        this.archivesUrl = archivesUrl == null ? null : archivesUrl.trim();
    }

    public Integer getArchivesType() {
        return archivesType;
    }

    public void setArchivesType(Integer archivesType) {
        this.archivesType = archivesType;
    }

    public String getArchivesCoverUrl() {
        return archivesCoverUrl;
    }

    public void setArchivesCoverUrl(String archivesCoverUrl) {
        this.archivesCoverUrl = archivesCoverUrl == null ? null : archivesCoverUrl.trim();
    }

    public Integer getArchivesReadNum() {
        return archivesReadNum;
    }

    public void setArchivesReadNum(Integer archivesReadNum) {
        this.archivesReadNum = archivesReadNum;
    }

    public Long getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(Long createUserId) {
        this.createUserId = createUserId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Long updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }
}