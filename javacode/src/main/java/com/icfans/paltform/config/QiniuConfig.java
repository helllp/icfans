package com.icfans.paltform.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qiniu.common.Zone;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

/**
 * 七牛的配置信息
 * 
 * @author 林晓明
 *
 */
@Configuration
public class QiniuConfig {
	/**
	 * 系统的配置信息
	 */
    @Autowired
    private PropertiesConfig sysConfig;
    
    @Bean
    public Auth qiniuAuth() {
        return Auth.create(sysConfig.getAccessKey(), sysConfig.getSecuretKey());
    }
    
    @Bean
    public com.qiniu.storage.Configuration qiniuConfiguration() {
        return new com.qiniu.storage.Configuration(Zone.autoZone());
    }
    
    @Bean
    public BucketManager qiniuBucketManager() {
        return new BucketManager(qiniuAuth(), qiniuConfiguration());
    }
    
    @Bean
    public UploadManager qiniuUploadManager() {
        return new UploadManager(qiniuConfiguration());
    }
    
}
