package com.icfans.paltform.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

/**
 * 国际化的配置
 * 
 * @author 林晓明
 *
 */
@Configuration
public class I18nConfig extends WebMvcConfigurerAdapter {
	@Autowired
	private MessageSource messageSource = null;
	
    @Bean  
    public MyLocaleResolver localeResolver() {  
    	MyLocaleResolver slr = new MyLocaleResolver();  
        //	默认设为简体中文
        slr.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);  
        return slr;  
    }  

    @Bean  
    public LocaleChangeInterceptor localeChangeInterceptor() {  
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();  
        //	参数中通过lang指定语言
        lci.setParamName("lang");  
        return lci;  
    }  

    @Override  
    public void addInterceptors(InterceptorRegistry registry) {  
        registry.addInterceptor(localeChangeInterceptor());  
    }  

    @Bean  
    public Validator getValidator(){  
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();  
        validator.setValidationMessageSource(messageSource);  
        return validator;  
    }  
}
