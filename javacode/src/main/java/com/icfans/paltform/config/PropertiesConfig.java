package com.icfans.paltform.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 系统的配置信息
 * 
 * @author 林晓明
 *
 */
//@ConfigurationProperties(prefix = "icfans.config")
@Configuration
public class PropertiesConfig {
	
	@Value("${icfans.config.debug}")
	private boolean isDebug;
	
	/**
	 * 默认的图片存放路径：测试用
	 */
	@Value("${icfans.config.phoneUrl}")
	private String phoneUrl;

	/**
	 * 七牛的AccessKey
	 */
	@Value("${icfans.config.qiniu.accessKey}")
	private String accessKey;
	
	/**
	 * 七牛的SecuretKey
	 */
	@Value("${icfans.config.qiniu.securetKey}")
	private String securetKey;
	
	/**
	 * 七牛的空间名
	 */
	@Value("${icfans.config.qiniu.bucketName}")
	private String bucketName;
	
	/**
	 * 云片的Key
	 */
	@Value("${icfans.config.yunpian.apiKey}")
	private String yunpianKey;
	
	/**
	 * 用户协议HTTP地址
	 */
	@Value("${icfans.config.user.contract.uri}")
	private String userContract;
	
	/**
	 * 用户默认头像地址
	 */
	@Value("${icfans.config.user.headphoto.uri}")
	private String headPhotoUri;
	
	public String getPhoneUrl() {
		return phoneUrl;
	}

	public void setPhoneUrl(String phoneUrl) {
		this.phoneUrl = phoneUrl;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecuretKey() {
		return securetKey;
	}

	public void setSecuretKey(String securetKey) {
		this.securetKey = securetKey;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getYunpianKey() {
		return yunpianKey;
	}

	public void setYunpianKey(String yunpianKey) {
		this.yunpianKey = yunpianKey;
	}

	public String getUserContract() {
		return userContract;
	}

	public void setUserContract(String userContract) {
		this.userContract = userContract;
	}

	public String getHeadPhotoUri() {
		return headPhotoUri;
	}

	public void setHeadPhotoUri(String headPhotoUri) {
		this.headPhotoUri = headPhotoUri;
	}

	public boolean isDebug() {
		return isDebug;
	}

	public void setDebug(boolean isDebug) {
		this.isDebug = isDebug;
	}
}
