/*
 *    Copyright 2010-2015 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.icfans.paltform.config;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

import com.icfans.paltform.core.shiro.RolesAuthorizationFilter;
import com.icfans.paltform.core.shiro.StatelessAuthcFilter;
import com.icfans.paltform.core.shiro.TokenRealm;

@Configuration
@Import(ShiroManager.class)
public class ShiroConfiguration {

    /**
     * 配置自定义的Realm
     * @return
     */
	@Bean(name = "realm")
	@DependsOn("lifecycleBeanPostProcessor")
	@ConditionalOnMissingBean
	public Realm realm() {
		return new TokenRealm();
	}

	/**
	 * 过滤器的主要配置，具体权限信息书写在这里
	 * 
	 * @param securityManager
	 * @param realm
	 * @return
	 */
	@Bean(name = "shiroFilter")
	@DependsOn("securityManager")
	@ConditionalOnMissingBean
	public ShiroFilterFactoryBean getShiroFilterFactoryBean(DefaultSecurityManager securityManager, Realm realm) {
		securityManager.setRealm(realm);

		ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();
		shiroFilter.setSecurityManager(securityManager);

		/**
		 * 设置配置数据：anon,statelessAuthc,roles,statelessRoles
		 */
		Map<String,String> filterDef = new LinkedHashMap<String,String>();

		filterDef.put("/user/no001", "anon");
		filterDef.put("/user/no002", "anon");
		filterDef.put("/user/no003", "anon");
		filterDef.put("/user/no004", "statelessAuthc");
		filterDef.put("/user/no005", "statelessAuthc");
		filterDef.put("/user/no006", "statelessAuthc");
		filterDef.put("/user/no007", "statelessAuthc");
		filterDef.put("/user/no008", "statelessAuthc");
		filterDef.put("/user/no009", "statelessAuthc");
		filterDef.put("/user/no010", "statelessAuthc");
		filterDef.put("/user/no011", "statelessAuthc");
		filterDef.put("/user/no012", "statelessAuthc");
		
		filterDef.put("/comm/**", "anon");
		
		filterDef.put("/system/**", "statelessAuthc");
		filterDef.put("/res/**", "statelessAuthc");
		filterDef.put("/person/**", "statelessAuthc");
		
		filterDef.put("/**", "anon");
		
		shiroFilter.setFilterChainDefinitionMap(filterDef);
		
		//	设置过滤器：进行权限过滤
        shiroFilter.getFilters().put("statelessAuthc", getAuthcFilter());
        //	需要进行多个角色校验的时候：statelessRoles[role1,role2]
        shiroFilter.getFilters().put("statelessRoles", getRoleFilter());
        
		return shiroFilter;
	}

	/**
	 * 自定义的Token过滤器
	 * 
	 * @return
	 */
	@Bean(name = "statelessAuthc")
	@ConditionalOnMissingBean
	public StatelessAuthcFilter getAuthcFilter(){
		return new StatelessAuthcFilter();
	}
	
	/**
	 * 自定义的Role过滤器
	 * 
	 * @return
	 */
	@Bean(name = "statelessRoles")
	@ConditionalOnMissingBean
	public RolesAuthorizationFilter getRoleFilter(){
		return new RolesAuthorizationFilter();
	}
}
