package com.icfans.paltform.core.service;

/**
 * 存储用户ID号，方便进行缓存的读取
 * 
 * @author 林晓明
 *
 */
public class LocalSessionFactory {

	/**
	 * 保存登录者的用户ID号(数据库中的ID)
	 */
	public static final ThreadLocal<Long> userIdLocal = new ThreadLocal<Long>();

	public static Long getUserId() {
		return (Long) userIdLocal.get();
	}

	public static void setUserId(Long user) {
		userIdLocal.set(user);
	}
	
	/**
	 * 保持登录者的Token信息
	 */
	public static final ThreadLocal<String> tokenLocal = new ThreadLocal<String>();
	
	public static String getToken() {
		return (String) tokenLocal.get();
	}

	public static void setToken(String token) {
		tokenLocal.set(token);
	}
}