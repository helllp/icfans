package com.icfans.paltform.core.shiro;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import com.icfans.paltform.core.service.LocalSessionFactory;
import com.icfans.paltform.logic.comm.service.UserPolicyService;

/**
 * 进行权限的验证
 * 
 * @author 林晓明
 *
 */
public class TokenRealm extends AuthorizingRealm {
	
	@Autowired
	private UserPolicyService policyService;
	
    @Override
    public boolean supports(AuthenticationToken token) {
        //仅支持StatelessToken类型的Token
        return token instanceof StatelessToken;
    }
    
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    	
        if (principals == null) {
            throw new AuthorizationException("用户身份不能为空！");
        }
        
    	Long userId = (Long)getAvailablePrincipal(principals);
    	
        //	根据用户名查找角色，请根据需求实现
        SimpleAuthorizationInfo authorizationInfo =  new SimpleAuthorizationInfo();
        //	设置权限角色
        authorizationInfo.addRoles(policyService.gerUserRole(userId));
        return authorizationInfo;
    }
    
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        StatelessToken statelessToken = (StatelessToken) token;
        
        String passInfo = statelessToken.getToken();
        LocalSessionFactory.setToken(passInfo);
        
        Long userId = policyService.checkToken(passInfo); 
        
        if(userId != null){
        	LocalSessionFactory.setUserId(userId);
        	
            //然后进行客户端消息摘要和服务器端消息摘要的匹配
            return new SimpleAuthenticationInfo(
            		userId,		//	用户名
            		passInfo, 	//	认证信息
                    getName());        	
        }else{
        	throw new AccountException("权限认证的Token不正确！");
        }
    }
}
