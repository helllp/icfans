package com.icfans.paltform.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;

import com.icfans.paltform.common.vo.Result;
import com.icfans.paltform.common.vo.ResultEnum;
import com.icfans.paltform.common.vo.ResultGenerator;


public class BaseController {
	@Autowired
	private ResultGenerator generator;
	
    public Result genSuccessResult() {
        return generator.genSuccessResult();
    }

    public Result genSuccessResult(Object data) {
        return generator.genSuccessResult(data);
    }

    public Result genErrorResult(int code) {
        return generator.genErrorResult(code);
    } 

    public Result genErrorResult(ResultEnum e) {
        return generator.genErrorResult(e.getCode());
    } 
    
    public Result genErrorResult(int code, String message) {
        return generator.genErrorResult(code, message);
    } 
    
    public Result genCheckResult(BindingResult checkRusult) {
    	return generator.genCheckResult(checkRusult);
    }
}
