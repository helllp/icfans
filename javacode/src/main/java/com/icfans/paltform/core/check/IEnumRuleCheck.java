package com.icfans.paltform.core.check;

import java.util.Map;

/**
 * 所有的enum检查的基础接口
 * 
 * @author 林晓明
 *
 */
public interface IEnumRuleCheck {
	/**
	 * 检验数据是否合法
	 * 
	 * @param value
	 * @return
	 */
	public boolean check(int value);
	
	/**
	 * 获取指定的名称
	 * 
	 * @param value
	 * @return
	 */
	public String getName(int value);
	
	/**
	 * 获取当前对象的值
	 * 
	 * @return
	 */
	public int getValue();
	
	/**
	 * 获取所有enum中的信息
	 * 
	 * @return
	 */
	public Map<Integer,String> getList();
}
