package com.icfans.paltform.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.icfans.paltform.common.inters.ISmsService;
import com.icfans.paltform.config.PropertiesConfig;
import com.icfans.paltform.utils.SmsYunpianUtils;

/**
 * 用云片发送短信
 * 
 * @author 林晓明
 *
 */
@Component
public class SmsServiceYunpian implements ISmsService{
	@Autowired
	private PropertiesConfig config;
	
	@Override
	public String sendSms(String phone, String text) {
		return SmsYunpianUtils.sendSms(config.getYunpianKey(), text, phone);
	}

	@Override
	public String sendBatchSms(String[] phones, String text) {
		return SmsYunpianUtils.sendBatchSms(config.getYunpianKey(), text, phones);
	}

}
