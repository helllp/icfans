package com.icfans.paltform.core.service;

import java.util.Locale;

import javax.annotation.Resource;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

/**
 * 获取本地信息的服务
 * 
 * @author 林晓明
 *
 */
@Component
public class LocaleMessageSourceService {
	@Resource
	private MessageSource messageSource;

	/**
	 * 获取对应key的信息
	 * 
	 * @param code	对应messages配置的key.
	 * @return
	 */
	public String getMessage(String code) {
		return this.getMessage(code, new Object[] {});
	}

	public String getMessage(String code, String defaultMessage) {
		return this.getMessage(code, null, defaultMessage);
	}

	public String getMessage(String code, String defaultMessage, Locale locale) {
		return this.getMessage(code, null, defaultMessage, locale);
	}

	public String getMessage(String code, Locale locale) {
		return this.getMessage(code, null, "", locale);
	}

	public String getMessage(String code, Object[] args) {
		return this.getMessage(code, args, "");
	}

	public String getMessage(String code, Object[] args, Locale locale) {
		return this.getMessage(code, args, "", locale);
	}

	public String getMessage(String code, Object[] args, String defaultMessage) {
		// 这里使用比较方便的方法，不依赖request.
		Locale locale = LocaleContextHolder.getLocale();
		return this.getMessage(code, args, defaultMessage, locale);
	}

	public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		return messageSource.getMessage(code, args, defaultMessage, locale);
	}
}
