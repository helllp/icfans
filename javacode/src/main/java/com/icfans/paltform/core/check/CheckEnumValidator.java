package com.icfans.paltform.core.check;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckEnumValidator implements ConstraintValidator<CheckEnum, Integer>{
	private CheckEnum checkEnum ;
	
	@Override
	public void initialize(CheckEnum arg0) {
		this.checkEnum = arg0;
	}

	@Override
	public boolean isValid(Integer value, ConstraintValidatorContext arg1){
		// 使用enum的values方法
		try {
			Method valuesMethod = checkEnum.value().getMethod("values");
			
			IEnumRuleCheck[] checkInter = (IEnumRuleCheck[])valuesMethod.invoke((Object)null, (Object[])null);
			
			if(!checkInter[0].check(value)){
				return false;
			}
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}

}
