package com.icfans.paltform.core.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.icfans.paltform.common.Define;

/**
 * 无状态请求过滤器
 * 
 * @author linxm
 *
 */
public class StatelessAuthcFilter extends AccessControlFilter {

	private Logger logger = LoggerFactory.getLogger(StatelessAuthcFilter.class);
	
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        return false;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) throws Exception {
		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		
    	String token = request.getParameter(Define.TOKEN_PARAM_NAME);
    	
    	if(token == null){
    		logger.info("请求未含有Token");
    		WebUtils.issueRedirect(request, response, Define.COMMON_URL_TOKEN);
            return false;
    	}else{
        	StatelessToken loginToken = new StatelessToken(token);
        	
            try {
                //	委托给Realm进行登录
                getSubject(request, response).login(loginToken);
                logger.info("Token={0} 验证成功", token);
            } catch (Exception e) {
                logger.info("Token={0} 验证失败", token);
        		WebUtils.issueRedirect(request, response, Define.COMMON_URL_TOKEN);
                return false;
            }
    	}
        
        return true;
    }
    
//	private void out(HttpServletResponse response, Result resultMap)
//			throws IOException {
//
//		response.setHeader("Content-type", "application/json;charset=UTF-8"); 
//		response.setCharacterEncoding("UTF-8");  
//		
//		PrintWriter out = response.getWriter();
//		out.write(JSON.toJSONString(resultMap));
//		out.flush();
//		out.close();
//	}
}
