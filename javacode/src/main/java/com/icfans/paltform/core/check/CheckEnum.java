package com.icfans.paltform.core.check;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Constraint(validatedBy = {CheckEnumValidator.class})
public @interface CheckEnum {

    Class<? extends IEnumRuleCheck> value();
    
    String message() default "错误的现实信息";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
