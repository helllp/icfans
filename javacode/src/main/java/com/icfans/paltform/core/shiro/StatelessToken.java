package com.icfans.paltform.core.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * 自定义的Token
 * 
 * @author linxm
 *
 */
public class StatelessToken implements AuthenticationToken {

	private static final long serialVersionUID = 1L;
	
	private String token;
    
    public StatelessToken(String token) {
        this.token = token;
    }

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
    public Object getPrincipal() {
       return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
