package com.icfans.paltform.core.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.icfans.paltform.config.PropertiesConfig;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.FetchRet;
import com.qiniu.storage.model.FileInfo;
import com.qiniu.storage.model.FileListing;
import com.qiniu.util.Auth;

/**
 * 七牛的工具服务
 * 
 * @author 林晓明
 *
 */
@Component
public class QiniuService {
	/**
	 * 系统的配置信息
	 */
    @Autowired
    private PropertiesConfig sysConfig;
    
    /**
     * 认证
     */
	@Autowired
    private Auth auth;

    /**
     * 七牛的配置
     */
	@Autowired
    private Configuration configuration;

    /**
     * 七牛的资源管理器
     */
	@Autowired
    private BucketManager bucketManager;

    /**
     * 七牛的上传管理器
     */
	@Autowired
    private UploadManager uploadManager;

    /**
     * 查询的最大数量限制
     */
    private static int LIMIT_SIZE = 1000;

    /**
     * 返回七牛账号的所有空间
     *
     * @return
     * @throws QiniuException
     */
    public String[] listBucket() throws QiniuException {
        return bucketManager.buckets();
    }

    /**
     * 返回指定空间的域名信息
     * 
     * @param bucketName
     * @return
     * @throws QiniuException
     */
    public String[] domainList(String bucketName) throws QiniuException {
        return bucketManager.domainList(bucketName);
    }
    
    
    /**
     * 获取指定空间下的文件列表
     *
     * @param bucketName
     * @param prefix
     * @param limit
     * @return
     */
    public List<FileInfo> listFileOfBucket(String bucketName, String prefix, int limit) {
        if (StringUtils.isBlank(bucketName)) {
            bucketName = sysConfig.getBucketName();
        }
        BucketManager.FileListIterator it = bucketManager.createFileListIterator(bucketName, prefix, limit, null);
        List<FileInfo> list = new ArrayList<>();
        while (it.hasNext()) {
            FileInfo[] items = it.next();
            if (null != items && items.length > 0) {
                list.addAll(Arrays.asList(items));
            }
        }
        return list;
    }

    /**
     * 七牛图片上传
     *
     * @param inputStream
     * @param bucketName
     * @param key
     * @param mimeType
     * @return
     * @throws Exception
     */
    public String uploadFile(InputStream inputStream, String bucketName, String key, String mimeType) throws IOException {
        String token = auth.uploadToken(bucketName);
        byte[] byteData = IOUtils.toByteArray(inputStream);
        Response response = uploadManager.put(byteData, key, token, null, mimeType, false);
        inputStream.close();
        return response.bodyString();
    }

    /**
     * 七牛图片上传
     *
     * @param inputStream
     * @param bucketName
     * @param key
     * @return
     * @throws IOException
     */
    public String uploadFile(InputStream inputStream, String bucketName, String key) throws IOException {
        return uploadFile(inputStream, bucketName, key, null);
    }

    /**
     * 七牛图片上传
     *
     * @param filePath
     * @param fileName
     * @param bucketName
     * @param key
     * @return
     * @throws IOException
     */
    public String uploadFile(String filePath, String fileName, String bucketName, String key) throws IOException {
        String token = auth.uploadToken(bucketName);
        InputStream is = new FileInputStream(new File(filePath + fileName));
        byte[] byteData = IOUtils.toByteArray(is);
        Response response = uploadManager.put(byteData, (StringUtils.isBlank(key)) ? fileName : key, token);
        is.close();
        return response.bodyString();
    }

    public String uploadFile(String filePath, String fileName, String bucketName) throws IOException {
        return uploadFile(filePath, fileName, bucketName, null);
    }

    /**
     * 提取网络资源并上传到七牛空间里
     *
     * @param url
     * @param bucketName
     * @param key
     * @return
     * @throws QiniuException
     */
    public String fetchToBucket(String url, String bucketName, String key) throws QiniuException {
        if (StringUtils.isBlank(bucketName)) {
            bucketName = sysConfig.getBucketName();
        }
        BucketManager bucketManager = new BucketManager(auth, configuration);
        
        FetchRet putret = bucketManager.fetch(url, bucketName, key);
        return putret.key;
    }

    public String fetchToBucket(String url, String bucketName) throws QiniuException {
        if (StringUtils.isBlank(bucketName)) {
            bucketName = sysConfig.getBucketName();
        }
        BucketManager bucketManager = new BucketManager(auth, configuration);
        FetchRet putret = bucketManager.fetch(url, bucketName);
        return putret.key;
    }

    /**
     * 七牛空间内文件复制
     *
     * @param bucket
     * @param key
     * @param targetBucket
     * @param targetKey
     * @throws QiniuException
     */
    public void copyFile(String bucket, String key, String targetBucket, String targetKey) throws QiniuException {
        bucketManager.copy(bucket, key, targetBucket, targetKey);
    }

    /**
     * 七牛空间内文件剪切
     *
     * @param bucket
     * @param key
     * @param targetBucket
     * @param targetKey
     * @throws QiniuException
     */
    public void moveFile(String bucket, String key, String targetBucket, String targetKey) throws QiniuException {
        bucketManager.move(bucket, key, targetBucket, targetKey);
    }

    /**
     * 七牛空间文件重命名
     *
     * @param bucket
     * @param key
     * @param targetKey
     * @throws QiniuException
     */
    public void renameFile(String bucket, String key, String targetKey) throws QiniuException {
        bucketManager.rename(bucket, key, targetKey);
    }

    /**
     * 七牛空间内文件删除
     *
     * @param bucket
     * @param key
     * @throws QiniuException
     */
    public void deleteFile(String bucket, String key) throws QiniuException {
        bucketManager.delete(bucket, key);
    }

    /**
     * 返回制定空间下的所有文件信息
     *
     * @param bucketName
     * @param prefix
     * @param limit
     * @return
     * @throws QiniuException
     */
    public FileInfo[] findFiles(String bucketName, String prefix, int limit) throws QiniuException {
        FileListing listing = bucketManager.listFiles(bucketName, prefix, null, limit, null);
        if (listing == null || listing.items == null || listing.items.length <= 0) {
            return null;
        }
        return listing.items;
    }

    public FileInfo[] findFiles(String bucketName) throws QiniuException {
        FileListing listing = bucketManager.listFiles(bucketName, null, null, LIMIT_SIZE, null);
        if (listing == null || listing.items == null || listing.items.length <= 0) {
            return null;
        }
        return listing.items;
    }

    /**
     * 返回制定空间下的某个文件
     *
     * @param bucketName
     * @param key
     * @param limit
     * @return
     * @throws QiniuException
     */
    public FileInfo findOneFile(String bucketName, String key, int limit) throws QiniuException {
        FileListing listing = bucketManager.listFiles(bucketName, key, null, limit, null);
        if (listing == null || listing.items == null || listing.items.length <= 0) {
            return null;
        }
        return (listing.items)[0];
    }

    public FileInfo findOneFile(String bucketName, String key) throws QiniuException {
        FileListing listing = bucketManager.listFiles(bucketName, key, null, LIMIT_SIZE, null);
        if (listing == null || listing.items == null || listing.items.length <= 0) {
            return null;
        }
        return (listing.items)[0];
    }

}