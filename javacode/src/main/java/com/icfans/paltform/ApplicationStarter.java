package com.icfans.paltform;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@MapperScan({
	"com.icfans.paltform.mapper"
	,"com.icfans.paltform.logic.demo.mapper"
	,"com.icfans.paltform.logic.user.mapper"
	,"com.icfans.paltform.logic.comm.mapper"
	,"com.icfans.paltform.logic.resource.mapper"
	,"com.icfans.paltform.logic.person.mapper"
	,"com.icfans.paltform.logic.system.mapper"})
public class ApplicationStarter {
	public static void main(String[] args) {
		SpringApplication.run(ApplicationStarter.class, args);
	}
}
